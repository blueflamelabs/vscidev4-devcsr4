<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <label>Synergent Default</label>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Lead</tab>
    <tab>standard-Opportunity</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-Chatter</tab>
    <tab>standard-File</tab>
    <tab>Contract__c</tab>
    <tab>Change_Management_Phase__c</tab>
    <tab>Case_Monitoring__c</tab>
</CustomApplication>
