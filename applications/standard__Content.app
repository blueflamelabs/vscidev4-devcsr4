<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <tab>standard-Chatter</tab>
    <tab>standard-Workspace</tab>
    <tab>standard-ContentSearch</tab>
    <tab>standard-ContentSubscriptions</tab>
    <tab>Contract__c</tab>
    <tab>Change_Management_Phase__c</tab>
</CustomApplication>
