({
    doInit : function(component, event, helper) 
    {
       
    },
    
    onCheckChangeReview:function(component,event,helper)
  	{
                let varRow = {},
            row = component.get("v.row");
        var casecommentId = row.CaseCommentId;
        var bReviewComplete = row.CommentReviewComplete;
        
        
        var action = component.get('c.UpdateReviewCompleteMonitoring');
        action.setParams(
        {
            "CaseCommentId": casecommentId,
            "bReviewComplete": bReviewComplete,
            "strCommunicationType": "Case Comment"
        });
        action.setCallback(this, $A.getCallback(function (response) 
        {
            var state = response.getState();
            if (state === "SUCCESS") 
            {
                
                $A.get("e.force:refreshView").fire();
                
                

            } 
            else if (state === "ERROR")
            {
                //console.log('state - '+ state);
                var errors = response.getError();
                console.error(errors);
               
            }
        }));
        $A.enqueueAction(action);
 
        
},
    onCheckChangePublic:function(component,event,helper)
  	{
        let varRow = {},
            row = component.get("v.row");

        
        var casecommentId = row.CaseCommentId;
        var bPublished = row.PublicFlag;
        
        
        var action = component.get('c.UpdateComment');
        action.setParams(
        {
            "CaseCommentId": casecommentId,
            "bPublished": bPublished
            
        });
        action.setCallback(this, $A.getCallback(function (response) 
        {
            var state = response.getState();
            if (state === "SUCCESS") 
            {
                
                $A.get("e.force:refreshView").fire();
                
                

            } 
            else if (state === "ERROR")
            {
                //console.log('state - '+ state);
                var errors = response.getError();
                console.error(errors);
               
            }
        }));
        $A.enqueueAction(action);
 
        
},
 
        getSelected :function(component,event,helper){
        	var navigationSObject = $A.get("e.force:navigateToSObject");
                console.log('navigate--',navigationSObject);
                navigationSObject.setParams({
                 "recordId": component.get("v.row.casecommentId")
                });
                navigationSObject.fire();
        },
    
     editRecord : function(component, event, helper)
    {
        helper.editRecord(component, event, helper);
    },

 	// Method to open the popup for confirmation the file deletion.
	handleClick : function(component, event, helper){
    
    component.set("v.deleteFile",true);
    var rowId = component.get("v.row.fileId");
    alert("rowid : "+rowId);
    
},
    handleSelect: function (component, event, helper) {
    // This will contain the string of the "value" attribute of the selected
    // lightning:menuItem
    // 
    
    var selectedMenuItemValue = event.getParam("value");
    //alert("Menu item selected with value: " + selectedMenuItemValue);
    if(selectedMenuItemValue === 'delete')
    {
        //console.log('test--');
        //component.set("v.deleteFile",true);
        //var rowId = component.get("v.row.fileId");
        //alert("rowid : "+rowId);
        //helper.deleteSelectedFile(component, event, helper, rowId);
    }
    else if(selectedMenuItemValue === 'edit') 
    {
        console.log('testedit---');
        helper.editRecord(component, event, helper);
    }
},
    /*navigateToRecord :function(component,event,helper){
        var navigationSObject = $A.get("e.force:navigateToSObject");
        console.log('navigate--',navigationSObject);
        navigationSObject.setParams({
            "recordId": component.get("v.row.fileId")
        });
        navigationSObject.fire();
    },*/
    
    // Method to redirect to the Owner of the file record detail page .
    navigateToOwnRecord :function(component,event,helper){
        var navigationSObject = $A.get("e.force:navigateToSObject");
        console.log('navigate--',navigationSObject);
        navigationSObject.setParams({
            "recordId": component.get("v.row.createById")
        });
        navigationSObject.fire();
    },
        
        // Method to redirect to the file's parent detail page .
        navigateToParentRecord :function(component,event,helper){
            var navigationSObject = $A.get("e.force:navigateToSObject");
            console.log('navigate--',navigationSObject);
            navigationSObject.setParams({
                "recordId": component.get("v.row.parentId")
            });
            navigationSObject.fire();
        },  
            
            // to close the Popup on close button.
            closeDeleteModel : function(component, event, helper){
                component.set("v.deleteFile",false);
            },   

})