({
    doInit: function(component, event, helper) {
        helper.getProfileMetaDataRecord(component, event, helper);
        helper.getCUAdminUser(component, event, helper);
        helper.getUsersList(component, event, helper);
        
        var action = component.get("c.getCompanyInformation");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                //console.log('=====storeResponse=====',storeResponse);
                component.set("v.timeZoneSidKeyValue", storeResponse);
            }
        });
        $A.enqueueAction(action);
        
    },
    
    doHandleChange : function(component, event, helper) {
        var selectedOptionValue = event.getParam("value");
        //alert(checkCmp);
        alert("Option selected with value: '" + selectedOptionValue + "'");
        component.set("v.profileId",selectedOptionValue);
        //console.log('---profile--',component.get('v.profileId'));
    },
    
    getToggleButtonValue:function(component, event, helper) {
        console.log('toggle ');
        helper.updateUser(component,event);
    },
    
    toCreateNewUser:function(cmp, event, helper) {
        var checkvalue= event.getSource().get("v.checked");
        //console.log('checkvalue==========',checkvalue);
        var getvalue= event.getSource().get("v.value");
        //console.log('getvalue==========',getvalue);
        var contactId= event.getSource().get("v.name");
        //var checkvalue = event.getSource().get("v.value");
        //var contactId= event.getSource().get("v.name");
        cmp.set('v.contactId',contactId);
        cmp.set('v.isToggleClicked',checkvalue);
        
        var wrapperList = cmp.get('v.usersWrapper');
        var firstName;
        var lastName;
        var Email;
        // 190819 - T-00555 - VennScience_BFL_Amruta - Added variables
        var phone;
        var mobilePhone;
        var title;
        
        var contactWrapList = wrapperList[0].contactWrapperList;
        for(var j=0; j< contactWrapList.length; j++) {
            var contactList = contactWrapList[j];
            //console.log('--inside contact loop-12-',contactList);
            //console.log('--inside contact loop-12-',contactList.Id);
            if(contactId == contactList.Id) {
                //console.log('--inside contact if-');
                firstName = contactList.FirstName;
                //console.log('--inside firstName-',firstName);
                lastName = contactList.LastName;
                Email = contactList.Email;
                // 190819 - T-00555 - VennScience_BFL_Amruta - Get Contact's phone
                phone = contactList.Phone;
                mobilePhone = contactList.MobilePhone;
                title = contactList.Title;
            }
            //console.log('--inside42 firstName-',firstName);
        }
        //console.log('--out if-',firstName);
        cmp.set('v.userFirstName',firstName);
        cmp.set('v.userLasttName',lastName);
        cmp.set('v.userEmail',Email);
        // 190819 - T-00555 - VennScience_BFL_Amruta - Set Contact's Phone
        cmp.set('v.userPhone',phone);
        // 190819 - T-00555 - VennScience_BFL_Amruta - Set Contact's Mobile Number
        cmp.set('v.userMobileNo',mobilePhone);
        // 190819 - T-00555 - VennScience_BFL_Amruta - Set Contact's Title
        cmp.set('v.userTitle',title);
        helper.fetchTimeZonePicklist(cmp, event, helper);
        helper.getProfileMetaDataRecord(cmp, event, helper);
        cmp.set('v.isOpenforNewUser',true);
    },
    
    closeNewUserModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpenforNewUser", false);
        //event.getsource().set("v.checked",false);
        component.set("v.isToggleClicked",false);
        
    },
    
    searchKeyChange: function(component, event, helper) {
        
        helper.searchStringChanged(component, event, helper);
    },
    
    saveNewCommunityUser : function(component, event, helper) {
        var allValid = component.find('userform').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && !inputCmp.get('v.validity').valueMissing;
        }, true);
        // 190819 - T-00555 - VennScience_BFL_Amruta - Validate mobile number digits
        var getMobileNumber = component.get('v.userMobileNo');
        if (allValid){ 
            
            	helper.saveTestCommunityUser(component, event, helper);   
            
        } 
        
    },
    
    getSelectedValue:function(component, event, helper){
        var picklist=component.find('ddIndustry');
        var picklistvalue=picklist.get('v.value');
        //alert(picklistvalue);
    },
    
    openModel: function(component, event, helper) {
        helper.fetchTimeZonePicklist(component, event, helper);
        helper.getProfileMetaDataRecord(component, event, helper);
        //helper.fetchLocalePicklist(component, event, helper);
        //helper.fetchEmailEncodingPicklist(component, event, helper);
        //helper.fetchLanguagePicklist(component, event, helper);
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.userFirstName", '');
		component.set("v.userLasttName", '');
        component.set("v.userEmail", '');
        // 190819 - T-00555 - VennScience_BFL_Amruta - Set the variables to default
        component.set("v.userPhone", '');
		component.set("v.userMobileNo", '');
        component.set("v.userTitle", '');
        
        component.set("v.isOpen", true);
    },
    
    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        
        component.set("v.isOpen", false);
    },
    navigateToOwnRecord :function(component,event,helper) {
           // 200819 - T-00555 - VennScience_BFL_Amruta - Navigate to contact detail page
           var navigationSObject = $A.get("e.force:navigateToSObject");
           var getTargetElement = event.currentTarget;
           var recordId = getTargetElement.getAttribute('data-record-id');
		   // Navigate to detail page
           navigationSObject.setParams({
               "recordId": recordId
           });
           navigationSObject.fire();
           
    },
    
    onClickEdit : function(component, event, helper) {
        
        var selecteduserId= event.getSource().get("v.name");
        
        helper.toggleEditSave(component, true, selecteduserId );
        helper.getProfileRecordforEdit(component, event, helper, selecteduserId);
    },
    
    onClickSaveProfile : function(component, event, helper) {
        
        var selecteduserId= event.getSource().get("v.name");
        helper.saveProfile(component, event, helper, selecteduserId);
        var isToggle = false;
        helper.toggleEditSave(component, false, selecteduserId );
    },
    
    onChange: function (cmp, evt, helper) {
        cmp.set('v.isChange',true);
        var pickListvalue = cmp.find('select').get('v.value');
        cmp.set('v.changedProfile',pickListvalue);
    },
    
    sortByName: function(component, event, helper) {
        component.set("v.sortByFieldName", "Name");
        var field = component.get("v.sortByFieldName");
        // toggle sort 
        var sortAsc = component.get("v.sortAsc");
        var sortField = component.get("v.sortField");
        //var sortAsc = (field == sortField) ? !sortAsc : sortAsc;
        var sortAsc = sortAsc ? false : true;
        component.set("v.sortAsc", sortAsc);
        console.log('sadad', component.get("v.sortAsc"));
        
        
        helper.sortBy(component,helper, field);
        var a=component.get("v.sortAsc");
        component.set("v.Name",a);
        component.set("v.NameArrow",true);
        component.set("v.PhoneArrow",false);
        component.set("v.ProfileArrow",false);
        component.set("v.EmailArrow",false);
        component.set("v.TitleArrow",false);
        
    },
    
    sortByPhone: function(component, event, helper) {
        component.set("v.sortByFieldName", "Phone");
        
        var field = component.get("v.sortByFieldName");
        // toggle sort 
        var sortAsc = component.get("v.sortAsc");
        var sortField = component.get("v.sortField");
        //var sortAsc = (field == sortField) ? !sortAsc : sortAsc;
        var sortAsc = sortAsc ? false : true;
        component.set("v.sortAsc", sortAsc);
        //console.log('sadad', component.get("v.sortAsc"));
        
        helper.sortBy(component,helper, field);
        var a=component.get("v.sortAsc");
        component.set("v.phone",a);
        component.set("v.NameArrow",false);
        component.set("v.PhoneArrow",true);
        component.set("v.ProfileArrow",false);
        component.set("v.EmailArrow",false);
        component.set("v.TitleArrow",false);
       
    },
    sortByTitle: function(component, event, helper) {
        component.set("v.sortByFieldName", "Title");
        
        var field = component.get("v.sortByFieldName");
        // toggle sort 
        var sortAsc = component.get("v.sortAsc");
        var sortField = component.get("v.sortField");
        //var sortAsc = (field == sortField) ? !sortAsc : sortAsc;
        var sortAsc = sortAsc ? false : true;
        component.set("v.sortAsc", sortAsc);
        
        helper.sortBy(component,helper, field);
        var a=component.get("v.sortAsc");
        component.set("v.Title",a);
        component.set("v.NameArrow",false);
        component.set("v.PhoneArrow",false);
        component.set("v.ProfileArrow",false);
        component.set("v.EmailArrow",false);
        component.set("v.TitleArrow",true);
       
    },
    sortByEmail: function(component, event, helper) {
        component.set("v.sortByFieldName", "Email");
        
        var field = component.get("v.sortByFieldName");
        // toggle sort 
        var sortAsc = component.get("v.sortAsc");
        var sortField = component.get("v.sortField");
        var sortAsc = sortAsc ? false : true;
        component.set("v.sortAsc", sortAsc);
        
        helper.sortBy(component,helper, field);
        var a=component.get("v.sortAsc");
        component.set("v.email",a);
        component.set("v.NameArrow",false);
        component.set("v.PhoneArrow",false);
        component.set("v.ProfileArrow",false);
        component.set("v.EmailArrow",true);
        component.set("v.TitleArrow",false);
       
    },
    sortByProfile : function(component, event, helper) {
        component.set("v.sortByFieldName", "ProfileName");
        
        var field = component.get("v.sortByFieldName");
        // toggle sort 
        var sortAsc = component.get("v.sortAsc");
        var sortField = component.get("v.sortField");
        var sortAsc = sortAsc ? false : true;
        component.set("v.sortAsc", sortAsc);
        
        helper.sortBy(component,helper, field);
        var a=component.get("v.sortAsc");
        component.set("v.Profile",a);
        component.set("v.NameArrow",false);
        component.set("v.ProfileArrow",true);
        component.set("v.ProfileArrow",false);
        component.set("v.EmailArrow",false);
        component.set("v.TitleArrow",false);
       
    },
})