({
   init: function (component, event, helper) 
    {
        helper.getMonitoringData(component);
        

    },
    
    openCreateModal: function(component, event, helper) 
    {
       
       component.set("v.newCommentBody", '');
        component.set("v.newIsPublic", true);
      // Set isModalOpen attribute to true
      component.set("v.isCreateModalOpen", true);
   },
  
   closeCreateModal: function(component, event, helper) 
    {
      // Set isModalOpen attribute to false  
      component.set("v.isCreateModalOpen", false);
   },
    openListModal: function(component, event, helper) 
    {
       
       //component.set("v.newCommentBody", '');
       // component.set("v.newIsPublic", true);
      // Set isModalOpen attribute to true
      component.set("v.isListModalOpen", true);
   },
  
   closeListModal: function(component, event, helper) 
    {
      // Set isModalOpen attribute to false  
      component.set("v.isListModalOpen", false);
        helper.getMonitoringData(component);
   },
  
   submitCreateDetails: function(component, event, helper) 
    {
      helper.submitCreateDetails(component, event, helper);
        
               
   },
   
    
    handleViewAllClick: function (component, event, helper) 
    {


        if(!component.get("v.isCommunityUser"))
        {
            if (false)
            {
             component.set("v.isListModalOpen", true);   
            }else
            {
            var evt = $A.get("e.force:navigateToComponent");
           evt.setParams({
                componentDef : "c:CommentsListView",
                componentAttributes: {
                    parentId : component.get("v.recordId"),
                    CaseNumber : component.get("v.CaseNumber")
                }
            });
            evt.fire();
            }
        } 
        else 
        {
        	// Careful, this related list may not filter out public.... 
           // var relatedListEvent = $A.get("e.force:navigateToRelatedList");
        	//relatedListEvent.setParams({
            //	"relatedListId": "Case_Comments__r",
            //	"parentRecordId": component.get("v.recordId")
        	//});
        	//relatedListEvent.fire();
        	//
        	//
        	//
        	//var strCaseId = component.get("v.recordId");
            //var strCaseNumber = component.get("v.CaseNumber");
            
			//var appEvent = $A.get("e.c:CommunityAttributesEvent");
    		//appEvent.setParams({
        	//	"CaseId" : strCaseId, 
            //    "CaseNumber" : strCaseNumber
            //});
   			// appEvent.fire();

			//go to list            
            //var urlEvent = $A.get("e.force:navigateToURL");
   			// urlEvent.setParams({
		    //  "url": "/case-comments-list"
   			// });
			// urlEvent.fire();
   			// 
   			// lightning-navagation now supported in spring 19 ?  using web com?? 
   			// 
   			// 
   			
   			      component.set("v.isListModalOpen", true);

        }

        
    }, 



})