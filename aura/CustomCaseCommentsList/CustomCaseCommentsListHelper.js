({
   getMonitoringData : function(component) {
        var caseId = component.get("v.recordId");
		console.log('entered casecomments getData, caseId - '+ caseId); 
		       
        var action = component.get('c.GetCaseCommentCaseMonitoring');
        action.setParams({
            "caseId": caseId
        });
        action.setCallback(this, $A.getCallback(function (response) 
        {
            var state = response.getState();
            if (state === "SUCCESS") {
                var commentslist = response.getReturnValue();

                component.set("v.commentsdata", commentslist);
               	var element;
                for (var i = 0; i < commentslist.length; i++) 
                {
                    element = commentslist[i];
                    component.set("v.isCommunityUser", element.IsCommunityUser);
                    component.set("v.CaseNumber", element.CaseNumber);
				}                



            } else if (state === "ERROR") {
                console.log('state - '+ state);
                var errors = response.getError();
                console.error(errors);
            }
        }));
        $A.enqueueAction(action);
    }, 
    
     submitCreateDetails: function(component, event, helper) 
    {
      console.log('submitCreateDetails  ' + component.get("v.recordId"));
      var caseId = component.get("v.recordId");
      var varcommentBody = component.get("v.newCommentBody");
      var varpublished = component.get("v.newIsPublic");
        // Prepare the action to create new case comment
        var addCommentAction = component.get('c.addCaseComment');
        addCommentAction.setParams({
            "commentBody": varcommentBody,
            "published": varpublished,
            "caseId": caseId
            
        });
        
        // Configure the response handler for the action
        addCommentAction.setCallback(this, $A.getCallback(function (response) 
        {
            var state = response.getState();
            if(state === "SUCCESS") 
            {
               //$A.get("e.force:refreshView").fire();
               // 
                 
                 helper.getMonitoringData(component);
            }
            else if (state === "ERROR") 
            {
                console.log('Problem saving contact, response state: ' + state);
                
            }
            else 
            {
                console.log('Unknown problem, response state: ' + state);
                
            }
            
        }));
        $A.enqueueAction(addCommentAction);
      	component.set("v.isCreateModalOpen", false);
         
       
   },
 
})