({
   init: function (component, event, helper) 
    {
      
    },
    
     handleSelect: function (component, event, helper) 
    {
        // This will contain the string of the "value" attribute of the selected
        // lightning:menuItem
        var selectedMenuItemValue = event.getParam("value");
        if (selectedMenuItemValue = 'Edit')
        {
              component.set("v.EditPublicFlag", component.get("v.PublicFlag"));
              component.set("v.EditCommentBody", component.get("v.CommentBody"));
              component.set("v.EditCommentReviewComplete", component.get("v.CommentReviewComplete"));

              // Set isModalOpen attribute to true
      		component.set("v.isEditModalOpen", true);
        }
        if (selectedMenuItemValue = 'Delete')
        {
            
        }
        if (selectedMenuItemValue = 'Close')
        {
            
        }
        //alert("Menu item selected with value: " + selectedMenuItemValue);
    },
    openEditModal: function(component, event, helper) 
    {
        

        
      component.set("v.EditPublicFlag", "v.PublicFlag");
      component.set("v.EditCommentBody", "v.CommentBody");
      component.set("v.EditCommentReviewComplete", "v.CommentReviewComplete");

        // Set isModalOpen attribute to true
      component.set("v.isEditModalOpen", true);
   },
  
   closeEditModal: function(component, event, helper) 
    {
      // Set isModalOpen attribute to false  
      component.set("v.isEditModalOpen", false);
        // force a refresh here to clean up changes
        $A.get("e.force:refreshView").fire();
   },
  
    submitEditDetails: function(component, event, helper) 
    {
     	helper.submitEditDetails(component, event, helper);
        

        
        // Set isModalOpen attribute to false
      //Add your code to call apex method or do some processing
      component.set("v.isEditModalOpen", false);
   },
    
   handleReviewCompleteChange: function(component, event, helper) 
    {
                
        helper.UpdateReviewComplete(component);
    }, 
    
   handlePublicChange: function(component, event, helper) 
    {
                
        helper.UpdatePublic(component);
    } 

})