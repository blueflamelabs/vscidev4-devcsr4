({
    submitEditDetails: function(component, event, helper) 
    {
      console.log('submitEditDetails  ' + component.get("v.recordId"));
      var varcasecommentId = component.get("v.CaseCommentId");
   		//var varCommentBody = component.get("v.CommentBody"); 
        //var varpublished = component.get("v.PublicFlag");
        //var bReviewComplete = component.get("v.CommentReviewComplete");
        
   		var varCommentBody = component.get("v.EditCommentBody"); 
        var varpublished = component.get("v.EditPublicFlag");
        var bReviewComplete = component.get("v.EditCommentReviewComplete");
        
        // Prepare the action to create new case comment
        var addCommentAction = component.get('c.EditCaseComment');
        addCommentAction.setParams({
            "commentBody": varCommentBody,
            "published": varpublished,
            "casecommentId": varcasecommentId,
            "bReviewComplete": bReviewComplete
        });
        
        // Configure the response handler for the action
        addCommentAction.setCallback(this, $A.getCallback(function (response) 
        {
            var state = response.getState();
            if(state === "SUCCESS") 
            {
                  component.set("v.PublicFlag", component.get("v.EditPublicFlag"));
                  component.set("v.CommentBody", component.get("v.EditCommentBody"));
                  component.set("v.CommentReviewComplete", component.get("v.EditCommentReviewComplete"));

                $A.get("e.force:refreshView").fire();
               // 
                
            }
            else if (state === "ERROR") 
            {
                console.log('Problem saving contact, response state: ' + state);
                
            }
            else 
            {
                console.log('Unknown problem, response state: ' + state);
                
            }
            
        }));
        $A.enqueueAction(addCommentAction);
      component.set("v.isEditModalOpen", false);
         
       
   },
 
     UpdateReviewComplete : function(component) 
    {
        var casecommentId = component.get("v.CaseCommentId");
        var bReviewComplete = component.get("v.CommentReviewComplete");
        
		console.log('entered casecommentId - '+ casecommentId); 
		console.log('entered bReviewComplete - '+ bReviewComplete); 
        
        var action = component.get('c.UpdateReviewCompleteMonitoring');
        action.setParams(
        {
            "CaseCommentId": casecommentId,
            "bReviewComplete": bReviewComplete,
            "strCommunicationType": "Case Comment"
        });
        action.setCallback(this, $A.getCallback(function (response) 
        {
            var state = response.getState();
            if (state === "SUCCESS") 
            {
                
                $A.get("e.force:refreshView").fire();
                
                

            } 
            else if (state === "ERROR")
            {
                //console.log('state - '+ state);
                var errors = response.getError();
                console.error(errors);
               
            }
        }));
        $A.enqueueAction(action);
    },
     UpdatePublic : function(component) 
    {
        var casecommentId = component.get("v.CaseCommentId");
        var bPublicFlag = component.get("v.PublicFlag");
        
		console.log('entered casecommentId - '+ casecommentId); 
		//console.log('entered bReviewComplete - '+ bReviewComplete); 
        
        var action = component.get('c.UpdateComment');
        action.setParams(
        {
            "CaseCommentId": casecommentId,
            "bPublished": bPublicFlag
            
        });
        action.setCallback(this, $A.getCallback(function (response) 
        {
            var state = response.getState();
            if (state === "SUCCESS") 
            {
                
                $A.get("e.force:refreshView").fire();
                
                

            } 
            else if (state === "ERROR")
            {
                //console.log('state - '+ state);
                var errors = response.getError();
                console.error(errors);
               
            }
        }));
        $A.enqueueAction(action);
    },
    
    
})