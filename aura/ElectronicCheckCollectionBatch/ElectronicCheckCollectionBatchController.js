({
	myAction : function(component, event, helper) 
    {
		
	},
    // Load  from Salesforce
     doInit: function(component, event, helper) 
    {
        helper.GetContactId(component);
        if (true)
        {
        	helper.GetContactInfo(component); 
        	helper.GetContactAccount(component);     
        }
        else
        {            
        	helper.GetUserInfo(component); 
        	helper.GetUserAccount(component); 
        }
        
        helper.GetSubmissionDeadlinePassed(component); 
        // save the action here in case they use default
		
 
    },
   
    SetActionRequested: function(component, event, helper)
    {
           	var varBatch = component.get("v.objBatch");
                
            var varAction = component.get("v.radioGrpValue");
            
            if (varAction == 'Batch Deletion')
            {
                varBatch.Action_Requested__c = 'Batch Delete';    
            }
            if (varAction == 'Item Deletion')
            {
                varBatch.Action_Requested__c = 'Item Delete';    
            }
            if (varAction == 'Item Correction')
            {
                varBatch.Action_Requested__c = 'Item Correction';    
            }
     
        	component.set("v.objBatch", varBatch);
        
    },
    
    EnableAppropriateFields: function(component, event, helper) 
    {
        
            var varBatch = component.get("v.objBatch");
                
            var varAction = component.get("v.radioGrpValue");
            
            if (varAction == 'Batch Deletion')
            {
                varBatch.Action_Requested__c = 'Batch Delete';    
            }
            if (varAction == 'Item Deletion')
            {
                varBatch.Action_Requested__c = 'Item Delete';    
            }
            if (varAction == 'Item Correction')
            {
                varBatch.Action_Requested__c = 'Item Correction';    
            }
     
        	component.set("v.objBatch", varBatch);

        var radioGrpValue = component.get("v.radioGrpValue");
        if (radioGrpValue == 'Batch Deletion') 
        {
          component.find("itemamount").set("v.disabled", true);
          component.find("itemcorrectamount").set("v.disabled", true);
          component.find("itemlastfourofaccountnumber").set("v.disabled", true);
          component.find("itemroutingnumber").set("v.disabled", true);
          component.find("itemchecknumber").set("v.disabled", true);
          component.find("itemnewbatchtotal").set("v.disabled", true);
         
          	var varBatchItem = component.get("v.objBatchItem");
			varBatchItem.Amount__c = null;
			varBatchItem.Correct_Amount__c = null;
			varBatchItem.Last_Four_of_Item_Account_Number__c = null;
			varBatchItem.Routing_Number__c = null;
			varBatchItem.Check_Number__c = null;
			varBatchItem.New_Batch_Total__c = null;            
            component.set("v.objBatchItem", varBatchItem);
            
          component.find("itemamount").set("v.value", null);
          component.find("itemcorrectamount").set("v.value", null);
          component.find("itemlastfourofaccountnumber").set("v.value", null);
          component.find("itemroutingnumber").set("v.value", null);
          component.find("itemchecknumber").set("v.value", null);
          component.find("itemnewbatchtotal").set("v.value", null);


        } 
        else if (radioGrpValue == 'Item Deletion') 
        {
          component.find("itemamount").set("v.disabled", false);
          component.find("itemcorrectamount").set("v.disabled", true);
          component.find("itemlastfourofaccountnumber").set("v.disabled", false);
          component.find("itemroutingnumber").set("v.disabled", false);
          component.find("itemchecknumber").set("v.disabled", false);
          component.find("itemnewbatchtotal").set("v.disabled", false);

          	var varBatchItem = component.get("v.objBatchItem");
			varBatchItem.Amount__c = null;
			varBatchItem.Correct_Amount__c = null;
			varBatchItem.Last_Four_of_Item_Account_Number__c = null;
			varBatchItem.Routing_Number__c = null;
			varBatchItem.Check_Number__c = null;
			varBatchItem.New_Batch_Total__c = null;            
            component.set("v.objBatchItem", varBatchItem);
            
            
          component.find("itemamount").set("v.value", null);
          component.find("itemcorrectamount").set("v.value", null);
          component.find("itemlastfourofaccountnumber").set("v.value", null);
          component.find("itemroutingnumber").set("v.value", null);
          component.find("itemchecknumber").set("v.value", null);
          component.find("itemnewbatchtotal").set("v.value", null);


        }
        else if (radioGrpValue == 'Item Correction') 
        {
          component.find("itemamount").set("v.disabled", false);
          component.find("itemcorrectamount").set("v.disabled", false);
          component.find("itemlastfourofaccountnumber").set("v.disabled", false);
          component.find("itemroutingnumber").set("v.disabled", false);
          component.find("itemchecknumber").set("v.disabled", false);
          component.find("itemnewbatchtotal").set("v.disabled", false);

                      	var varBatchItem = component.get("v.objBatchItem");
			varBatchItem.Amount__c = null;
			varBatchItem.Correct_Amount__c = null;
			varBatchItem.Last_Four_of_Item_Account_Number__c = null;
			varBatchItem.Routing_Number__c = null;
			varBatchItem.Check_Number__c = null;
			varBatchItem.New_Batch_Total__c = null;            
            component.set("v.objBatchItem", varBatchItem);
            

          component.find("itemamount").set("v.value", null);
          component.find("itemcorrectamount").set("v.value", null);
          component.find("itemlastfourofaccountnumber").set("v.value", null);
          component.find("itemroutingnumber").set("v.value", null);
          component.find("itemchecknumber").set("v.value", null);
          component.find("itemnewbatchtotal").set("v.value", null);

        }
    },
     
     throwErrorForDeadlinePassed: function(cmp) 
    {
        // BMM Note must be cmp as parameter
        // this sample always throws an error to demo try/catch5
        var hasPerm = false;
        try 
        {
            if (!hasPerm) {
                throw new Error("Items can only be deleted/corrected the same day they are received by Synergent");
            }
        }
        catch (e) 
        {
            $A.createComponents([
                ["ui:message",{
                    "title" : "FORM SUBMISSION DEADLINE HAS PASSED !!!",
                    "severity" : "error",
                }],
                ["ui:outputText",{
                    "value" : e.message
                }]
                ],
                function(components, status, errorMessage){
                    if (status === "SUCCESS") 
                    {
                        var message = components[0];
                        var outputText = components[1];
                        // set the body of the ui:message to be the ui:outputText
                        message.set("v.body", outputText);
                        var div1 = cmp.find("div1");
                        // Replace div body with the dynamic component
                        div1.set("v.body", message);
                        var div2 = cmp.find("div2");
                        // Replace div body with the dynamic component
                        div2.set("v.body", message);
                        
                        component.find("SubmitButton").set('v.disabled', true);
                    }
                    //else if (status === "INCOMPLETE") {
                    //    console.log("No response from server or client is offline.")
                        // Show offline error
                    //}
                    //else if (status === "ERROR") {
                    //    console.log("Error: " + errorMessage);
                        // Show error message
                    //}
                }
            );
        }
    }

})