({
    
    doInit : function(component, event, helper) {
       
         
        var action = component.get("c.GetBatch");
        action.setParams({
            recordId: component.get("v.recordId")
            
        });
        action.setCallback(this, function(response){
            var obj = response.getReturnValue();
            component.set("v.objBatch", obj);
                       	var varBatch = component.get("v.objBatch");

             if (varBatch.Action_Requested__c == 'Batch Delete')
            {
                component.find("viewFormBatchItem").set("v.mode", "readonly"); 
                component.find("viewFormBatchItem").set("v.class", "slds-hide");     

            }
            else
            {
               component.find("viewFormBatchItem").set("v.mode", "view");     
                
            }

            
        });
        $A.enqueueAction(action);
        
        
        var actionItem = component.get("c.GetBatchItem");
        actionItem.setParams({
            recordId: component.get("v.recordId")
            
        });
        actionItem.setCallback(this, function(response)
        {
            var obj = response.getReturnValue();
            component.set("v.objBatchItem", obj);
            
        });
        $A.enqueueAction(actionItem);

    },

    
    navToRecord : function (component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.objBatch.Id")
        });
            navEvt.fire();
    },
    editRecord : function(component, event, helper) {
       // helper.showHide(component);
        //bmm add
        //     var navEvt = $A.get("e.force:navigateToSObject");
       /// navEvt.setParams({
       //     "recordId": component.get("v.objBatch.Id")
       // });
       //     navEvt.fire();
   
        
    },
    handleSuccess : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({"title": "Success!","message": "The batch's info has been updated.","type": "success"});toastEvent.fire();
        //helper.showHide(component);
    },
    handleLoad : function(component, event, helper) 
    {
        //var toastEvent = $A.get("e.force:showToast");
       // toastEvent.setParams({
       //     "title": "Loaded!",
       //     "message": "The record has been Loaded successfully ."
       // });
       // toastEvent.fire(); 
        //helper.showHide(component);
    },
    handleError : function(component, event, helper) 
    {
var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error!",
            "message": "Error."
        });
        toastEvent.fire();       
        // helper.showHide(component);
    },

    handleCancel : function(component, event, helper) {
       // helper.showHide(component);
       // event.preventDefault();
    },
    handleSaveWithItem : function(component, event, helper) 
    {
        //helper.showHide(component);
        //event.preventDefault();
    }

})