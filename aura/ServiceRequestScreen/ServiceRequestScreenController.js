({
		    // Load  from Salesforce
     init: function(component, event, helper) 
    {
         if(component.get("v.ServiceArea") != '' )
        {
            helper.getCategorys(component, event, helper);
        }
            
         if(component.get("v.Category") != '' )
        {
            helper.getSubCategorys(component, event, helper);
        }
           
       // Set the validate attribute to a function that includes validation logic
       component.set('v.validate', function() 
       {
           var userInputSubject = component.get('v.Subject');
           if(userInputSubject && userInputSubject.length>0) 
           {
               // If the component is valid...
               //return { isValid: true };
           }
           else 
           {
               // If the component is invalid...
               return { isValid: false, errorMessage: 'A subject is required.' };
           }
           var userInputDescription = component.get('v.Description');
           if(userInputDescription && userInputDescription.length>0) 
           {
               // If the component is valid...
               //return { isValid: true };
           }
           else 
           {
               // If the component is invalid...
               return { isValid: false, errorMessage: 'A description is required.' };
           }
          var userInputServiceArea = component.get('v.ServiceArea');
           if(userInputServiceArea && userInputServiceArea.length>0 && userInputServiceArea != '') 
           {
               // If the component is valid...
               //return { isValid: true };
           }
           else 
           {
               // If the component is invalid...
               return { isValid: false, errorMessage: 'A service area is required.' };
           }
           

           var userInputCategory = component.get('v.Category');
           if(userInputCategory && userInputCategory.length>0) 
           {
               // If the component is valid...
               //return { isValid: true };
           }
           else 
           {
               // If the component is invalid...
               return { isValid: false, errorMessage: 'A category is required.' };
           }
           
           
           
           
           // made it here, should be ok
			return { isValid: true };               

       })

    },
    
        // 
    
 updateCategorys : function(component, event, helper) 
    {
        //
         if(component.get("v.ServiceArea") != '' )
        {
            helper.getCategorys(component, event, helper);
        }
        // 
        
	},
    
updateSubCategorys : function(component, event, helper) 
    {
        //
         if(component.get("v.Category") != '' )
        {
            helper.getSubCategorys(component, event, helper);
        }
        
	}

})