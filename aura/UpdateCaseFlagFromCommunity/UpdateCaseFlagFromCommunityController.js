({
	myAction : function(component, event, helper) {
		
	},
    doInit : function(component, event, helper) {
        helper.getCaseFlagValue(component, event, helper);
    },
    updateCaseFlag : function(component, event, helper) {
        var getFlagVal = event.getSource().get("v.value");
        // console.log('getFlagVal========'+getFlagVal);
        var action = component.get("c.updateCaseFlagValue");
        action.setParams({
            caseId : component.get('v.recordId'),
            caseFlagVal : getFlagVal
        });  
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            if( state === 'SUCCESS'){
                var resValue = response.getReturnValue();
                console.log('--resValue--',resValue);
                helper.showSuccessToastfire(resValue);
            }
            else if(state === 'ERROR') {
                
            }
            
        });
        $A.enqueueAction(action);
    }
})