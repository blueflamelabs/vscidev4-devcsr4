({
	helperMethod : function() {
		
	},
    getCaseFlagValue: function(component, event, helper) {
        var action = component.get("c.getCaseFlag");
        action.setParams({
            caseId : component.get('v.recordId')
        });       
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            if( state === 'SUCCESS'){
                var resValue = response.getReturnValue();
                console.log('--resValue--',resValue);
                component.set("v.isCaseUpdated",resValue);
                console.log('isCaseUpdated=======',component.get("v.isCaseUpdated"));
            }
            
        });
        $A.enqueueAction(action);
    },
    showSuccessToastfire : function(msg) {
       console.log('success-');
       var toastEvent = $A.get("e.force:showToast");
       toastEvent.setParams({
           title : 'Success',
           message: msg,
           duration:'5000',
           type: 'success'
       });
       toastEvent.fire();
    }
})