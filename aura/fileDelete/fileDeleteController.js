({
	init: function (cmp, event, helper) {
        
        console.log('---------filesList:::',cmp.get("v.fileIds"));
    },
    
    deleteSelectedFile : function(component, event, helper){
        var rowId = component.get("v.fileIds");
        console.log("rowid18-- : "+rowId);
        var action = component.get("c.deleteFile");
        action.setParams({
            "fileId": rowId
        });    
        
        action.setCallback(this, function(response) {
            var state =  response.getState();
            if(state == "SUCCESS") {
                var resultObj = response.getReturnValue();
                console.log('--resultObj--',resultObj);
                helper.showDelMsgToastfire(resultObj);
                component.set("v.deleteFile",false);
                component.find("overlayLib").notifyClose();
                component.set("v.EnableDoInit",true);
                 $A.get('e.force:refreshView').fire();
            }
        });
        
        $A.enqueueAction(action);
       
    },
    
    closeDeleteModel : function(component, event, helper){
        console.log('--41--');
        component.find("overlayLib").notifyClose();
    },
})