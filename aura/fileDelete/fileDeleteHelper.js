({
	showDelMsgToastfire : function(msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Success',
            message: msg,
            duration:'3000',
            type: 'Success'
        });
        toastEvent.fire();
        },
})