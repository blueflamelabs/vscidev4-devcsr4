({
	handleChange : function(component, event, helper) {
        
        component.set("v.change",true);
	},
    
    handleCapture : function(cmp,event,helper){
       // var type = cmp.find("type").get("v.value");
        var record = cmp.get("v.record");
        
     /*   if(!type){
             console.log('type',type);
            cmp.find('notifLib').showToast({
                "variant":'error',
                "title": "Required field missing",
                "message": "Select a type and try again"
            });
            
            var appEvent = $A.get("e.c:fileValidationError");
        	appEvent.setParams({ "message" : "fail" });
			appEvent.fire();
           
            //return;

        }*/
        if(event.getParam("message") == "save" && cmp.get("v.change") == true ) {
            
            // 120619 - T - 00090 - Added to validate the SharingPrivacy field when loggedin user is internal user
            if(cmp.get("v.isCommUser") == false) {
                var getSharingPrivacy = cmp.find("sharingPrivacy").get("v.value");
            	console.log('getSharingPrivacy=======',getSharingPrivacy);
            } else {
                var getSharingPrivacy = '';
            }
            
            if(cmp.get("v.isCommUser") == false && (getSharingPrivacy == 'N' || getSharingPrivacy == 'P')) {
                console.log('Inside If');
            	cmp.find('recordViewForm').submit();
            	var appEvent = $A.get("e.c:fileValidationError");
        		appEvent.setParams({ "message" : "save" });
				appEvent.fire();
            } else if(cmp.get("v.isCommUser") == true) {
                console.log('Inside else if');
                cmp.find('recordViewForm').submit();
            	var appEvent = $A.get("e.c:fileValidationError");
        		appEvent.setParams({ "message" : "save" });
				appEvent.fire();
            } else if((getSharingPrivacy != 'N' || getSharingPrivacy != 'P') && cmp.get("v.isCommUser") == false) {
                console.log('Inside else');
                //120619- T-00090 - Display validation error when SharingPrivacy option
    			// is not selected and loggedin user is internal user
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Error',
                    message: 'File Privacy on Records field is required',
                    mode: 'sticky',
                    type: 'Error'
                });
                toastEvent.fire();
            }
            
        }
        
    },   
})