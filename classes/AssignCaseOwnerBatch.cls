/********************************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                 ModifiedDate  ModifiedBy              Description
*       1.0      221019         VennScience_BFL_Monali                                        This Batch is built to update the case owner.
                               
*********************************************************************************************************************************************************************/ 
global class AssignCaseOwnerBatch implements Database.Batchable<Sobject> {
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        // Fetch custom label
        //String developmentRequest = Label.CSRRecordTypeLabel;
        // Fetch the record type name.
        Id developmentRequestRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName()
                                                       .get(Label.CSRRecordTypeLabel)
                                                       .getRecordTypeId();
        System.debug('developmentRequestRecordTypeId === >'+developmentRequestRecordTypeId);
        String csrType = 'CSR';
        String fixType = 'Fix';
        String quickType = 'Quick';
        
        String query = 'SELECT Id,'
                              +'OwnerId,'
                              +'RecordTypeId,'
            				  +'Owner.Name,'
                              +'Type '
                        +' FROM Case'            
                        +' WHERE RecordTypeId = :developmentRequestRecordTypeId'
                        +' AND (Type = :csrType OR Type = :fixType OR Type = :quickType)';
        System.debug('query === >'+query);
        return Database.getQueryLocator(query); 
        
    }
    
    global void execute(Database.BatchableContext BC, List<Case> listCaseRecords) {
        System.debug('listCaseRecords === >'+listCaseRecords);
        
        // 221019 - T-000740 - VennScience_BFL_Monali - Call handler 
        AssignCaseOwnerBatchHandler.autoAssignCaseOwner(listCaseRecords);  
    }
    global void finish(Database.BatchableContext bc) {   
    }   

}