/********************************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                 ModifiedDate  ModifiedBy              Description
*       1.0      221019         VennScience_BFL_Monali                  VennScience_BFL_Monali  This Batch is built to assign the case owner. 
                               
*********************************************************************************************************************************************************************/  
public class AssignCaseOwnerBatchHandler {
    
    /**
    * Method Name : autoAssignCaseOwner
    * Parameters  : param1: List<Case>
    * Description : Used to update the case owner.
    * Created By  : VennScience_BFL_Monali 
    * Created On  : 221019
    **/
    public static void autoAssignCaseOwner(List<Case> listCaseRecord) {
        
        System.debug('inside handler listCaseRecord === >'+listCaseRecord);
        // variable declaration.
        String queueRecord = '';
        List<Case> assignCaseOwnerRecordlist = new List<Case>();
        // get custom label .
        //String sprintQueue = Label.Sprint_Queue;
        //System.debug('sprintQueue === >'+sprintQueue);
        // fetch the 'Sprint Queue' queue recod.
        List<Group> groupRecordList = [SELECT Id, 
                                              Name 
                                      FROM Group 
                                      WHERE Name = :Label.Sprint_Queue 
                                      AND Type = 'Queue'];
        
        System.debug('groupRecordList === >'+groupRecordList);
        // if group record is not null.
        if(!groupRecordList.isEmpty()) {
            queueRecord = groupRecordList[0].Id;
        }// End of if.
        if(!listCaseRecord.isEmpty()) {
            // Iterate over the case list.
            for(Case caseRecord : listCaseRecord) {
                if(!caseRecord.Owner.Name.equals('Sprint Backlog') && caseRecord.Owner.Name != 'Triage Queue') { 
                    caseRecord.OwnerId = queueRecord;
                    assignCaseOwnerRecordlist.add(caseRecord);
                }// End of if.
            }// End of for.
        }// End of if.
        try {
            // if case list not null.
            if(!assignCaseOwnerRecordlist.isEmpty()) {
                System.debug('assignCaseOwnerRecordlist === >'+assignCaseOwnerRecordlist);
                update assignCaseOwnerRecordlist;
                System.debug('after assignCaseOwnerRecordlist === >'+assignCaseOwnerRecordlist);
            }//End of if.
        }catch(Exception ex) {
            System.debug('ex === >'+ex.getMessage());
        }//End of try catch.
    }

}