/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                    Description
*       1.0         221019      VennScience_BFL_Monali       This Scheduler is built to execute the batch apex
**********************************************************************************************************************************************************/
global class AssignCaseOwnerScheduler implements Schedulable {
    
    global void execute(SchedulableContext ctx) {
        AssignCaseOwnerBatch objBatch = new AssignCaseOwnerBatch();
        Database.executeBatch(objBatch);
        
    }
    
}