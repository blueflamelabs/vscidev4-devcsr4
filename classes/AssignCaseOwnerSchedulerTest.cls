@isTest
public class AssignCaseOwnerSchedulerTest {
    @testSetup static void schedulerTest() {
        List<Case> caseList = new List<Case>();
        
        UserRole role1 = new UserRole(DeveloperName = 'rams', Name = 'CEO');
        insert role1;
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Development Request').getRecordTypeId();                
        
        User recOwner = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'Sprint Backlog',
            Email = 'testUser1@gmail.com',
            Username = 'testUser1@amama.com' + System.currentTimeMillis(),
            CompanyName = 'testUser',
            Title = 'Developer',
            Alias = 'Alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = role1.Id          
        ); 
        insert recOwner;	 // create a owner of the record
        
        Account newAcc = new Account(); // create account
        newAcc.Name = 'newAcc1';
        insert newAcc;
        
        Case createCase1 = new Case(); //case 1
        createCase1.AccountId = newAcc.Id ;
        createCase1.OwnerId = recOwner.Id;
        createCase1.RecordTypeId  = caseRecordTypeId; 
        createCase1.Subject = 'case subject';
        createCase1.Description = 'case Description';
        createCase1.Type = 'CSR';// CSR, Quick or Fix 
        createCase1.Origin = 'Email';
        createCase1.Category__c = 'eDesk';
        createCase1.Status = 'Coding';
        createCase1.Sub_status__c = 'Sitter';
        createCase1.Estimated_Project_Hours__c = 23;
        createCase1.Signed_CSR_Date__c = System.today() + 5;
        caseList.add(createCase1);
        
        Case createCase2 = new Case(); // case 2
        createCase2.AccountId = newAcc.Id  ;
        createCase2.OwnerId = recOwner.Id;
        createCase2.RecordTypeId  = caseRecordTypeId; 
        createCase2.Subject = 'case subject';
        createCase2.Description = 'case Description';
        createCase2.Type = 'Fix';// CSR, Quick or Fix 
        createCase2.Origin = 'Email';
        createCase2.Category__c = 'eDesk';
        createCase2.Status = 'Coding';
        createCase2.Sub_status__c = 'Sitter';
        createCase2.Estimated_Project_Hours__c = 23;
        createCase2.Signed_CSR_Date__c = System.today() + 5;
        caseList.add(createCase2);
        insert caseList;
    }
    @isTest static void testschedular() {
        List<Case> fetchCase = new  List<Case>([SELECT Id ,OwnerId FROM Case]);
        System.debug('fetchCase >>'+fetchCase); 
        System.assert(fetchCase.size() == 0 ,'Case records are not present');
        String CRON_EXP = '0 59 23 * * 7 *';
 
        Test.startTest();
        String jobId = System.schedule('ScheduleApexClassTest',  CRON_EXP, new AssignCaseOwnerScheduler());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        Test.stopTest();        
    }
}