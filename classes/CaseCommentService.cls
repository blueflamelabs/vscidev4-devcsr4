public class CaseCommentService 
{
    /*
    public static Map<Id, Case_Monitoring__c> contentDocumentWithCaseMonitoringRec = new Map<Id, Case_Monitoring__c>();
    public static Map<Id, Case> contentDocumentIdCaseRec = new Map<Id, Case>();

    public static void contentDocumentWithCaseMonitoringRecMapping(List<syn_Case_Comment__c> contentRecords) 
    {

        Map<Id, Id> contentVersionIdWithContentDocumentId = new Map<Id, Id>();
        Set<Id> contentDocumentIds = new Set<Id>();
        for(syn_Case_Comment__c cv : contentRecords) 
        {
            
            if(cv.ContentDocumentId != null) {
                contentDocumentIds.add(cv.ContentDocumentId);
            }
        }
        
        for(ContentVersion cv : [SELECT Id, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId IN :contentDocumentIds AND IsLatest = true]) 
        {

            contentVersionIdWithContentDocumentId.put(cv.Id, cv.ContentDocumentId);
        }  

        for(Case_Monitoring__c cm : [SELECT Id, Parent_Id__c FROM Case_Monitoring__c WHERE Parent_Id__c IN :contentVersionIdWithContentDocumentId.keySet()]) 
        {

            if(String.isNotBlank(cm.Parent_Id__c) && contentVersionIdWithContentDocumentId.containsKey(cm.Parent_Id__c)) 
            {
                
                contentDocumentWithCaseMonitoringRec.put(contentVersionIdWithContentDocumentId.get(cm.Parent_Id__c), cm);
            }
        }     
    }

    public static void updateCaseMonitoringRecords(List<syn_Case_Comment__c> contentRecords) 
    {

        //Set<Id> contentDocumentIds = new Set<Id>();
        List<Case_Monitoring__c> caseMonitoringRecsToUpsert = new List<Case_Monitoring__c>();

        
        //system.debug('contentDocumentWithCaseMonitoringRec:'+contentDocumentWithCaseMonitoringRec);
        //system.debug('contentDocumentIdCaseRec:'+contentDocumentIdCaseRec);
        for(syn_Case_Comment__c cv : contentRecords) 
        {
            system.debug('cv:'+cv);
            Case_Monitoring__c cm = new Case_Monitoring__c();
            if(contentDocumentWithCaseMonitoringRec.containsKey(cv.ContentDocumentId)) 
            {

                cm = contentDocumentWithCaseMonitoringRec.get(cv.ContentDocumentId);
            }
            cm.Case__c = contentDocumentIdCaseRec.get(cv.ContentDocumentId).Id;
            cm.Parent_Id__c = cv.Id;
            cm.Communication_Type__c = 'File';
            cm.OwnerId = cv.OwnerId;
            cm.Review_Complete__c = false;
            if(contentDocumentIdCaseRec.get(cv.ContentDocumentId).OwnerId == cv.OwnerId) {
                cm.Review_Complete__c = true;
                cm.Acknowledge_Date__c = system.today();
            }
            cm.Name = 'Case: '+contentDocumentIdCaseRec.get(cv.ContentDocumentId).CaseNumber+' Case Comment';
            caseMonitoringRecsToUpsert.add(cm);
        }
        system.debug('caseMonitoringRecsToUpsert:'+caseMonitoringRecsToUpsert);
        if(caseMonitoringRecsToUpsert.size() > 0) {

            upsert caseMonitoringRecsToUpsert;
        }
    }

    public static List<syn_Case_Comment__c> getCaseTypeContentVersionRecords(List<syn_Case_Comment__c> newRecords) {

        Map<Id, List<syn_Case_Comment__c>> contentDocumentWithContentVersions = new Map<Id, List<syn_Case_Comment__c>>();
        Map<Id, Id> caseIdWithDocumentId = new Map<Id, Id>();
        List<syn_Case_Comment__c> recordsToProcess = new List<syn_Case_Comment__c>();
        Set<Id> caseIds = new Set<Id>();
        
        system.debug('newRecords:'+newRecords);
        for(syn_Case_Comment__c cvRecord : newRecords) {

            if(cvRecord.ContentDocumentId != null) {
                
                if(!contentDocumentWithContentVersions.containsKey(cvRecord.ContentDocumentId)) {

                    contentDocumentWithContentVersions.put(cvRecord.ContentDocumentId, new List<syn_Case_Comment__c>());
                }
                contentDocumentWithContentVersions.get(cvRecord.ContentDocumentId).add(cvRecord);
            }
        }
        
        system.debug('contentDocumentWithContentVersions:'+contentDocumentWithContentVersions);
        if(contentDocumentWithContentVersions.size() > 0) {

            for(ContentDocumentLink cdl : [
                SELECT Id, LinkedEntityId, ContentDocumentId 
                FROM ContentDocumentLink 
                WHERE ContentDocumentId IN :contentDocumentWithContentVersions.keySet() AND LinkedEntityId != null
            ]){
                
                system.debug('cdl:'+cdl);
                if(cdl.LinkedEntityId.getSObjectType() == Schema.getGlobalDescribe().get('Case')){
                    
                    recordsToProcess.addAll(contentDocumentWithContentVersions.get(cdl.ContentDocumentId));
                    caseIdWithDocumentId.put(cdl.LinkedEntityId, cdl.ContentDocumentId);
                    caseIds.add(cdl.LinkedEntityId);
                }
            } 
        }
        system.debug('caseIdWithDocumentId:'+caseIdWithDocumentId);
        system.debug('caseIds:'+caseIds);
        for(Case c : [SELECT Id, CaseNumber, OwnerId FROM Case WHERE Id IN :caseIds]) {

            contentDocumentIdCaseRec.put(caseIdWithDocumentId.get(c.Id), c);
        }
        system.debug('::::'+recordsToProcess.size()+':::recordsToProcess:'+recordsToProcess);
        return recordsToProcess;
    }

    
    Private static Boolean getIsCommunityUser()
    {
        List<User> uList = [select Id from user where id=:userinfo.getuserid() AND ContactId = NULL];
        
        if(uList.size() > 0){
           return false; 
        }
        return true;
    }
    
    public static void updateReviewStatus(List<syn_Case_Comment__c> newRecords)
    {
        
        Map<Id,ContentVersionWrapper> cvWrapperList = getCaseTypeRecords(newRecords);
        Boolean isCommunityUser = getIsCommunityUser();
        List<Case> caseListToUpdate = new List<Case>();
        List<syn_Case_Comment__c>  cvList = new List<syn_Case_Comment__c>();
        if(!isCommunityUser)
        {
            for(ContentVersionWrapper cvw : cvWrapperList.values())
            {
                for(syn_Case_Comment__c cv : cvw.cvRecords)
                {
                    if(!cv.File_Reviewed__c){
                        syn_Case_Comment__c c = new syn_Case_Comment__c();
                        c.Id = cv.Id;
                        c.File_Reviewed__c = true;
                        cvList.add(c);
                    } 
                } 
            }
            update cvList;
            system.debug('##3'+cvWrapperList.keySet());
            caseListToUpdate = getCaseReviewListToUpdate(cvWrapperList.keySet());
            system.debug('##3'+caseListToUpdate);
        } 
        else 
        {
            for(String caseId : cvWrapperList.keySet())
            {
                Case c = new Case();
                c.Id = caseId;
                c.Case_Communication_Flag__c = true;
                caseListToUpdate.add(c);
            }
            if(caseListToUpdate.size() > 0)
            {
                update caseListToUpdate;
            }
        }
    }
    
     public static void sendEmailNotification(List<ContentVersion> newRecords)
     {
         
        String strExternaltemplateApiName = 'Case_File_Notification_to_Contact_and_Customer_Case_Team';
         
        
        Id templateId,targetOwnerObjectId,targetContactObjectId,targetWhatId,CreatedByID;
        
        Boolean IsCommunityUser = false;
        Boolean IsCaseClosed = false;
        Boolean bToAddressFilled = false;
         
        
         try {
             templateId = [select id, name from EmailTemplate where developername = : strExternaltemplateApiName].id;}
         catch (Exception e)  {

         }

        //  Create a master list to hold the emails
        List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();
         
        Set<Id> contentDocumentIds = new Set<Id>();
         
        for(ContentVersion cvRecord : newRecords) {
            contentDocumentIds.add(cvRecord.ContentDocumentId);
        }
         
        Map<Id,Id> contentMapWithParent = new Map<Id,Id>();
        
        for(ContentDocumentLink cdl : [SELECT Id,LinkedEntityId,ContentDocumentId FROM ContentDocumentLink WHERE ContentDocumentId in:contentDocumentIds]){
            contentMapWithParent.put(cdl.ContentDocumentId, cdl.LinkedEntityId);
        }
        
        for (ContentVersion cvRecord : newRecords) {
            Messaging.SingleEmailMessage mail =  new Messaging.SingleEmailMessage();
            
            CreatedByID = cvRecord.CreatedById; 
            User objUser = [SELECT Id, ProfileId, ContactId from User where ID = : cvRecord.CreatedById];
            Profile objProfile = [SELECT Id, Name from Profile where ID = : objUser.ProfileId];
            
            if (objProfile.Name.Contains('Community'))
            {
                IsCommunityUser = true;    
            }
            if(!contentMapWithParent.containsKey(cvRecord.ContentDocumentId) || (contentMapWithParent.get(cvRecord.ContentDocumentId).getSObjectType() != Schema.getGlobalDescribe().get('Case'))){
                return;
            }
            targetWhatId = contentMapWithParent.get(cvRecord.ContentDocumentId);  
            Case objCase = [select id, OwnerId, ContactId, Status, CaseNumber from Case where ID = : contentMapWithParent.get(cvRecord.ContentDocumentId)];      
            
             targetOwnerObjectId = objCase.OwnerId;
        targetContactObjectId = objCase.ContactId;
 
        if (objCase.Status == 'Closed')
        {
            IsCaseClosed = true;    
        }
        if(Test.isRunningTest())
        {
            IsCommunityUser = false; 
        }

        // 
        if (IsCommunityUser == false)
        {
            mail.setTargetObjectId(targetContactObjectId);
            mail.setWhatId(targetWhatId);
            mail.setTemplateId(templateId);
            for(OrgWideEmailAddress owa : [select id, Address, DisplayName from OrgWideEmailAddress limit 1])
            mail.setorgWideEmailAddressId(owa.id);
            mail.setSaveAsActivity(false);          // must be false ...    
            // Add your email to the master list
            mails.add(mail);

        }
        
        if(Test.isRunningTest())
        {
            IsCommunityUser = true; //true false
        }
        
        if (IsCommunityUser == true)
        {
            //  list of internal people who should get the email
            List<String> sendTo = new List<String>();
            String strEmail;
            Id GroupId;
            //[Select Id from Group where type='Queue' and Name='Queue Name'] 
            //Get the queue
            List<Group> objQueues = [SELECT id FROM Group WHERE Id = :objCase.OwnerId ];    //and type='queue'
            for(Group objQueue : objQueues)
            {
                Id iQueueid = objQueue.id;
                // get the groups in the queue, do not worry about recursive looping for now
                List<GroupMember> objGroupMembersInQueue = [SELECT id, GroupId, UserOrGroupId FROM GroupMember WHERE GroupId = :iQueueid ];                      
                for(GroupMember objGroupMember : objGroupMembersInQueue)
                {
                    List<GroupMember> groupswithusers = [SELECT UserOrGroupId FROM GroupMember WHERE UserOrGroupId = : objGroupMember.UserOrGroupId];                   
                    for(GroupMember objGroupMemberUsers : groupswithusers)
                    {
         
                        List<User> usersingroup = [SELECT Email FROM User WHERE Id IN (
                          SELECT UserOrGroupId
                          FROM GroupMember
                          WHERE GroupId = :objGroupMemberUsers.UserOrGroupId )];

                        for(User u : usersingroup)
                        {                
                            sendTo.add(u.email);
                            bToAddressFilled = true;
                        }
                    }
                }
            }
          
            // this is the simple user as case owner
            List<User> users = [SELECT Email FROM User WHERE Id = :objCase.OwnerId];
            for(User u : users)
            {
                sendTo.add(u.email);
                bToAddressFilled = true;
            }

          
          
            //sendTo.add(strEmail);
            mail.setToAddresses(sendTo);
            // Set email is sent from
            //mail.setReplyTo('youremail@yourdomain.com');
            //mail.setSenderDisplayName('Your Name');
        
            // Set email contents
            String strSubject = 'Case #' + objCase.CaseNumber + ' has been updated.';

            mail.setSubject(strSubject);
          
            URL caseUrl = URL.getOrgDomainUrl();
            string strcaseUrl = caseUrl.toExternalForm() + '/' + objCase.Id;
            
            String body = 'Case #' + objCase.CaseNumber + ' has a new File from the community.' + '<br/>' + '<br/>';
            
            
            body += strcaseUrl;
          
            mail.setHtmlBody(body); 
            mail.setSaveAsActivity(false);          // must be false ...    
            // Add your email to the master list
            mails.add(mail);
  
        }
      
    
    }
    // Send all emails in the master list
    if ((mails.size()>0) && (IsCaseClosed == false) && (bToAddressFilled == true))
    {
        Messaging.sendEmail(mails);
    }
     }
*/
    public static void sendEmailNotificationOnCaseComment(List<syn_Case_Comment__c> caseCommentRecords) {
        //System.debug('Inside sendEmailNotificationOnCaseComment');
        //System.debug('caseCommentRecords======='+caseCommentRecords);
        // Variable declaration
        Boolean isCommunityUser = false;
        // Get email template's API names

        // Email template to be sent to Case.Contact when internal user creates a new comment
        String strCaseContactNotifyTemplateApiName = 'CaseCommentNotificationToCustomerCaseTeam';
        // Email template to be sent to Case.Owner when community user creates a new comment
        String strCaseOwnerNotifyTemplateApiName = 'Case_Comment_Notification_to_Case_Owner';
        //  Create a master list to hold the emails
        List<Messaging.SingleEmailMessage> mailList =  new List<Messaging.SingleEmailMessage>();
        Map<Id,List<GroupMember>>  groupMemberWithUserMap = new  Map<Id,List<GroupMember>>();
        Map<Id,List<GroupMember>>  groupMemberWithGroupMap = new  Map<Id,List<GroupMember>>();
        Map<Id, Set<String>>  caseEmailMap = new Map<Id, Set<String>>();
        Set<Id> setCaseId = new Set<Id>();
        Set<Id> caseOwnerIds = new Set<Id>();
        Set<Id> userOrGroupIds = new Set<Id>();
        Id targetWhatId;
        // To check if logged in User is community user or Internal user
        isCommunityUser = getIsCommunityUser();
        //System.debug('===isCommunityUser======'+isCommunityUser);
        // Fetch email templates
        
        // Get template id that is to be sent to Case.Contact when case comment is posted by internal user
        Id caseContactTemplateId = [SELECT    Id, 
                                    Name,
                                    Developername 
                                    FROM    EmailTemplate 
                                    WHERE    Developername = : strCaseContactNotifyTemplateApiName].Id;
        // Get template id that is to be sent to Case.Owner when case comment is posted by community user
        Id caseOwnerTemplateId = [SELECT    Id, 
                                  Name,
                                  Developername 
                                  FROM    EmailTemplate 
                                  WHERE    Developername = : strCaseOwnerNotifyTemplateApiName].Id;
        // Get orgwide email addresses
        Id orgWideAddress = [SELECT Id, 
                             Address, 
                             DisplayName 
                             FROM OrgWideEmailAddress 
                             LIMIT 1].Id;
        for(syn_Case_Comment__c objSyncComment : caseCommentRecords) {
            if(objSyncComment.Case__c != null) {
                setCaseId.add(objSyncComment.Case__c);
            }
        }
         if(setCaseId.isEmpty()) {
            return;
         } else {
             Map<Id,Case> mapCaseRelatedToCaseComments = new Map<Id,Case>([SELECT    Id, 
                                                                                     OwnerId, 
                                                                                     ContactId,
                                                                                     Account.Name,
                                                                                     Contact.Name,
                                                                                     Subject,
                                                                                     Status, 
                                                                                     CaseNumber 
                                                                             FROM    Case 
                                                                            WHERE    Id IN :setCaseId]);
             //System.debug('mapCaseRelatedToCaseComments======'+mapCaseRelatedToCaseComments);
             // Check if loggedin user is community User then send emails to Owners of related Case
            if(isCommunityUser == true) {
                
                
                // Get case owner Id in a set
                for(Case caseRecord : mapCaseRelatedToCaseComments.Values()){
                    caseOwnerIds.add(caseRecord.OwnerId);
                }
                 // Fetch case related group/queue
                Map<Id,Group> groupsMap = new Map<Id,Group>([SELECT Id 
                                                               FROM Group 
                                                              WHERE Id IN :caseOwnerIds]);
                // Fetch group members                                            
                Map<Id,GroupMember> groupMemberMap = new Map<Id,GroupMember>([SELECT Id, 
                                                                                     GroupId, 
                                                                                     UserOrGroupId 
                                                                                FROM GroupMember 
                                                                               WHERE GroupId IN :groupsMap.keySet()]);
                // 240619 - T - 00211 - Fetch members from public group that is part of queue(Case.Owner)

                // Variable Declarations
                Map<Id, GroupMember> mapAllGroupMembers = new Map<Id, GroupMember>();
                Map<Id, List<GroupMember>> mapQueueIdVSGroupMembersList = new Map<Id, List<GroupMember>>();
                // map of PublicGroupIdVSQueueId
                Map<Id, Id> mapGroupIdVSQueueId = new Map<Id, Id>();
                for(GroupMember objGroupMember : groupMemberMap.values()) {
                    //System.debug('get id type======='+objGroupMember.UserOrGroupId.getsobjecttype());
                    if(objGroupMember.UserOrGroupId.getsobjecttype() == Group.SObjectType) {
                        mapGroupIdVSQueueId.put(objGroupMember.UserOrGroupId,objGroupMember.GroupId);
                    }
                }
                //System.debug('mapGroupIdVSQueueId========'+mapGroupIdVSQueueId);
                // Fetch group members from queue's public group
                Map<Id,GroupMember> mapPublicGroupMembers = new Map<Id,GroupMember>([SELECT Id, 
                                                                                             GroupId, 
                                                                                             UserOrGroupId 
                                                                                        FROM GroupMember 
                                                                                       WHERE GroupId IN :mapGroupIdVSQueueId.keySet()]);
                //System.debug('mapPublicGroupMembers========'+mapPublicGroupMembers);
                // Populate mapAllGroupMembers with queue's public group members
                mapAllGroupMembers.putAll(mapPublicGroupMembers);
                // Used to store the UserorGroupId from queue's public group(Users of public group)
                List<Id> listPublicGroupUserId = new List<Id>();
                // Fetch the users of public group
                for(GroupMember objPublicGroupMem : mapPublicGroupMembers.values()) {
                    listPublicGroupUserId.add(objPublicGroupMem.UserOrGroupId);
                    if(!mapQueueIdVSGroupMembersList.containsKey(mapGroupIdVSQueueId.get(objPublicGroupMem.GroupId))) {
                        mapQueueIdVSGroupMembersList.put(mapGroupIdVSQueueId.get(objPublicGroupMem.GroupId), new List<GroupMember>());
                    }
                    mapQueueIdVSGroupMembersList.get(mapGroupIdVSQueueId.get(objPublicGroupMem.GroupId)).add(objPublicGroupMem);
                }
                //System.debug('listPublicGroupUserId======='+listPublicGroupUserId);
                //System.debug('mapQueueIdVSGroupMembersList======='+mapQueueIdVSGroupMembersList);
                // Upto here

                // Built group member and user group map to use them as per case owner's details
                for(GroupMember groupMemberInstance : groupMemberMap.values()) {
                    userOrGroupIds.add(groupMemberInstance.UserOrGroupId);
                    if(!groupMemberWithGroupMap.containsKey(groupMemberInstance.GroupId)){
                        groupMemberWithGroupMap.put(groupMemberInstance.GroupId,new List<GroupMember>());
                    }
                    groupMemberWithGroupMap.get(groupMemberInstance.GroupId).add(groupMemberInstance);
                }
                //System.debug('userOrGroupIds========'+userOrGroupIds);
                //System.debug('groupMemberWithGroupMap========'+groupMemberWithGroupMap);
                
                // Fetch group members
                Map<Id,GroupMember> groupUserMap = new Map<Id,GroupMember>([SELECT Id, 
                                                                                   UserOrGroupId 
                                                                              FROM GroupMember 
                                                                             WHERE UserOrGroupId IN :userOrGroupIds]);
                //System.debug('groupUserMap========'+groupUserMap);
                // Populate mapAllGroupMembers with queue members
                mapAllGroupMembers.putAll(groupUserMap);

                // Fetch related group users
                Map<Id,User> userListMap = new Map<Id,User>([SELECT Email 
                                                               FROM User 
                                                              WHERE Id IN :userOrGroupIds
                                                                 OR Id IN :listPublicGroupUserId]);
                //System.debug('userListMap========'+userListMap);

                // Fetch related Case users
                Map<Id,User> userCaseListMap = new Map<Id,User>([SELECT Email 
                                                                   FROM User 
                                                                  WHERE Id IN :caseOwnerIds]);

                //System.debug('userCaseListMap========'+userCaseListMap);

                // Create map of user and group member
                for(GroupMember groupMemberInstance : mapAllGroupMembers.values()) {
                    if(!groupMemberWithUserMap.containsKey(groupMemberInstance.UserOrGroupId)) {
                        groupMemberWithUserMap.put(groupMemberInstance.UserOrGroupId,new List<GroupMember>());
                    }
                    groupMemberWithUserMap.get(groupMemberInstance.UserOrGroupId).add(groupMemberInstance);
                }
                //System.debug('groupMemberWithUserMap========'+groupMemberWithUserMap);
                // 240619 - T - 00209 - Update the values of groupMemberWithGroupMap with public group member values
                for(Id queueId : mapQueueIdVSGroupMembersList.keySet()) {
                    if(groupMemberWithGroupMap.containsKey(queueId)) {
                        groupMemberWithGroupMap.get(queueId).addAll(mapQueueIdVSGroupMembersList.get(queueId));
                    }
                }
                //System.debug('groupMemberWithGroupMap after update========'+groupMemberWithGroupMap);

                // Iterate on case to identify owner
                for(Case caseRecord : mapCaseRelatedToCaseComments.Values()){
                    // Case objCase = caseMap.get(caseRecord.Id); 
                    //Get the queue
                    Group objQueue = groupsMap.containsKey(caseRecord.OwnerId) ? groupsMap.get(caseRecord.OwnerId) : null;  
                    if(objQueue != null) {
                        Id queueId = objQueue.id;
                        // get the groups in the queue
                        for(GroupMember objGroupMember : groupMemberWithGroupMap.get(queueId)) {
                            for(GroupMember objGroupMemberUsers : groupMemberWithUserMap.get(objGroupMember.UserOrGroupId)) {
                                
                                User usersIngroup = userListMap.containsKey(objGroupMember.UserOrGroupId) ? userListMap.get(objGroupMember.UserOrGroupId) : null;
                                
                                //System.debug('usersIngroup========='+usersIngroup);
                                
                                if(usersIngroup != null) {                
                                    if(!caseEmailMap.containsKey(caseRecord.Id)) 
                                        caseEmailMap.put(caseRecord.Id,new Set<String>());
                                    caseEmailMap.get(caseRecord.Id).add(usersIngroup.email);                    
                                }
                            }
                        }
                    }
                } // End of for
                // Iterate on wrapper to fetch details of parent case
                for(Case objCase : mapCaseRelatedToCaseComments.values()) {
                    // Iterate on version to get parent case owner
                    //for(ContentVersion contentVersionInstance : wrapperInstance.cvRecords) {
                        Messaging.SingleEmailMessage mail =  new Messaging.SingleEmailMessage();
                        targetWhatId = objCase.Id;      
                        //Case objCase = caseMap.get(targetWhatId); 
                        
                        //  list of internal people who should get the email
                        Set<String> sendTo = new Set<String>();
                        List<String> sendToList= new List<String>();
                        
                        // check case owner if group or user
                        // if owner is group add related sendto address
                        Group objQueue = groupsMap.containsKey(objCase.OwnerId) ? groupsMap.get(objCase.OwnerId) : null;  
                        if(objQueue != null) {
                            sendTo = caseEmailMap.get(targetWhatId);
                            
                        } else {
                            // else add case owner address
                            User usersWithCase = userCaseListMap.containsKey(objCase.OwnerId) ? userCaseListMap.get(objCase.OwnerId) : null;
                        
                            if(usersWithCase != null) {
                                // 190619 - T - 00211 - Check if email address is valid and then add to sendTo list
                                Boolean isEmailValid = ValidateEmailAddress(usersWithCase.email);
                                if(isEmailValid) {
                                    sendTo.add(usersWithCase.email);
                                } // End of inner if
                            } // End of outer if that is if usersWithCase is not null
                        } // End of if that is if objQueue is not null
                        //System.debug('sendTo======='+sendTo);
                        // 240619 - T - 00209 - Add all the user email addresses to the list
                        sendToList.addAll(sendTo);
                        //System.debug('sendToList========'+sendToList);
                        if (objCase.Status != 'Closed') {
                             
                            mail.setToAddresses(sendToList);
                            //String strSubject = MailSubject(objCase.CaseNumber);
                            mail.setSubject(MailSubject(objCase.CaseNumber));
                            //String body = MailBody(objCase.CaseNumber, objCase.Id);
                            mail.setHtmlBody(MailBody(objCase.CaseNumber, objCase.Account.Name, objCase.Contact.Name,
                                                      objCase.Subject,objCase.Id));
                            //mail.setTemplateId(caseOwnerTemplateId); 
                            mail.setorgWideEmailAddressId(orgWideAddress);
                            mail.setSaveAsActivity(false);          // must be false ...    
                            // Add your email to the master list
                            mailList.add(mail);   
                        }                    
                    //}
                }
                
            } else {
                
                // Loggedin User is Internal User, send emails to contacts of related Cases
                for(Case objCase : mapCaseRelatedToCaseComments.values()) {
                    // Create emails to send notifications
                    Messaging.SingleEmailMessage mail =  new Messaging.SingleEmailMessage();
                    targetWhatId = objCase.Id; 
                    if(objCase.Status != 'Closed' && objCase.ContactId != null) {
                        mail.setTargetObjectId(objCase.ContactId);
                        mail.setWhatId(targetWhatId);
                        // 170619 - Set email template Id to be sent to Case.Contact
                        mail.setTemplateId(caseContactTemplateId);
                        mail.setorgWideEmailAddressId(orgWideAddress);
                        mail.setSaveAsActivity(false);                                 
                        // Add your email to the master list
                        mailList.add(mail);
                    } // End of if  
                } // End of for   
            } // End of inner if i.e. if user is internal user   
         } // End of outer if i.e. setCaseId is not empty or null
         //System.debug('======mailList.size=====' + mailList.size() + '=====mailList=== ' +  mailList );
         // If maillist is not empty, null or 0 then send emails
         if(mailList.size() > 0 ) {
             //System.debug('====Inside send email====');
             // Send emails and handle the exceptions
             try {
                Messaging.sendEmail(mailList);
                } catch(Exception e) {

                }
             
         } // End of if loop
    }
    Private static Boolean getIsCommunityUser(){
        List<User> uList = [select Id from user where id=:userinfo.getuserid() AND ContactId = NULL];
        
        if(uList.size() > 0){
           return false; 
        }
        return true;
    }
    
    // Method to built Email Subject
    /**
    * Method Name : MailSubject
    * Parameters : param1: String 
    * Description : This method is used to built email Subject
    **/
    public static String MailSubject(String caseNumber){
        String subject = 'Case #' + caseNumber + '| Comment Has Been Added.';
        return subject;
    }

    // Method to built Email Body
    /**
    * Method Name : MailBody
    * Parameters : param1: String param2: String, param3: String, param4: String, param5: Id
    * Description : This method is used to built email Body
    **/
    public static String MailBody(String caseNumber, String accName, String contactName, String caseSubject, Id caseId){
        URL url = URL.getOrgDomainUrl();
        string caseUrl = url.toExternalForm() + '/' + caseId;
        String body = 'Case #' + caseNumber + ' has a new comment from the Community: ' + '<br/>' +
                      'Credit Union: '+ accName  + '<br/>' +'Case Contact: ' + contactName + '<br/>' +
                       'Case Subject: '+ caseSubject + '<br/>' +'Case Link: ' + caseUrl;
        //body += caseUrl;
        return body;
    }
    // 190619 - T - 00211 - Added to check if email address is valid or not
    /**
    * Method Name : ValidateEmailAddress
    * Parameters : param1: String
    * Description : This method is used to check if email address is valid or not
    **/
    public static Boolean ValidateEmailAddress(String email) {
        Boolean res = true;
        String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$'; 
        Pattern getPattern = Pattern.compile(emailRegex);
        Matcher getMatcher = getPattern.matcher(email);

        if (!getMatcher.matches()) 
            res = false;
        return res; 
    }
    /**
    * Method Name : setCaseFlagForCommunityUsers
    * Parameters  : 
    * Description : This method is used to set the Case flag for community users when internal user posts a new comment on 
    *               Case record
    * Created By  : VennScience_BFL_Amruta
    * Created On  : 180719
    **/
    public static void setCaseFlagForCommunityUsers(List<syn_Case_Comment__c> listCaseComments) {

        // Variable Declarations
        List<Case> listCases = new List<Case>();
        List<Case> listCaseToBeUpdated = new List<Case>();
        Set<Id> setCaseId = new Set<Id>();

        // Iterate over case comment records
        for(syn_Case_Comment__c objCaseComment : listCaseComments) {
            setCaseId.add(objCaseComment.Case__c);
        } // End of for

        // Fetch all the parent Cases
        listCases = [SELECT Id
                       FROM Case
                      WHERE Id IN :setCaseId];
        // Iterate over case records to update the flag
        for(Case objCase : listCases) {
            objCase.Needs_Review__c = true;
            listCaseToBeUpdated.add(objCase);
        } // End of for
        System.debug('listCaseToBeUpdated========='+listCaseToBeUpdated);
        // Update the case records
        if(!listCaseToBeUpdated.isEmpty()) {
            try {
                update listCaseToBeUpdated;
            } catch(Exception e) {
                System.debug('Error occurred while updating Case records:'+e.getMessage());
            } // End of try-catch block
        } // End of if
    }
}