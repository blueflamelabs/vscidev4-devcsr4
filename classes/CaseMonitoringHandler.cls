public class CaseMonitoringHandler 
{
    @testVisible static Boolean Bypasstrigger = false;
    
    List<Case_Monitoring__c > caseMonitoringRecordsList = new  List<Case_Monitoring__c >();
    
    //  Used to store Triger.oldMap
    Map<Id, Case_Monitoring__c> objOldMap = new Map<Id, Case_Monitoring__c>();
    //  Used to store filtered case comment records
    List<Case_Monitoring__c > filtertedCaseCommentRecords = new List<Case_Monitoring__c >();
    
    public CaseMonitoringHandler (List<sObject> newCaseMonitoringRecords)
    {
        caseMonitoringRecordsList = (List<Case_Monitoring__c >)newCaseMonitoringRecords;
        
    }
    public CaseMonitoringHandler (List<sObject> newCaseMonitoringRecords, Map<Id, Case_Monitoring__c> objRecordsOldMap)
    {
        caseMonitoringRecordsList = (List<Case_Monitoring__c >)newCaseMonitoringRecords;
        objOldMap = objRecordsOldMap;
    }

    
   /* public void afterUpdate()
    {
        // 190619 - T - 00295 - Added debug
        System.debug('Inside afterUpdate');
        List<Case_Monitoring__c> objLstMonitoring = new List<Case_Monitoring__c>();
    	List<Case> objLstCase = new List<Case>();
        
        // 190719 - T - 00445 - Initialize the set to add the case records.
        Set<Case> objSetCase = new Set<Case>();
    	Case objCase;
    
        for(Case_Monitoring__c obj : contentRecords)
        {
            objLstMonitoring = [SELECT Id, Case__c, Acknowledge_Date__c FROM Case_Monitoring__c WHERE Case__c =: obj.Case__c AND  Review_Complete__c = false AND Acknowledge_Date__c = null];
            objCase = [SELECT Id, CaseNumber, Status, Case_Communication_Flag__c, OwnerId FROM Case WHERE ID =: obj.Case__c ];
            // 190619 - T - 00295 - Added debugs
        	System.debug('objLstMonitoring========'+objLstMonitoring);
            System.debug('objCase========'+objCase);
            System.debug('obj.Review_Complete__c========'+obj.Review_Complete__c);
            if (obj.Review_Complete__c == false)
            {
                // 190619 - T - 00295 - Added debugs
        		System.debug('Inside main if');
                if (objCase.Case_Communication_Flag__c == false)
                {
                    // 190619 - T - 00295 - Added debugs
        			System.debug('Case communication flag is false');
                    objCase.Case_Communication_Flag__c = true;
                    objLstCase.add(objCase);
                    // 190719 - T - 00445 - added the case record in Set variable.
                    objSetCase.add(objCase);
                }    
            }
            else
            {
            	// 190619 - T - 00295 - Added debugs
        		System.debug('Inside main else');
                if (objLstMonitoring.size()==0)
                {
                    // 190619 - T - 00295 - Added debugs
        			System.debug('objLstMonitoring is zero');
                    if (objCase.Case_Communication_Flag__c == true)
                    {
                        // 190619 - T - 00295 - Added debugs
        				System.debug('case comm flag is true');
                        objCase.Case_Communication_Flag__c = false;
                        objLstCase.add(objCase);
                        // 190719 - T - 00445 - added the case record in Set variable.
                        objSetCase.add(objCase);
                    }
                }
                if (objLstMonitoring.size()>0)
                {
                    // 190619 - T - 00295 - Added debugs
        			System.debug('objLstMonitoring is greater than zero');
                    if (objCase.Case_Communication_Flag__c == false)
                    {
                        // 190619 - T - 00295 - Added debugs
        				System.debug('Case_Communication_Flag__c is false');
                        objCase.Case_Communication_Flag__c = true;
                        objLstCase.add(objCase); 
                        // 190719 - T - 00445 - added the case record in Set variable.
                        objSetCase.add(objCase);
                    }
                }  
            }
         }
    
       // 190619 - T - 00295 - Added debug
       System.debug('objLstCase======'+objLstCase);
        List<Case> caseList = new List<Case>();
         // 190719 - T - 00445 - added the case records Set variable into List .
        caseList.addAll(objSetCase);
       if (caseList.size()>0)
       {
           // 190719 - T - 00445 - update the case List.
             update caseList;  
       }
    
	}*/
    
    /**
    * Method Name : updateCaseMonitoringRelatedCases
    * Parameters  : 
    * Description : This method is used to update all the related Cases.
    * Created By  : VennScience_BFL_Monali
    * Created On  : 100919
    **/
    public void updateCaseMonitoringRelatedCases() {
        // T-00627 - 100919 - VennScience_BFL_Monali - check Boolean variable to handle the recursion.
         if(trgr_Case_SetOwnerHandler.isExecuteUpdateTrigger) { 
            return ;
        }
        Map<Id, Case> updateCaseMap = new Map<Id, Case>();
        Set<Id> setOfCaseId = new Set<Id>();
        List<Case> caseList = new List<Case>();
        for(Case_Monitoring__c caseMonitoring : caseMonitoringRecordsList) {
            setOfCaseId.add(caseMonitoring.Case__c);
        }//End of for
        
        if(!setOfCaseId.isEmpty()) {
            
            for(Case caseRecord : [ SELECT Id, 
                                   		   Case_Communication_Flag__c,
                                           (SELECT Id,
                                                   Case__c, 
                                                   Acknowledge_Date__c,
                                                   Review_Complete__c
                                              FROM CaseMonitoring__r
                                             WHERE Review_Complete__c =false)
                                      FROM Case WHERE Id IN :setOfCaseId]
            ) {
                //If Case Monitoring record's Review Completed is set to false then related Case's Communication flag is set to true.
                if(caseRecord.CaseMonitoring__r.size() > 0 && !caseRecord.Case_Communication_Flag__c){
                    //System.debug('====1st if====== ');
                    caseRecord.Case_Communication_Flag__c = true;
                    updateCaseMap.put(caseRecord.Id, caseRecord);
                } 
                //If Case Monitoring record's Review Completed is set to true then related Case's Communication flag is set to false..
                if(caseRecord.CaseMonitoring__r.size() == 0 && caseRecord.Case_Communication_Flag__c){
                    //System.debug('=========2nd if =========== ');
                    caseRecord.Case_Communication_Flag__c = false;
                     updateCaseMap.put(caseRecord.Id, caseRecord);
                } 
                
            }//End of for
            System.debug('caseMap '+updateCaseMap);
            System.debug('caseMap values'+updateCaseMap.values());
            if(updateCaseMap.size() > 0 ) {
                update updateCaseMap.values();
            }
        }//End of outer if
       
    }
    
}