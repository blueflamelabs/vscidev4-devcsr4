public class ContentVersionHandler {
    @testVisible static Boolean Bypasstrigger = false;
    List<ContentVersion> contentRecords = new  List<ContentVersion>();
    // 250619 - T - 00326 - Used to store the filtered content version records on the basis of file sharing option
    List<ContentVersion> listFilteredContentRecords = new List<ContentVersion>();
    // 250619 - T - 00326 - Used to store the content version records from Trigger.oldMap
    Map<Id, ContentVersion> contentVerionOldMap = new Map<Id, ContentVersion>();
    // 180619 - T - 00275 - Fetch custom setting Org Default value to on/off file upload email notification functionality
    Email_Notification_Trigger_Setting__c triggerSetting = Email_Notification_Trigger_Setting__c.getOrgDefaults();
    
    public ContentVersionHandler (List<sObject> cvRecords){
        contentRecords = (List<ContentVersion>)cvRecords;
    }
    // 250619 - Added to get list of content version records and map of records from Trigger.OldMap
    public ContentVersionHandler(List<sObject> cvRecords, Map<Id, ContentVersion> contentVerionRecordsOldMap)
    {
        contentRecords = (List<sObject>)cvRecords;
        contentVerionOldMap = contentVerionRecordsOldMap;
    }

    public void beforeInsert(){
        if(Bypasstrigger) return;
        //ContentVersionService.fileReviewFlagHandler(contentRecords);
        ContentVersionService.contentDocumentWithCaseMonitoringRecMapping(ContentVersionService.getCaseTypeContentVersionRecords(contentRecords));
    }
    public void afterUpdate(){
         //System.debug('Inside after update');
         //System.debug('ContentVersionService.isInitialUpdate======'+ContentVersionService.isInitialUpdate);

         // 180719 - T - 00444 - VennScience_BFL_Amruta - Used to check if logged in user is community user or not
         Boolean bIsCommunityUser = GetIsCommunityUser();
         // 180719 - T - 00444 - VennScience_BFL_Amruta - Used to store the filtered content version records to set the Case 
         // flag for community users
         List<ContentVersion> listFiltertedContentVersion = new List<ContentVersion>();
         Boolean isSharingOptionChanged = false;
         if(Bypasstrigger) return;
         ContentVersionService.updateCaseMonitoringRecordWhenFileUpdate(ContentVersionService.getCaseTypeContentVersionRecords(contentRecords));
        //ContentVersionService.updateCaseRecordWhenFileUpdate(contentRecords);

        if(triggerSetting.Is_File_Notification_Active__c && !ContentVersionService.isInitialUpdate) {

            // 250619 - T - 00326 - Filter ContentVersion records on the basis of FileSharing option
            //System.debug('contentVerionOldMap======='+contentVerionOldMap);
            for(ContentVersion objContentVersion : contentRecords) {
                ContentVersion objOldContentVersion = contentVerionOldMap.get(objContentVersion.Id);
                String previousSharingPrivacyOption = objOldContentVersion.SharingPrivacy;
                String currentSharingPrivacyOption = objContentVersion.SharingPrivacy;
                //System.debug('previousSharingPrivacyOption======='+previousSharingPrivacyOption);
                //System.debug('currentSharingPrivacyOption======='+currentSharingPrivacyOption);
                if(previousSharingPrivacyOption != currentSharingPrivacyOption && previousSharingPrivacyOption == 'P' && currentSharingPrivacyOption == 'N') {
                    isSharingOptionChanged = true;
                    listFilteredContentRecords.add(objContentVersion);
                } else if(!objContentVersion.IsEmailNotificationSent__c && previousSharingPrivacyOption == currentSharingPrivacyOption && currentSharingPrivacyOption == 'N') {
                    listFilteredContentRecords.add(objContentVersion);
                }// End of if
            } // End of for

            //System.debug('listFilteredContentRecords in after update======'+listFilteredContentRecords);
            // 250619 - T - 00326 - Call Service Class to send file upload email notifications
            if(!listFilteredContentRecords.isEmpty()) {
                ContentVersionService.sendEmailNotification(listFilteredContentRecords, isSharingOptionChanged);
            }
        } // End of if that is if custom setting for email notification is Active

        // 180719 - T - 00444 - VennScience_BFL_Amruta - Filter records whose File sharing option is set to 'Visible to All'
        // and set the Case flag when file is uploaded by the internal users
        if(!ContentVersionService.isInitialUpdate) {
            for(ContentVersion objContentVersion : contentRecords) {
                // Check if the uploaded content version is the first version and the file privacy is set to 'visible to all'
                if(objContentVersion.SharingPrivacy == 'N' && !bIsCommunityUser) {
                    listFiltertedContentVersion.add(objContentVersion);
                } // End of if
            } // End of for

            System.debug('listFiltertedContentVersion======'+listFiltertedContentVersion);
            // Call Service method
            if(!listFiltertedContentVersion.isEmpty()) {
                ContentVersionService.setCaseFlagForCommunityUsers(listFiltertedContentVersion);  
            } // End of if
        } // End of if that is isInitialUpdate is false
    }
     public void afterInsert(){


        // 180719 - T - 00444 - VennScience_BFL_Amruta - Used to check if logged in user is community user or not
        Boolean bIsCommunityUser = GetIsCommunityUser();
        // 180719 - T - 00444 - VennScience_BFL_Amruta - Used to store the filtered content version records to set the Case 
        // flag for community users
        List<ContentVersion> listFiltertedContentVersion = new List<ContentVersion>();
        if(Bypasstrigger) return;
        ContentVersionService.updateCaseMonitoringRecords(ContentVersionService.getCaseTypeContentVersionRecords(contentRecords));
        //ContentVersionService.updateReviewStatus(contentRecords);
        //ContentVersionService.updateCaseRecordWhenFileInsert(contentRecords);
        //ContentVersionService.sendEmailNotification(contentRecords);
        
        // 070619 - T-00209 - Added debug
        // System.debug('ContentVersionHandler called'); 
        // 180619 - T - 00275 - Check if triger setting is active
        if(triggerSetting.Is_File_Notification_Active__c) {

            // 250619 - T - 00326 - Check if triger setting is active
            List<ContentVersion> listContentVersionToUpdate = new List<ContentVersion>();
            // 250619 - T - 00326 - Filter ContentVersion records on the basis of FileSharing option
            for(ContentVersion objContentVersion : contentRecords) {
                
                if(objContentVersion.SharingPrivacy == 'N' && objContentVersion.VersionNumber != '1') {
                    listFilteredContentRecords.add(objContentVersion);
                } // End of if
            } // End of for

            //System.debug('listFilteredContentRecords in after insert======'+listFilteredContentRecords);
            // Call Service Class to send file upload email notifications
            if(!listFilteredContentRecords.isEmpty()) {
                ContentVersionService.sendEmailNotification(listFilteredContentRecords, false);
            } // End of inner if
        } // End of if that is Trigger setting is active

        // 180719 - T - 00444 - VennScience_BFL_Amruta - Filter records whose File sharing option is set to 'Visible to All'
        // and set the Case flag when file is uploaded by the internal users
        for(ContentVersion objContentVersion : contentRecords) {
            // Check if the uploaded content version is the first version and the file privacy is set to 'visible to all'
            if(objContentVersion.SharingPrivacy == 'N' && objContentVersion.VersionNumber != '1' && !bIsCommunityUser) {
                listFiltertedContentVersion.add(objContentVersion);
            } // End of if
        } // End of for

        System.debug('listFiltertedContentVersion======'+listFiltertedContentVersion);
        // Call Service method
        if(!listFiltertedContentVersion.isEmpty()) {
            ContentVersionService.setCaseFlagForCommunityUsers(listFiltertedContentVersion);  
        } // End of if
    }
    public static boolean GetIsCommunityUser () 
    {
        //
        return (getPathPrefix() != '' ? true : false);
        //
        
    }

    public static String getPathPrefix () {
        Id net_id = Network.getNetworkId();
        if (net_id  != null) {
            return [SELECT Id, UrlPathPrefix
                    FROM Network
                    WHERE Id = :net_id].UrlPathPrefix;
        }
        return '';
    }
}