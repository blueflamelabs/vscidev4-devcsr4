@isTest
private without sharing class ContentVersionService_Test {
    
    // 210819 - T-00564 - VennScience_BFL_Amruta - Used to store the community profile name that is fetched from custom label
    public static final String PROFILENAME = Label.CommunityProfileName;
    //Upload new File As Internal User
    @IsTest
    static void method1(){
        
        UserRole ur = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role',PortalType = 'None');
        insert ur;
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        
        User user;
        Account ac;
        Contact con;
        
        User portalAccountOwner1 = new User(
            UserRoleId = ur.Id,
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'test2@test.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1 ;
        
        
        System.runAs(portalAccountOwner1) {
            
            Email_Notification_Trigger_Setting__c custObj = new Email_Notification_Trigger_Setting__c();
            custObj.Is_Case_Comment_Notification_Active__c = false;
            custObj.Is_File_Notification_Active__c = true;
            
            
            insert custObj;
            
            Account accObj = new Account();
            accObj.Name = 'Test Account';
            
            insert accObj;
            
            Contact objContact = new Contact();
            objContact.FirstName = 'Test';
            objContact.LastName = 'Contact';
            objContact.AccountId = accObj.Id;
            objContact.Email = 'test@gmail.com';
            
            insert objContact;
            
            Id RecordTypeIdCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Internal Service Request').getRecordTypeId();
            List<Case> cases = new List<Case>{
                new Case(Status = 'New', Date_Required__c = System.today(),recordtypeid =RecordTypeIdCase,ContactId = objContact.Id),
                    new Case(Status = 'New', Date_Required__c = System.today(),recordtypeid = RecordTypeIdCase,ContactId = objContact.Id),
                    new Case(Status = 'New', Date_Required__c = System.today(),recordtypeid =RecordTypeIdCase,ContactId = objContact.Id)
                    };
                        insert cases;
            test.startTest();
            ContentVersion conVer = new ContentVersion();
            conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
            conVer.PathOnClient = 'Test File 1'; // The files name, extension is very important here which will help the file in preview.
            conVer.Title = 'Test File 1'; // Display name of the files
            conVer.VersionData = Blob.valueOf('Unit Test'); // converting your binary string to Blog
            insert conVer;
            
            List<ContentVersion> conDocList = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id];
            
            //Create ContentDocumentLink
            ContentDocumentLink cDe = new ContentDocumentLink();
            cDe.ContentDocumentId = conDocList[0].ContentDocumentId;
            cDe.LinkedEntityId = cases[0].Id; // you can use objectId,GroupId etc
            cDe.ShareType = 'V'; // Inferred permission, checkout description of ContentDocumentLink object for more details
            cDe.Visibility = 'AllUsers';
            insert cDe;
            
            conVer = new ContentVersion();
            conVer.ContentDocumentId = conDocList[0].ContentDocumentId;
            conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
            conVer.PathOnClient = 'Test File 2'; // The files name, extension is very important here which will help the file in preview.
            conVer.Title = 'Test File 2'; // Display name of the files
            conVer.VersionData = Blob.valueOf('Unit Test'); // converting your binary string to Blog
            insert conVer;
            
            conDocList = [SELECT Review_Complete__c , ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id];
            system.assertEquals(true, conDocList[0].Review_Complete__c );
            List<Case_Monitoring__c> caseMonitoringRecs = [SELECT Id, Review_Complete__c, Acknowledge_Date__c FROM Case_Monitoring__c WHERE Parent_Id__c = :conVer.Id];
            system.assertEquals(true, caseMonitoringRecs[0].Review_Complete__c);
            //system.assertEquals(System.today(), caseMonitoringRecs[0].Acknowledge_Date__c);
        }
        Test.stopTest();
    }
    
    //Upload new Version As Internal User, when document owner is Internal User
    @IsTest
    static void method2(){
        
        UserRole ur = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role',PortalType = 'None');
        insert ur;
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        
        User user;
        Account ac;
        Contact con;
        
        User portalAccountOwner1 = new User(
            UserRoleId = ur.Id,
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'test2@test.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1 ;
        
        
        System.runAs(portalAccountOwner1) {
            Email_Notification_Trigger_Setting__c custObj = new Email_Notification_Trigger_Setting__c();
            custObj.Is_Case_Comment_Notification_Active__c = false;
            custObj.Is_File_Notification_Active__c = true;
            
            insert custObj;
            
            Account accObj = new Account();
            accObj.Name = 'Test Account';
            
            insert accObj;
            
            Contact objContact = new Contact();
            objContact.FirstName = 'Test';
            objContact.LastName = 'Contact';
            objContact.AccountId = accObj.Id;
            objContact.Email = 'test@gmail.com';
            
            insert objContact;
            
            Test.startTest();
            Id RecordTypeIdCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Internal Service Request').getRecordTypeId();
            
            List<Case> cases = new List<Case>{
                new Case(Status = 'New', Date_Required__c = System.today(),recordtypeid =RecordTypeIdCase,ContactId = objContact.Id),
                new Case(Status = 'New', Date_Required__c = System.today(),recordtypeid =RecordTypeIdCase,ContactId = objContact.Id),
                new Case(Status = 'New', Date_Required__c = System.today(),recordtypeid =RecordTypeIdCase,ContactId = objContact.Id)
            };
            insert cases;
            
            ContentVersion conVer = new ContentVersion();
            conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
            conVer.PathOnClient = 'Test File 1'; // The files name, extension is very important here which will help the file in preview.
            conVer.Title = 'Test File 1'; // Display name of the files
            conVer.VersionData = Blob.valueOf('Unit Test'); // converting your binary string to Blog
            insert conVer;
            
            List<ContentVersion> conDocList = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id];
        
            //Create ContentDocumentLink
            ContentDocumentLink cDe = new ContentDocumentLink();
            cDe.ContentDocumentId = conDocList[0].ContentDocumentId;
            cDe.LinkedEntityId = cases[0].Id; // you can use objectId,GroupId etc
            cDe.ShareType = 'V'; // Inferred permission, checkout description of ContentDocumentLink object for more details
            cDe.Visibility = 'AllUsers';
            insert cDe;
            
            conVer = new ContentVersion();
            conVer.ContentDocumentId = conDocList[0].ContentDocumentId;
            conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
            conVer.PathOnClient = 'Test File 2'; // The files name, extension is very important here which will help the file in preview.
            conVer.Title = 'Test File 2'; // Display name of the files
            conVer.VersionData = Blob.valueOf('Unit Test'); // converting your binary string to Blog
            insert conVer;
            
            conVer = new ContentVersion();
            conVer.ContentDocumentId = conDocList[0].ContentDocumentId;
            conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
            conVer.PathOnClient = 'Test File 3'; // The files name, extension is very important here which will help the file in preview.
            conVer.Title = 'Test File 3'; // Display name of the files
            conVer.VersionData = Blob.valueOf('Unit Test'); // converting your binary string to Blog
            insert conVer;
            
            conDocList = [SELECT Review_Complete__c , ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id];
            system.assertEquals(true, conDocList[0].Review_Complete__c );
            List<Case_Monitoring__c> caseMonitoringRecs = [SELECT Id, Review_Complete__c, Acknowledge_Date__c FROM Case_Monitoring__c WHERE Parent_Id__c = :conVer.Id];
            system.assertEquals(true, caseMonitoringRecs[0].Review_Complete__c);
            //system.assertEquals(System.today(), caseMonitoringRecs[0].Acknowledge_Date__c);
        }
        Test.stopTest();
    }

    //Upload new File As Community User
    @IsTest
    static void method3(){
       
        UserRole ur = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role',PortalType = 'None');
        insert ur;
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        
        User user;
        Account ac;
        Contact con;
        
        User portalAccountOwner1 = new User(
            UserRoleId = ur.Id,
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'test2@test.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1 ;
        
        
        Id p = [select id from profile where Name = :PROFILENAME].id;
        System.runAs(portalAccountOwner1) {
            
            ac = new Account(name ='Grazitti', OwnerId = portalAccountOwner1.Id) ;
            insert ac; 
            
            con = new Contact(LastName ='testCon',AccountId = ac.Id);
            insert con;
            
            user = new User(alias = 'test123', email='test123@noemail.com',
                            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                            localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                            ContactId = con.Id,
                            timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
            
            insert user; 
        }
        List<NetworkMember> networkMembers = [SELECT Id, MemberId, NetworkId FROM NetworkMember WHERE MemberId = :user.Id];
        System.runAs(user) {
            
            Email_Notification_Trigger_Setting__c custObj = new Email_Notification_Trigger_Setting__c();
            custObj.Is_Case_Comment_Notification_Active__c = false;
            custObj.Is_File_Notification_Active__c = true;
            
            Id RecordTypeIdCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Internal Service Request').getRecordTypeId();
            List<Case> cases = new List<Case>{
                new Case(Status = 'New', Date_Required__c = System.today(),recordtypeid =RecordTypeIdCase,ContactId = con.Id),
                new Case(Status = 'New', Date_Required__c = System.today(),recordtypeid =RecordTypeIdCase,ContactId = con.Id),
                new Case(Status = 'New', Date_Required__c = System.today(),recordtypeid =RecordTypeIdCase,ContactId = con.Id)
            };
            insert cases;
            
            ContentVersion conVer = new ContentVersion();
            conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
            conVer.PathOnClient = 'Test File 1'; // The files name, extension is very important here which will help the file in preview.
            conVer.Title = 'Test File 1'; // Display name of the files
            conVer.VersionData = Blob.valueOf('Unit Test'); // converting your binary string to Blog
            conVer.OwnerId = user.Id;
            conVer.NetworkId = networkMembers[0].NetworkId;
            insert conVer;
            
            List<ContentVersion> conDocList = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id];
        
            //Create ContentDocumentLink
            ContentDocumentLink cDe = new ContentDocumentLink();
            cDe.ContentDocumentId = conDocList[0].ContentDocumentId;
            cDe.LinkedEntityId = cases[0].Id; // you can use objectId,GroupId etc
            cDe.ShareType = 'V'; // Inferred permission, checkout description of ContentDocumentLink object for more details
            cDe.Visibility = 'AllUsers';
            insert cDe;
            
            Test.startTest();
            
            conVer = new ContentVersion();
            conVer.ContentDocumentId = conDocList[0].ContentDocumentId;
            conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
            conVer.PathOnClient = 'Test File 2'; // The files name, extension is very important here which will help the file in preview.
            conVer.Title = 'Test File 2'; // Display name of the files
            conVer.VersionData = Blob.valueOf('Unit Test'); // converting your binary string to Blog
            //conVer.OwnerId = user.Id;
            conVer.NetworkId = networkMembers[0].NetworkId;
            insert conVer;
            
            conDocList = [SELECT Review_Complete__c , ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id];
            system.assertEquals(false, conDocList[0].Review_Complete__c );
            List<Case_Monitoring__c> caseMonitoringRecs = [SELECT Id, Review_Complete__c, Acknowledge_Date__c FROM Case_Monitoring__c WHERE Parent_Id__c = :conVer.Id];
            system.assertEquals(false, caseMonitoringRecs[0].Review_Complete__c);
            system.assertEquals(null, caseMonitoringRecs[0].Acknowledge_Date__c);
        }
        Test.stopTest();
    }
    
    //Upload new Version As Community User, when document owner is Community User
    @IsTest
    static void method4(){
       
        UserRole ur = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role',PortalType = 'None');
        insert ur;
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        
        User user;
        Account ac;
        Contact con;
        
        User portalAccountOwner1 = new User(
            UserRoleId = ur.Id,
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'test2@test.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1 ;
        
        
        Id p = [select id from profile where name='+Power Community User'].id;
        System.runAs(portalAccountOwner1) {
            
            Email_Notification_Trigger_Setting__c custObj = new Email_Notification_Trigger_Setting__c();
            custObj.Is_Case_Comment_Notification_Active__c = false;
            custObj.Is_File_Notification_Active__c = true;
            
            insert custObj;
            
            ac = new Account(name ='Grazitti', OwnerId = portalAccountOwner1.Id) ;
            insert ac; 
            
            con = new Contact(LastName ='testCon',AccountId = ac.Id);
            insert con;
            
            user = new User(alias = 'test123', email='test123@noemail.com',
                            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                            localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                            ContactId = con.Id,
                            timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
            
            insert user; 
        }
        List<NetworkMember> networkMembers = [SELECT Id, MemberId, NetworkId FROM NetworkMember WHERE MemberId = :user.Id];
        System.runAs(user) {
            Id RecordTypeIdCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Internal Service Request').getRecordTypeId();
            List<Case> cases = new List<Case>{
                new Case(Status = 'New', Date_Required__c = System.today(),recordtypeid =RecordTypeIdCase,contactId = con.Id),
                new Case(Status = 'New', Date_Required__c = System.today(),recordtypeid =RecordTypeIdCase,contactId = con.Id),
                new Case(Status = 'New', Date_Required__c = System.today(),recordtypeid =RecordTypeIdCase,contactId = con.Id)
            };
            insert cases;
            
            Test.startTest();
            ContentVersion conVer = new ContentVersion();
            conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
            conVer.PathOnClient = 'Test File 1'; // The files name, extension is very important here which will help the file in preview.
            conVer.Title = 'Test File 1'; // Display name of the files
            conVer.VersionData = Blob.valueOf('Unit Test'); // converting your binary string to Blog
            conVer.NetworkId = networkMembers[0].NetworkId;
            insert conVer;
            
            List<ContentVersion> conDocList = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id];
        
            //Create ContentDocumentLink
            ContentDocumentLink cDe = new ContentDocumentLink();
            cDe.ContentDocumentId = conDocList[0].ContentDocumentId;
            cDe.LinkedEntityId = cases[0].Id; // you can use objectId,GroupId etc
            cDe.ShareType = 'V'; // Inferred permission, checkout description of ContentDocumentLink object for more details
            cDe.Visibility = 'AllUsers';
            insert cDe;
            
            conVer = new ContentVersion();
            conVer.ContentDocumentId = conDocList[0].ContentDocumentId;
            conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
            conVer.PathOnClient = 'Test File 2'; // The files name, extension is very important here which will help the file in preview.
            conVer.Title = 'Test File 2'; // Display name of the files
            conVer.VersionData = Blob.valueOf('Unit Test'); // converting your binary string to Blog
            conVer.OwnerId = user.Id;
            insert conVer;
            
            conVer = new ContentVersion();
            conVer.ContentDocumentId = conDocList[0].ContentDocumentId;
            conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
            conVer.PathOnClient = 'Test File 3'; // The files name, extension is very important here which will help the file in preview.
            conVer.Title = 'Test File 3'; // Display name of the files
            conVer.VersionData = Blob.valueOf('Unit Test'); // converting your binary string to Blog
            conVer.OwnerId = user.Id;
            insert conVer;
            
            conDocList = [SELECT Review_Complete__c , ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id];
            system.assertEquals(false, conDocList[0].Review_Complete__c );
            List<Case_Monitoring__c> caseMonitoringRecs = [SELECT Id, Review_Complete__c, Acknowledge_Date__c FROM Case_Monitoring__c WHERE Parent_Id__c = :conVer.Id];
            system.assertEquals(false, caseMonitoringRecs[0].Review_Complete__c);
            system.assertEquals(null, caseMonitoringRecs[0].Acknowledge_Date__c);
        }
        Test.stopTest();
    }

    //Upload new Version As Internal User, when document owner is Community User
    @IsTest
    static void method5(){
        
        UserRole ur = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role',PortalType = 'None');
        insert ur;
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        
        User user;
        Account ac;
        Contact con;
        List<ContentVersion> conDocList;
        
        User portalAccountOwner1 = new User(
            UserRoleId = ur.Id,
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'test2@test.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1 ;
        
        
        Id p = [select id from profile where name='+Power Community User'].id;
        System.runAs(portalAccountOwner1) {
            Account accObj = new Account();
            accObj.Name = 'Test Account';
            
            insert accObj;
            
            Contact objContact = new Contact();
            objContact.FirstName = 'Test';
            objContact.LastName = 'Contact';
            objContact.AccountId = accObj.Id;
            objContact.Email = 'test@gmail.com';
            
            insert objContact;
            
            Id RecordTypeIdCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Internal Service Request').getRecordTypeId();
            List<Case> cases = new List<Case>{
                new Case(Status = 'New', Date_Required__c = System.today(),recordtypeid =RecordTypeIdCase),
                new Case(Status = 'New', Date_Required__c = System.today(),recordtypeid =RecordTypeIdCase),
                new Case(Status = 'New', Date_Required__c = System.today(),recordtypeid =RecordTypeIdCase)
            };
            insert cases;
            
            ContentVersion conVer = new ContentVersion();
            conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
            conVer.PathOnClient = 'Test File 1'; // The files name, extension is very important here which will help the file in preview.
            conVer.Title = 'Test File 1'; // Display name of the files
            conVer.VersionData = Blob.valueOf('Unit Test'); // converting your binary string to Blog
            insert conVer;
            
            conDocList = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id];
        
            //Create ContentDocumentLink
            ContentDocumentLink cDe = new ContentDocumentLink();
            cDe.ContentDocumentId = conDocList[0].ContentDocumentId;
            cDe.LinkedEntityId = cases[0].Id; // you can use objectId,GroupId etc
            cDe.ShareType = 'V'; // Inferred permission, checkout description of ContentDocumentLink object for more details
            cDe.Visibility = 'AllUsers';
            insert cDe;
            
            ac = new Account(name ='Grazitti', OwnerId = portalAccountOwner1.Id) ;
            insert ac; 
            
            con = new Contact(LastName ='testCon',AccountId = ac.Id);
            insert con;
            
            user = new User(alias = 'test123', email='test123@noemail.com',
                            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                            localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                            ContactId = con.Id,
                            timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
            
            insert user; 
        }
        System.runAs(user) {
            Test.startTest();
            Boolean errorOccured = false;
            try{ 
                ContentVersion conVer = new ContentVersion();
                conVer.ContentDocumentId = conDocList[0].ContentDocumentId;
                conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
                conVer.PathOnClient = 'Test File 2'; // The files name, extension is very important here which will help the file in preview.
                conVer.Title = 'Test File 2'; // Display name of the files
                conVer.VersionData = Blob.valueOf('Unit Test'); // converting your binary string to Blog
                conVer.OwnerId = user.Id;
                insert conVer;
           } catch(Exception ex) {
                errorOccured = true;
            }
            system.assertEquals(true, errorOccured);
            
        }
        Test.stopTest();
    }

    
    //Update File_Reviewed flag
    @IsTest
    static void method6(){
       
        UserRole ur = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role',PortalType = 'None');
        insert ur;
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        
        User user;
        Account ac;
        Contact con;
        
        User portalAccountOwner1 = new User(
            UserRoleId = ur.Id,
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'test2@test.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1 ;
        
        
        Id p = [select id from profile where name='+Power Community User'].id;
        System.runAs(portalAccountOwner1) {
            
            ac = new Account(name ='Grazitti', OwnerId = portalAccountOwner1.Id) ;
            insert ac; 
            
            con = new Contact(LastName ='testCon',AccountId = ac.Id);
            insert con;
            
            user = new User(alias = 'test123', email='test123@noemail.com',
                            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                            localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                            ContactId = con.Id,
                            timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
            
            insert user; 
        }
        List<NetworkMember> networkMembers = [SELECT Id, MemberId, NetworkId FROM NetworkMember WHERE MemberId = :user.Id];
        Test.startTest();
            
        System.runAs(user){
            Email_Notification_Trigger_Setting__c custObj = new Email_Notification_Trigger_Setting__c();
            custObj.Is_Case_Comment_Notification_Active__c = false;
            custObj.Is_File_Notification_Active__c = true;
            
            Id RecordTypeIdCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Internal Service Request').getRecordTypeId();
            List<Case> cases = new List<Case>{
                new Case(Status = 'New', Date_Required__c = System.today(),recordtypeid =RecordTypeIdCase,contactId = con.Id),
                new Case(Status = 'New', Date_Required__c = System.today(),recordtypeid =RecordTypeIdCase,contactId = con.Id),
                new Case(Status = 'New', Date_Required__c = System.today(),recordtypeid =RecordTypeIdCase,contactId = con.Id)
            };
            insert cases;
            
            ContentVersion conVer = new ContentVersion();
            conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
            conVer.PathOnClient = 'Test File 1'; // The files name, extension is very important here which will help the file in preview.
            conVer.Title = 'Test File 1'; // Display name of the files
            conVer.VersionData = Blob.valueOf('Unit Test'); // converting your binary string to Blog
            conVer.NetworkId = networkMembers[0].NetworkId;
            insert conVer;
            
            List<ContentVersion> conDocList = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id];
        
            //Create ContentDocumentLink
            ContentDocumentLink cDe = new ContentDocumentLink();
            cDe.ContentDocumentId = conDocList[0].ContentDocumentId;
            cDe.LinkedEntityId = cases[0].Id; // you can use objectId,GroupId etc
            cDe.ShareType = 'V'; // Inferred permission, checkout description of ContentDocumentLink object for more details
            cDe.Visibility = 'AllUsers';
            insert cDe;
            
            conVer = new ContentVersion();
            conVer.ContentDocumentId = conDocList[0].ContentDocumentId;
            conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
            conVer.PathOnClient = 'Test File 2'; // The files name, extension is very important here which will help the file in preview.
            conVer.Title = 'Test File 2'; // Display name of the files
            conVer.VersionData = Blob.valueOf('Unit Test'); // converting your binary string to Blog
            conVer.NetworkId = networkMembers[0].NetworkId;
            insert conVer;
            
            conDocList = [SELECT Review_Complete__c , ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id];
            system.assertEquals(false, conDocList[0].Review_Complete__c );
            List<Case_Monitoring__c> caseMonitoringRecs = [SELECT Id, Review_Complete__c, Acknowledge_Date__c FROM Case_Monitoring__c WHERE Parent_Id__c = :conVer.Id];
            system.assertEquals(false, caseMonitoringRecs[0].Review_Complete__c);
            system.assertEquals(null, caseMonitoringRecs[0].Acknowledge_Date__c);
            
            conVer.Review_Complete__c  = true;
            update conVer;
            
            caseMonitoringRecs = [SELECT Id, Review_Complete__c, Acknowledge_Date__c FROM Case_Monitoring__c WHERE Parent_Id__c = :conVer.Id];
            system.assertEquals(true, caseMonitoringRecs[0].Review_Complete__c);
            //system.assertEquals(system.today(), caseMonitoringRecs[0].Acknowledge_Date__c);
            
            conVer.Review_Complete__c  = false;
            update conVer;
            
            caseMonitoringRecs = [SELECT Id, Review_Complete__c, Acknowledge_Date__c FROM Case_Monitoring__c WHERE Parent_Id__c = :conVer.Id];
            system.assertEquals(false, caseMonitoringRecs[0].Review_Complete__c);
            system.assertEquals(null, caseMonitoringRecs[0].Acknowledge_Date__c);
        }
        Test.stopTest();
    }
    /**
    * Method Name : setCaseFlagForCommunityUsersPositiveTest
    * Parameters  : 
    * Description : This method is created to test the positive scenarios of setCaseFlagForCommunityUsers method
    * Created By  : VennScience_BFL_Amruta
    * Created On  : 190719
    **/
    @isTest
    public static void setCaseFlagForCommunityUsersPositiveTest() {

        // Variable Declarations
        List<ContentVersion> listConVersionToBeInserted = new List<ContentVersion>();
        List<ContentDocumentLink> listContentDocs = new List<ContentDocumentLink>();
        List<Case> listCases = new List<Case>();
        Set<Id> setCaseId = new Set<Id>();
        Set<Id> setConVersionId = new Set<Id>();
        Boolean isCaseUpdated = false;

        // Create Case records
        Id RecordTypeIdCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Internal Service Request').getRecordTypeId();

        for(Integer i=0; i < 5; i++) {
            Case objCase = new Case();
            objCase.Subject = 'Test Case 00'+i;
            objCase.Description = 'Test Case Description 00'+i;
            objCase.recordtypeid = RecordTypeIdCase;
            listCases.add(objCase);
        }
        insert listCases;
        // Fetch inserted case id
        for(Case objInsertedCase : listCases) {
            setCaseId.add(objInsertedCase.Id);
        }

        // Create Content Version records
        for(Integer i=0; i < 5; i++) {
            ContentVersion conVer = new ContentVersion();
            conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
            conVer.PathOnClient = 'Test File 00'+i; // The files name, extension is very important here which will help the file in preview.
            conVer.Title = 'Test File 00'+i; // Display name of the files
            conVer.VersionData = Blob.valueOf('Unit Test'); // converting your binary string to Blog
            conVer.SharingPrivacy = 'N'; // Set the File sharing option
            listConVersionToBeInserted.add(conVer);
        }
        insert listConVersionToBeInserted;
        System.debug('listConVersionToBeInserted=========='+listConVersionToBeInserted);
        for(ContentVersion objcontentVersion : listConVersionToBeInserted) {
            setConVersionId.add(objcontentVersion.Id);
        }

        List<ContentVersion> conDocList = [SELECT ContentDocumentId 
                                             FROM ContentVersion 
                                            WHERE Id IN :setConVersionId];

        // Create ContentDocumentLink data
        for(Integer i=0; i < 5; i++) {
            ContentDocumentLink cDe = new ContentDocumentLink();
            if(!conDocList.isEmpty()) {
                cDe.ContentDocumentId = conDocList[i].ContentDocumentId;
            }
            if(!listCases.isEmpty()) {
                cDe.LinkedEntityId = listCases[i].Id;
            }
            cDe.ShareType = 'V'; // Inferred permission, checkout description of ContentDocumentLink object for more details
            cDe.Visibility = 'AllUsers';
            listContentDocs.add(cDe);
        }
        insert listContentDocs;

        // Fetch Case records
        List<Case> listUpdatedCases = new List<Case>();
        listUpdatedCases = [SELECT Id,
                                   Needs_Review__c
                              FROM Case
                             WHERE Id IN:setCaseId];
        System.debug('listUpdatedCases======='+listUpdatedCases);
        // Assert: Check if Case flag has been updated
        for(Case objUpdatedCase : listUpdatedCases) {
            System.assertEquals(true, objUpdatedCase.Needs_Review__c, 'The value of Needs Review should be true.');
        }
    }
    /**
    * Method Name : setCaseFlagForCommunityUsersNegativeTest
    * Parameters  : 
    * Description : This method is created to test the negative scenarios of setCaseFlagForCommunityUsers method
    * Created By  : VennScience_BFL_Amruta
    * Created On  : 190719
    **/
    @isTest
    public static void setCaseFlagForCommunityUsersNegativeTest() {

        // Variable Declarations
        List<ContentVersion> listConVersionToBeInserted = new List<ContentVersion>();
        List<ContentDocumentLink> listContentDocs = new List<ContentDocumentLink>();
        List<Case> listCases = new List<Case>();
        Set<Id> setCaseId = new Set<Id>();
        Set<Id> setConVersionId = new Set<Id>();
        Boolean isCaseUpdated = false;

        // Create Case records
        Id RecordTypeIdCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Internal Service Request').getRecordTypeId();

        for(Integer i=0; i < 5; i++) {
            Case objCase = new Case();
            objCase.Subject = 'Test Case 00'+i;
            objCase.Description = 'Test Case Description 00'+i;
            objCase.recordtypeid = RecordTypeIdCase;
            listCases.add(objCase);
        }
        insert listCases;
        // Fetch inserted case id
        for(Case objInsertedCase : listCases) {
            setCaseId.add(objInsertedCase.Id);
        }

        // Create Content Version records
        for(Integer i=0; i < 5; i++) {
            ContentVersion conVer = new ContentVersion();
            conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
            conVer.PathOnClient = 'Test File 00'+i; // The files name, extension is very important here which will help the file in preview.
            conVer.Title = 'Test File 00'+i; // Display name of the files
            conVer.VersionData = Blob.valueOf('Unit Test'); // converting your binary string to Blog
            conVer.SharingPrivacy = 'P'; // Set the File sharing option
            listConVersionToBeInserted.add(conVer);
        }
        insert listConVersionToBeInserted;
        System.debug('listConVersionToBeInserted=========='+listConVersionToBeInserted);
        for(ContentVersion objcontentVersion : listConVersionToBeInserted) {
            setConVersionId.add(objcontentVersion.Id);
        }

        List<ContentVersion> conDocList = [SELECT ContentDocumentId 
                                             FROM ContentVersion 
                                            WHERE Id IN :setConVersionId];

        // Create ContentDocumentLink data
        for(Integer i=0; i < 5; i++) {
            ContentDocumentLink cDe = new ContentDocumentLink();
            if(!conDocList.isEmpty()) {
                cDe.ContentDocumentId = conDocList[i].ContentDocumentId;
            }
            if(!listCases.isEmpty()) {
                cDe.LinkedEntityId = listCases[i].Id;
            }
            cDe.ShareType = 'V'; // Inferred permission, checkout description of ContentDocumentLink object for more details
            cDe.Visibility = 'AllUsers';
            listContentDocs.add(cDe);
        }
        insert listContentDocs;

        // Fetch Case records
        List<Case> listUpdatedCases = new List<Case>();
        listUpdatedCases = [SELECT Id,
                                   Needs_Review__c
                              FROM Case
                             WHERE Id IN:setCaseId];
        System.debug('listUpdatedCases======='+listUpdatedCases);
        // Assert: Check if Case flag has not updated as the file sharing option was set to Private
        for(Case objUpdatedCase : listUpdatedCases) {
            System.assertEquals(false, objUpdatedCase.Needs_Review__c, 'The value of Needs Review should be false.');
        }
    }
}