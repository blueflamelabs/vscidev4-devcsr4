/*********************************************************************************************************************************************************
*     Version    CreateDate/Modified     CreatedBy                    Description
*       1.0         160719               VennScience_BFL_Monali       This class is built to display all users related to logged in community user's Account
*                                                                     and create new community User.
*       2.0 		010819               VennScience_BFL_Monali       Worked on validation on fields.
*       3.0			020819				 VennScience_BFL_Monali  	  Created 'getCustomMetaDataProfile' method to get the metadataRecords.
																	  Modified 'saveNewRelatedCommunityUser' to create new user.
**********************************************************************************************************************************************************/ 

public without sharing class CreateCommunityController {
    
    /**
   * Method Name : getCUManageUsers
   * Description : Used to check the current user's field value and return boolean.
   * Created By  : VennScience_BFL_Monali 
   * Created On  : 160719
   **/

    @AuraEnabled
    public static Boolean getCUManageUsers(){
        Boolean checkDefaultProfile = false;
        Boolean IsProfile = false;
        User currentUser = [SELECT  Id, 
                                    Name,
                                    FirstName, 
                                    LastName,
                                    IsActive,
                                    ContactId,
                                    Contact.AccountId,
                                    Profile.Name
                            FROM User 
                            WHERE Id =: UserInfo.getUserId()];
        
        List<Community_User_MetaData__mdt> profileMetaDataList = [SELECT id, 
                            								   Profile_ID__c,
                                                        	   Default__c,
                                                               CU_Admin__c
                                                        FROM Community_User_MetaData__mdt];
        for(Community_User_MetaData__mdt metaDtaObj : profileMetaDataList) {
            if(currentUser.Profile.Name == metaDtaObj.Profile_ID__c && metaDtaObj.CU_Admin__c) {
                IsProfile = true;
            }
        }
        
        if(IsProfile) {
            checkDefaultProfile =  true;
        }else {
            checkDefaultProfile =  false;
        }
        return checkDefaultProfile;
    }
    
    /**
   * Method Name : getCustomMetaDataProfile
   * Description : Used to get the MetaData records to compare the profile.
   * Created By  : VennScience_BFL_Monali 
   * Created On  : 020819
   **/
    @AuraEnabled
    public static List<Community_User_MetaData__mdt> getCustomMetaDataProfile() {
        
        List<Community_User_MetaData__mdt> profileMetaDataList = [SELECT id, 
                            								   Profile_ID__c,
                                                        	   Default__c,
                                                               CU_Admin__c
                                                        FROM Community_User_MetaData__mdt];
        
        return profileMetaDataList;
    }
    
    /**
   * Method Name : getCurrentLoggedInUsers
   * Description : Used to get the User list and display on the table.
   * Created By  : VennScience_BFL_Monali 
   * Created On  : 160719
   **/
    @AuraEnabled
    public static List<userAndContactWrapper> getCurrentLoggedInUsers(){
        Id accountId = '0015400000IMf5j' ;
        Set<Id> setOfContactIds = new Set<Id>();
        List<userAndContactWrapper> listOfUserWrapper = new List<userAndContactWrapper>();
        userAndContactWrapper wrapperList = new userAndContactWrapper();
        List<User> userList = new List<User>();
        // T-00154 - 080819 - VennScience_BFL_Amruta - Variable Declarations
        List<Contact> listFilteredContact = new List<Contact>();
        Set<Id> setFilteredContactId = new Set<Id>();
        
        User currentUser = [SELECT id, 
                            Name,
                            FirstName, 
                            LastName,
                            IsActive,IsPortalEnabled,
                            ContactId,
                            Contact.AccountId
                            FROM User 
                            WHERE id =: UserInfo.getUserId()];
        if(currentUser.ContactId != null) {
            
            accountId = currentUser.Contact.AccountId;
            wrapperList.contactWrapperList = [SELECT Id, 
                                      				 FirstName, 
                                      				 LastName,
                                                     Name,
                                              		 Email,
                                      				 AccountId,
                                              		 Phone,
                                              		 MobilePhone,
                                              		 Title,
                                                     IsActiveForToggle__c
                                      			FROM Contact 
                                     			WHERE AccountId =: accountId ];
            for(Contact con : wrapperList.contactWrapperList) {
                setOfContactIds.add(con.Id);
            }
            wrapperList.userWrapperList = [SELECT Id,
                                           		  Name,
                                                  IsActive,
                                                  IsPortalEnabled,
                                                  ContactId, Contact.AccountId,
                                                  Contact.Name,
                                                  Contact.Title,
                                                  Contact.Email,
                                                 Contact.Phone,
                                                 Profile.Name
                                            FROM User 
                                            WHERE ContactId IN : setOfContactIds order by createdDate desc];
        }
        // T-00154 - 080819 - VennScience_BFL_Amruta - Iterate over userlist
        for(User getUser : wrapperList.userWrapperList) {
            setFilteredContactId.add(getUser.ContactId);
        }
        for(Contact getContact : wrapperList.contactWrapperList) {
            if(!setFilteredContactId.contains(getContact.Id)) {
                listFilteredContact.add(getContact);
            }
        } // End of for
        wrapperList.contactWrapperList.clear();
        wrapperList.contactWrapperList.addAll(listFilteredContact);
        listOfUserWrapper.add(wrapperList);
        return listOfUserWrapper;
        
    }
    
    /**
   * Method Name : updateSelectedUser
   * Parameters  : param1: string userId,boolean status
   * Description : Used to udate the User for activate or deactivate.
   * Created By  : VennScience_BFL_Monali 
   * Created On  : 160719
   **/
    @AuraEnabled
    public static void updateSelectedUser(string userId,boolean status){
        try{
            User newUser = [SELECT Id, 
                            Name,
                            IsActive  
                            FROM User 
                            WHERE Id=: userId LIMIT 1];
            if(newUser !=null){
                newUser.IsActive = status;
                update newUser;
            }
            
        }
        catch(Exception ex){}
    }
    
    /**
   * Method Name : getPicklistvalues
   * Parameters  : param1: String objectName, String field_apiname
   * Description : Used to get the picklistvalues.
   * Created By  : VennScience_BFL_Monali 
   * Created On  : 160719
   **/
    @AuraEnabled
    public static List<PickListWrapper> getTimeZonePicklistvalues(String objectName, String field_apiname) {
        
        
        List<PickListWrapper> optionlist = new List<PickListWrapper>();
        
        Map<String,Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe(); 
        Map<String, Schema.SObjectField> field_map = globalDescribe.get(objectName.toLowerCase()).getDescribe().fields.getMap(); 
        
        List<Schema.PicklistEntry> picklistValues = field_map.get(field_apiname).getDescribe().getPickListValues();
        
        //optionlist.add(new PickListWrapper('--None--','--None--'));
        
        for (Schema.PickListEntry pickValue : picklistValues) {
            optionlist.add(new PickListWrapper(pickValue.getLabel(), pickValue.getValue()));
        }
        
        return optionlist;
    }
    
    /**
   * Method Name : saveNewRelatedCommunityUser
   * Parameters  : param1: String firstName, String lastName, String userEmail, String timeZone
   * Description : Used to create new User.
   * Created By  : VennScience_BFL_Monali 
   * Created On  : 160719
   **/
    @AuraEnabled
    public static SuccessMsgWrappper saveNewRelatedCommunityUser(String firstName, String lastName, String userEmail, String timeZone, String profileName, String contactId,
                                                                 String phone, String mobileNo, String title) {
        
        String baseNickName = '';
        String aliasStr = '';
        String nickName = '';
        Boolean isError = false;
        Contact newContact;
        Integer lengthOfLastName =  lastName.length();
        // 190819 - T-00555 - VennScience_BFL_Amruta - Added variables
        String strPhone = '';
        String strTitle = '';
        String mobileNumber = '';      
        // 190819 - T-00555 - VennScience_BFL_Amruta - Set variable values
        strPhone = phone;
        strTitle = title;
        mobileNumber = mobileNo;
              
        SuccessMsgWrappper displayMsg = new SuccessMsgWrappper();
        try{ 
            
            List<Profile> profile = [Select Id 
                                FROM Profile 
                                WHERE Name = :profileName];
            
            User currentUser = [SELECT id,
                                ContactId,
                                Contact.AccountId,
                                LocaleSidKey,
                                EmailEncodingKey,
                                LanguageLocaleKey
                                FROM User 
                                WHERE id =: UserInfo.getUserId()];
            
            newContact = new Contact();
            newContact.FirstName = firstName;
            newContact.LastName = lastName;
            newContact.Email = userEmail;
            newContact.MobilePhone = mobileNumber;
            newContact.Phone = strPhone;
            newContact.Title = strTitle;
            newContact.AccountId = currentUser.Contact.AccountId;
            // 190819 - T-00555 - VennScience_BFL_Amruta - Set Phone, Title and Mobile Number for contact record
            if(String.isNotBlank(contactId)) {
            	newContact.Phone = strPhone;
                if(String.isNotBlank(String.valueOf(mobileNumber))) {
                    newContact.MobilePhone = String.valueOf(mobileNumber);
                } // End of if
                if(String.isNotBlank(String.valueOf(strTitle))) {
                    newContact.Title = strTitle;
                } // End of if
        	} // End of if
            if(String.isNotBlank(contactId)){
                newContact.Id = contactId;
                update newContact; 
            } else {
                insert newContact;	
            }
            
            if(String.isNotBlank(firstName)) {
                aliasStr = firstName.substring(0, 1);
            }
            
            if(String.isNotBlank(lastName)) {
                for(Integer i=0; i<lengthOfLastName; i++) {
                    if(lengthOfLastName < 3){
                         aliasStr += lastName;
                    } else {
                        if(i <= 3) {
                        	aliasStr += lastName.substring(0, 4);
                        }
                    }
                    break;
                }
               
            }
            
            if(String.isNotBlank(userEmail)) {
                List<String> rt = userEmail.split('@');
                baseNickName = rt[1].substringbefore('.') + rt[1].substringafter('.');
                nickName  = rt[0] + baseNickName;
            }
            
            if(profile.size() > 0) {
                User newUser = new User();
                newUser.FirstName = firstName;
                newUser.LastName = lastName;
                newUser.Alias = aliasStr;
                newUser.Email = userEmail;
                newUser.Username = userEmail;
                newUser.CommunityNickname = nickName;
                newUser.TimeZoneSidKey = timeZone;
                newUser.LocaleSidKey = currentUser.LocaleSidKey;
                newUser.EmailEncodingKey = currentUser.EmailEncodingKey;
                newUser.LanguageLocaleKey = currentUser.LanguageLocaleKey;
                newUser.ProfileId = profile[0].Id; // System.Label.UserProfileId;//'00e0e000001FWZc';//p.Id;//
                newUser.ContactId = newContact.Id;
                newUser.Phone = strPhone;
                newUser.MobilePhone = String.valueOf(mobileNumber);
                newUser.Title = strTitle;
                
                insert newUser;
                displayMsg.successMsg = 'Community User has been Created successfully!';
            }else {
                isError = true;
                displayMsg.errorMsg = 'User is not created. Please contact Synergent for more help.';
            }
            
             
            
        }catch(Exception ex) {
            System.debug('====Exception=========' + ex);
            isError = true;
            if(ex.getMessage().contains('DUPLICATE_USERNAME')){
                displayMsg.errorMsg = 'This email address is already in use for another user, please enter another email addresses or contact Synergent for more help.'; 
            }else if(ex.getMessage().contains('INVALID_EMAIL_ADDRESS')) {
                displayMsg.errorMsg = 'Email: Invalid Email address.';
            }else if(ex.getMessage().contains('STRING_TOO_LONG')) {
                displayMsg.errorMsg = 'Alias: data value too large(max length=8)';
            }else if(ex.getMessage().contains('FIELD_INTEGRITY_EXCEPTION')){
                displayMsg.errorMsg = 'Username must be in the form of an email address (for example, john@acme.com)';
            }else if(ex.getMessage().contains('REQUIRED_FIELD_MISSING')){
                displayMsg.errorMsg = 'Required fields are missing';
            }else {
                displayMsg.errorMsg = 'User is not created. Please contact Synergent for more help.';
            }
            
        }
        if(isError) {
            if(contactId != newContact.Id) {
                Contact contacts = [SELECT Id
                              FROM Contact 
                             WHERE id =: newContact.Id];
                Delete contacts;
            }
        }
        return displayMsg;
    }
     
    @AuraEnabled
    public static UpdatedUserWrapper updateUserProfile(String userId, String profile) {
        UpdatedUserWrapper updateduserWrap = new UpdatedUserWrapper();
        String sussessMsg = '';
        List<Profile> profileList = [Select Id 
                                    FROM Profile 
                                    WHERE Name = :profile];
        
        User userObj = [SELECT id,
                        		ProfileId,
                                Profile.Name,
                                ContactId,
                                Contact.AccountId,
                                LocaleSidKey,
                                EmailEncodingKey,
                                LanguageLocaleKey
                                FROM User 
                                WHERE id =: userId];
        userObj.ProfileId = profileList[0].Id;
        update userObj;
        
        User userObj1 = [SELECT id,
                        		ProfileId,
                                Profile.Name,
                                ContactId,
                                Contact.AccountId,
                                LocaleSidKey,
                                EmailEncodingKey,
                                LanguageLocaleKey
                                FROM User 
                                WHERE id =: userObj.Id];
        
        updateduserWrap.updatedUser = userObj1;
        updateduserWrap.successMsg = 'User Profile Updated';
        updateduserWrap.isProfileEditable = true;
        System.debug(' == updateduserWrap=='+updateduserWrap);
        return updateduserWrap; 
    }
    /**
  * Method Name : getCompanyInformation
  * Parameters  : none
  * Description : Used to get Company Information.
  * Created By  : VennScience_BFL_Monali 
  * Created On  : 080819
  **/
   @AuraEnabled
   public static String getCompanyInformation(){
      Organization orgDetails = [SELECT Id, LanguageLocaleKey, TimeZoneSidKey FROM Organization WHERE Id = :UserInfo.getOrganizationId()];
      return orgDetails.TimeZoneSidKey;
  }
    
    public class userAndContactWrapper{
        @AuraEnabled
        public List<User> userWrapperList;
        @AuraEnabled
        public List<Contact> contactWrapperList;
        
    }
    public class SuccessMsgWrappper {
        @AuraEnabled
        public String successMsg;
        @AuraEnabled
        public String errorMsg;
        
    }
    
    public class UpdatedUserWrapper {
        @AuraEnabled
        public String successMsg;
        @AuraEnabled
        public User updatedUser; 
        @AuraEnabled
        public Boolean isProfileEditable;
        
    }
    
    public class PickListWrapper {
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String value;
        
        public PickListWrapper(String label, String val) {
            this.label = label;
            this.value = val;
        }
        
    }
    
}