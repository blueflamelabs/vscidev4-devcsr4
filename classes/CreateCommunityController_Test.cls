/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                    Description
*       1.0         160719      VennScience_BFL_Nisha       This class is built to test the functionality of create community user.
**********************************************************************************************************************************************************/ 
@isTest
public without sharing class CreateCommunityController_Test {
    
    // 210819 - T-00564 - VennScience_BFL_Amruta - Used to store the community profile name that is fetched from custom label
    public static final String PROFILENAME = Label.CommunityProfileName;
    @isTest
    public static void positiveTestfordisplayCommunityUser() {
        UserRole ur = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role',PortalType = 'None');
        insert ur;
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        
        User user;
        User user1;
        Account ac;
        Contact con;
        Contact cont;
        
        User portalAccountOwner1 = new User(
            UserRoleId = ur.Id,
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'test2@test.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1 ;
        
        
        Id p = [select id from profile where name = :PROFILENAME].id;
        
        System.runAs(portalAccountOwner1){
            ac = new Account(name ='BFL User', OwnerId = portalAccountOwner1.Id) ;
            insert ac; 
            
            con = new Contact(LastName ='testCon',AccountId = ac.Id);
            insert con;
            
            user = new User(alias = 'test123', email='test123@noemail.com',
                            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                            localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                            ContactId = con.Id,
                            timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
            
            insert user; 
        }System.runAs(user) {
            /*cont = new Contact(LastName ='testCon',AccountId = ac.Id);
            insert cont;
            
            user1 = new User(alias = 'test123', email='test12@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = cont.Id,
                             timezonesidkey='America/Los_Angeles', username='tester12@noemail.com');
            
            insert user1; */
            
            Test.startTest();
            Boolean currentUserManage = CreateCommunityController.getCUManageUsers();
            Boolean getmetadataValue = false;
            // 210819 - T-00564 - VennScience_BFL_Amruta - Fetch Custom MetaData Type for current user
            List<Community_User_MetaData__mdt> profileMetaDataList = [SELECT Id, 
                                                                             Profile_ID__c,
                                                                             Default__c,
                                                                             CU_Admin__c
                                                                        FROM Community_User_MetaData__mdt];
        //System.debug('==profileMetaDataList=='+profileMetaDataList);
        profileMetaDataList = CreateCommunityController.getCustomMetaDataProfile();
        String companyInformation = CreateCommunityController.getCompanyInformation();  
        CreateCommunityController.UpdatedUserWrapper userProfileMessage = CreateCommunityController.updateUserProfile(user.Id,'+Power Community User');
        for(Community_User_MetaData__mdt metaDtaObj : profileMetaDataList) {
            if(user.Profile.Name == metaDtaObj.Profile_ID__c && metaDtaObj.CU_Admin__c) {
                getmetadataValue = true;
            }
        }
            System.assertEquals(getmetadataValue, currentUserManage);
        }
    }
    
    @isTest
    public static void positiveTestforpicklistField() {
        UserRole ur = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role',PortalType = 'None');
        insert ur;
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        
        User user;
        User user1;
        Account ac;
        Contact con;
        Contact cont;
        
        User portalAccountOwner1 = new User(
            UserRoleId = ur.Id,
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'test2@test011.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1 ;
        
        
        Id p = [select id from profile where name = :PROFILENAME].id;
        
        System.runAs(portalAccountOwner1){
            ac = new Account(name ='BFL User', OwnerId = portalAccountOwner1.Id) ;
            insert ac; 
            
            con = new Contact(LastName ='testCon',AccountId = ac.Id);
            insert con;
            
            user = new User(alias = 'test123', email='test123@noemail.com',
                            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                            localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                            ContactId = con.Id,
                            timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
            
            insert user; 
        }System.runAs(user) {
            
            List<Profile> profile = [Select Id 
                                FROM Profile 
                                WHERE Name = :PROFILENAME];
            System.debug('profile in test class---'+profile.size());
            Test.startTest();
            String contactId = con.Id;
            String userProfileName = user.Profile.Name;
            List<CreateCommunityController.userAndContactWrapper> wrapperList = CreateCommunityController.getCurrentLoggedInUsers(); 
            CreateCommunityController.updateSelectedUser(user.Id, false);
            CreateCommunityController.SuccessMsgWrappper strmsg = CreateCommunityController.saveNewRelatedCommunityUser('testNew', 'Testing',  'test123@noemail.com', 'America/Los_Angeles',PROFILENAME,contactId,
                                                                  '1234567','9087890989','Test Title');
            List<CreateCommunityController.PickListWrapper> wrapp =  CreateCommunityController.getTimeZonePicklistvalues('User', 'timezonesidkey');
            
            System.assert(wrapp != null);
            // System.assertEquals('Community User has been Created successfully!', strmsg.successMsg);
        }
    }
}