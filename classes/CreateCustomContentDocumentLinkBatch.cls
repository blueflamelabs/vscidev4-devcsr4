global class CreateCustomContentDocumentLinkBatch implements Database.Batchable<sObject> {
	
	String query;
	
	global CreateCustomContentDocumentLinkBatch() {
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		query = 'SELECT Id FROM ContentDocument';
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<ContentDocument> scope) {
   		System.debug('scope======='+scope);
   		System.debug('scope size======='+scope.size());

   		// Variable declarations
   		Set<Id> contentDocIds = new Set<Id>();
   		Set<Id> setLinkedEntityIds = new Set<Id>();
   		List<ContentDocumentLink> listContentDocLink = new List<ContentDocumentLink>();
   		List<Custom_Content_Document_Link__c> listCustomContentDocLinkToInsert = new List<Custom_Content_Document_Link__c>();
   		Map<Id,Case> mapCaseIdVSCase = new Map<Id,Case>();

   		// Fetch ContentDocumentId
   		for(ContentDocument objDoc : scope) {
   			contentDocIds.add(objDoc.Id);
   		}
   		// Fetch ContentDocumentLink records
   		listContentDocLink = [SELECT Id,
   									 ContentDocumentId,
   									 LinkedEntityId,
   									 ShareType,
   									 Visibility
								FROM ContentDocumentLink
							   WHERE ContentDocumentId IN :contentDocIds
							     AND LinkedEntityId IN ( SELECT Id FROM Case)];
	    System.debug('listContentDocLink======'+listContentDocLink);
	    System.debug('listContentDocLink size======'+listContentDocLink.size());

	    // Fetch LinkedEntityId from ContentDocumentLink
	    for(ContentDocumentLink objContentDocId : listContentDocLink) {
	    	setLinkedEntityIds.add(objContentDocId.LinkedEntityId);
	    }
	    System.debug('setLinkedEntityIds======'+setLinkedEntityIds);
	    // Fetch Case records that are linked with ContentDocument
	    for(Case objCase : [SELECT Id, CaseNumber FROM Case WHERE Id IN :setLinkedEntityIds]) {
	    	mapCaseIdVSCase.put(objCase.Id, objCase);
	    }
	    System.debug('mapCaseIdVSCase======'+mapCaseIdVSCase);
	    

	    // Filter only those contentDocumentLink records which are linked with Case object
	    for(ContentDocumentLink objContentDoc : listContentDocLink) {
	    	Custom_Content_Document_Link__c objCustomContentDocLink = new Custom_Content_Document_Link__c();
	    	objCustomContentDocLink.ContentDocumentId__c = objContentDoc.ContentDocumentId;
	    	objCustomContentDocLink.LinkedEntityId__c = objContentDoc.LinkedEntityId;
	    	if(mapCaseIdVSCase.containsKey(objContentDoc.LinkedEntityId)) {
	    		objCustomContentDocLink.Case_Number__c = mapCaseIdVSCase.get(objContentDoc.LinkedEntityId).CaseNumber;
	    	}
	    	listCustomContentDocLinkToInsert.add(objCustomContentDocLink);
	    }
	    System.debug('listCustomContentDocLinkToInsert===='+listCustomContentDocLinkToInsert);
	    System.debug('listCustomContentDocLinkToInsert size===='+listCustomContentDocLinkToInsert.size());
	    try {
	    	// Insert custom content document link records
	    	insert listCustomContentDocLinkToInsert;
	    	System.debug('Custom content document link insertion is successful');
    	} catch(Exception e) {
    		System.debug('Custom content document link insertion failed because of the following error:'+e.getMessage());
    	}
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}