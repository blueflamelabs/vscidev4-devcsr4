public without sharing class FilesController {
    Public Class FileWrapper {
        @AuraEnabled
        public String title;
        @AuraEnabled
        public String parentId;
        @AuraEnabled
        public String source;
        @AuraEnabled
        public String fileId;
        @AuraEnabled
        public String ownerId;
        @AuraEnabled
        public String ownerName;
        @AuraEnabled
        public DateTime lastModified;
        @AuraEnabled
        public Integer contentSize;
        @AuraEnabled
        public String caseMoniterId;
        @AuraEnabled
        public Boolean fileReviewd;
        @AuraEnabled
        public String fileType;//070619-T-00185-to get the filetype of the file.
        @AuraEnabled
        public String filePrivacy;
        
    }
    @AuraEnabled
    public static List<FileWrapper> getFilesRecords(String parentId){
        List<Case> caseRecord = [SELECT Id,CaseNumber FROM Case WHERE Id=:parentId];
        List<Case_Monitoring__c> moniteringRecords = [SELECT Id,Review_Complete__c,Parent_ID__c FROM Case_Monitoring__c WHERE Case__c =:parentId AND Parent_ID__c != null];
        List<String> fileIds = new List<String>();
        
        Map<Id,Case_Monitoring__c> caseMoniterMap = new Map<Id,Case_Monitoring__c>();
        
        for(Case_Monitoring__c cdl : moniteringRecords){
            if(cdl.Parent_ID__c != null){
                caseMoniterMap.put(cdl.Parent_ID__c,cdl);
            }
        }
        
        List<ContentVersion> files2 =  [SELECT Id, Title, 
                                        FileType, FileExtension, 
                                        Description,OwnerId,Owner.Name,
                                        LastModifiedDate, ContentSize,
                                        Review_Complete__c, ContentDocumentId, 
                                        IsLatest, ContentModifiedDate, toLabel(SharingPrivacy) 
                                        FROM ContentVersion 
                                        WHERE IsLatest = True and Id in :caseMoniterMap.keySet() ORDER BY ContentModifiedDate DESC];
        //system.debug('###'+files2.size());
        
        List<FileWrapper> fileWrapperList = new List<FileWrapper>();
        for(ContentVersion cv: files2){
            FileWrapper fw = new FileWrapper();
            fw.title = cv.Title;
            fw.parentId = ParentId;
            fw.source = caseRecord[0].CaseNumber; 
            fw.fileId = cv.ContentDocumentId;
            fw.ownerId = cv.OwnerId;
            fw.contentSize = cv.ContentSize;
            fw.ownerName = cv.owner.Name; 
            fw.lastModified = cv.LastModifiedDate;
            fw.caseMoniterId = caseMoniterMap.get(cv.Id).Id;
            fw.fileReviewd = caseMoniterMap.get(cv.Id).Review_Complete__c;
            fw.fileType = cv.FileType;
            fw.filePrivacy = cv.SharingPrivacy;
            fileWrapperList.add(fw);
        }
        return fileWrapperList;
    }
    @AuraEnabled
    Public static void updateCaseRecord(String parentId,list<String> fileIds,Boolean isCommunityUser){

        
    }
    
    @AuraEnabled
    public static FilesControllerWrapper getFiles(String parentId) {
        system.debug(parentId);
        List<ContentDocumentLink> files = 
            [SELECT ContentDocumentId, LinkedEntityId  FROM ContentDocumentLink where LinkedEntityId = :parentId];
        
        List<String> fileIds = new List<String>();
        
        for(ContentDocumentLink cdl : files){
            fileIds.add(cdl.ContentDocumentId);
        }
        
        List<ContentVersion> files2 = 
            [SELECT Id, Title, FileType, FileExtension, Description,OwnerId,Owner.Name,LastModifiedDate, ContentSize,Review_Complete__c, ContentDocumentId, IsLatest, ContentModifiedDate FROM ContentVersion where IsLatest = True and ContentDocumentId in :fileIds ORDER BY ContentModifiedDate DESC];
        
        FilesControllerWrapper fwrap = new FilesControllerWrapper();
        fwrap.isCommunityUser = (getPathPrefix() != '' ? true : false);
        fwrap.files = files2;

        return fwrap;
    }
    
    @AuraEnabled
    public static void saveFiles(List<ContentVersion> files,Id caseId) {
        
        update files;
        
      /*  List<ContentDocumentLink> filesList = 
            [SELECT ContentDocumentId, LinkedEntityId  FROM ContentDocumentLink where LinkedEntityId = :caseId];
        
        List<String> fileIds = new List<String>();
        
        for(ContentDocumentLink cdl : filesList){
            fileIds.add(cdl.ContentDocumentId);
        }
        
        List<ContentVersion> filesReviewedList = [SELECT Id,File_Reviewed__c FROM ContentVersion WHERE ContentDocumentId in:fileIds AND File_Reviewed__c = false];
      system.debug('$%%'+filesReviewedList.size());
        List<Case_Monitoring__c> caseMontiteringList = new List<Case_Monitoring__c>();
        if(filesReviewedList.size() > 0) {
            caseMontiteringList = [SELECT Id,Review_Complete__c FROM Case_Monitoring__c WHERE Case__c =:caseId AND Review_Complete__c = false];
        }
      system.debug('$%%'+caseMontiteringList.size());
        Case c = [SELECT Id,Case_Communication_Flag__c FROM Case WHERE Id =:caseId];
        if(caseMontiteringList.size() > 0 || filesReviewedList.size() > 0){
            if(c.Case_Communication_Flag__c == false) {
                c.Case_Communication_Flag__c = true;
                update c;
            }                
        } else {
             system.debug('$%%came');
            c.Case_Communication_Flag__c = false;
            update c;
        }*/
    }
    
    @AuraEnabled
    public static String getPathPrefix () {
        Id net_id = Network.getNetworkId();
        if (net_id  != null) {
            return [SELECT Id, UrlPathPrefix
                    FROM Network
                    WHERE Id = :net_id].UrlPathPrefix;
        }
        return '';
    }
    
    @AuraEnabled
    Public static void saveRecordCmRecord(String cmRecord){
        Case_Monitoring__c cmRecordVal = (Case_Monitoring__c) System.JSON.deserialize(cmRecord, Case_Monitoring__c.class);
        List<ContentVersion> cvList = [SELECT Id, Review_Complete__c FROM ContentVersion where IsLatest = True and Id = :cmRecordVal.Parent_ID__c];
        if(cvList.size() > 0){
            cvList[0].Review_Complete__c = cmRecordVal.Review_Complete__c;
            update cvList[0];
        }
        update cmRecordVal; 

    }
    @AuraEnabled
    Public static void saveRecord(Case_Monitoring__c cmRecord) {
        List<ContentVersion> cvList = [SELECT Id, Review_Complete__c FROM ContentVersion where IsLatest = True and Id = :cmRecord.Parent_ID__c];
        if(cvList.size() > 0){
            cvList[0].Review_Complete__c = cmRecord.Review_Complete__c;
            update cvList[0];
            update cmRecord;
        }
       
    }
    
     @AuraEnabled
    public static List<ContentVersion> getAllFiles(list<String> fileIds) {
        return [SELECT Id, Title, FileType,
                FileExtension, Description,
                ContentSize,Review_Complete__c,OwnerId,Owner.Name,LastModifiedDate,
                ContentDocumentId, IsLatest, ContentModifiedDate, SharingPrivacy
                FROM ContentVersion where IsLatest = True and ContentDocumentId in :fileIds 
                ORDER BY ContentModifiedDate DESC];

    }
    
     @AuraEnabled
    public static FilesControllerWrapper getFilesClone(String parentId) {
        system.debug(parentId);
        List<Case_Monitoring__c> moniteringRecords = [SELECT Id,
                                                      Review_Complete__c,Parent_ID__c 
                                                      FROM Case_Monitoring__c 
                                                      WHERE Case__c =:parentId AND Parent_ID__c != null];
        //System.debug('moniteringRecords====='+moniteringRecords);
        // 12062019- T-00252 - Added to check if logged in user is an internal user or community user 
        Boolean isInternalUser = true;
        isInternalUser = (getPathPrefix() == '' ? true : false);
       // System.debug('isInternalUser========='+isInternalUser);
        
        List<String> fileIds = new List<String>();
        
        Map<Id,Case_Monitoring__c> caseMoniterMap = new Map<Id,Case_Monitoring__c>();
        
        for(Case_Monitoring__c cdl : moniteringRecords){
            if(cdl.Parent_ID__c != null){
                caseMoniterMap.put(cdl.Parent_ID__c,cdl);
            }
        }
        //System.debug('caseMoniterMap======'+caseMoniterMap);
        // 120619 - T - 00252 - Added to check hide the files from community users if the file sharing option 
        // is set to Private on records
        
        List<ContentVersion> files2 = new List<ContentVersion>();
        if(isInternalUser) {
            
            // Check if user is internal user then display all files
            files2 = [SELECT Id, 
                             Title, 
                             FileType, 
                             FileExtension, 
                             Description,
                             OwnerId,
                             Owner.Name,
                             LastModifiedDate, 
                             ContentSize,
                             Review_Complete__c, 
                             ContentDocumentId, 
                             IsLatest,toLabel(SharingPrivacy), 
                             ContentModifiedDate 
                        FROM 
                      		 ContentVersion 
                       WHERE 
                      		 IsLatest = True AND Id in :caseMoniterMap.keySet() 
                    ORDER BY ContentModifiedDate DESC];
        } else {
            
            // Check if logged in user is not community user then display only those files whose sharing option is set
            // to visible to all
            files2 = [SELECT Id, 
                             Title, 
                             FileType, 
                             FileExtension, 
                             Description,
                             OwnerId,
                             Owner.Name,
                             LastModifiedDate, 
                             ContentSize,
                             Review_Complete__c, 
                             ContentDocumentId, 
                             IsLatest, toLabel(SharingPrivacy),
                             ContentModifiedDate 
                        FROM 
                      		 ContentVersion 
                       WHERE 
                      		 IsLatest = True AND Id in :caseMoniterMap.keySet() AND (SharingPrivacy != 'P' OR OwnerId = :UserInfo.getUserId())
                    ORDER BY ContentModifiedDate DESC];
        } // End of if
        
        FilesControllerWrapper fwrap = new FilesControllerWrapper();
        fwrap.isCommunityUser = (getPathPrefix() != '' ? true : false);
        fwrap.files = files2;
        fwrap.caseMoniterMap = caseMoniterMap;
        //System.debug('fwrap======='+fwrap);
        return fwrap;
    }
    
    //060619 - T - 00185-Method to delete the selected file.
    @AuraEnabled
    public static String deleteFile(String fileId){
        //System.debug('--193-'+fileId);
        
        ContentDocument contentObj = [SELECT Id, Title, FileExtension, CreatedDate From ContentDocument 
                                      WHERE id =: fileId];

       ContentVersion cvObj = [SELECT Id, 
                                ContentDocumentId
                                FROM ContentVersion 
                                WHERE  ContentDocumentId = :fileId]; 

        //System.debug('contentObj--'+cvObj);

        Case_Monitoring__c cmObj = [SELECT Id,
                                     Review_Complete__c,
                                     Parent_ID__c 
                                     FROM Case_Monitoring__c 
                                     WHERE Parent_ID__c =: cvObj.Id];
        
        //System.debug('cmObj--'+cmObj);
        Delete cmObj;
        //System.debug('cmObj---'+cmObj.Parent_ID__c);
        delete contentObj;

        String succMsg = 'File was deleted.';
        return succMsg;
    }
	
    
    @AuraEnabled 
    public static user fetchUser(){
     // query current user information  
      User oUser = [select id,Name,Email,FirstName,LastName,IsActive,IsPortalEnabled 
                    FROM User 
                    WHERE id =: userInfo.getUserId()];
        return oUser;
    }
    //190619 - T - 00185- Method to get the owner of the uplaoded file.
    @AuraEnabled
    public static Boolean getfileUse(String fileOwnerId){
        //System.debug('==fileOwnerId=='+fileOwnerId);
       
        User oUser = [select id,Name,Email,FirstName,LastName,IsActive,IsPortalEnabled,ContactId 
                      FROM User 
                      WHERE id =: fileOwnerId];
        if(oUser.ContactId != null){
            return true;
        }else{
            return false;
        }
    }
    
    public Class FilesControllerWrapper {
        @AuraEnabled
        public Boolean isCommunityUser;
        @AuraEnabled
        public List<ContentVersion> files;
        @AuraEnabled
        public Map<Id,Case_Monitoring__c> caseMoniterMap;
    } 
}