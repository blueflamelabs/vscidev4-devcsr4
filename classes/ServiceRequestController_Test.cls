@isTest(SeeAllData=true)
public class ServiceRequestController_Test 
{
	@isTest static void TestSaveECCCase()
    {
        string strSubject = 'testsub';
        string strDescription = 'testdesc';
        string strServiceArea = 'Check Processing';
        string strCategory = 'Adjustment';
        string strSubCategory = 'Other';	
        string strPriority = 'Normal - Business as Usual';
        
        Account objAccount = new Account();
        objAccount.name = 'some';
        insert objAccount; 
          
        Contact objContact = new Contact();
        objContact.LastName = 'xxx';
        objContact.AccountId = objAccount.Id;
        insert objContact;

        Id objContactId = objContact.Id;

		Electronic_Check_Collection_Batch__c obj = new Electronic_Check_Collection_Batch__c();
        
        obj.Create_Date__c = System.Date.today();
        obj.Person_Requesting_Action__c = objContactId;
        obj.Action_Requested__c = 	'Item Delete';
        //obj.Case__c = objCaseId;
        obj.AccountId__c = objContact.AccountId;
        obj.Name = 'ECC Submission Batch';
        obj.Batch_Dollar_Amount__c = 9;
        obj.Batch_Item_Count__c = 1;
        obj.Batch_Number__c = 'ccc';
        obj.Date_Scanned__c = System.date.today();
        obj.Name ='some';
        obj.Reason_For_Batch_Action__c = 'Other';
        

           List<Electronic_Check_Collection_Batch_Item__c> objItemList = new List<Electronic_Check_Collection_Batch_Item__c>();
        Electronic_Check_Collection_Batch_Item__c objItem = new Electronic_Check_Collection_Batch_Item__c();
        objItem.Name = 'some';
        objItem.Item_Action__c = 'Delete';
        //objItem.
        
        objItemList.Add(objItem);                                 				

        ID iSave = ServiceRequestController.SaveECCCase( strSubject,strDescription,strServiceArea,strCategory,strSubCategory,strPriority, obj,objItemList,objContactId);
    }
    
 	@isTest static void TestSaveManualCase()
    {
        string strSubject = 'testsub';
        string strDescription = 'testdesc';
        string strServiceArea = 'Check Processing';
        string strCategory = 'Adjustment';
        string strSubCategory = 'Other';	
        string strPriority = 'Normal - Business as Usual';
        
        Account objAccount = new Account();
        objAccount.name = 'some';
        insert objAccount; 
          
        Contact objContact = new Contact();
        objContact.LastName = 'xxx';
        objContact.AccountId = objAccount.Id;
        insert objContact;

        Id objContactId = objContact.Id;

           Manual__c obj = new Manual__c();
        	//insert obj;

                                 				
     ID iSave = ServiceRequestController.SaveManualCase( strSubject,strDescription,strServiceArea,strCategory,strSubCategory,strPriority,obj,objContactId);
         }
         
         
 	@isTest static void TestSaveAdjustmentCase()
    {
        string strSubject = 'testsub';
        string strDescription = 'testdesc';
        string strServiceArea = 'Check Processing';
        string strCategory = 'Adjustment';
        string strSubCategory = 'Other';	
        string strPriority = 'Normal - Business as Usual';
        
        Account objAccount = new Account();
        objAccount.name = 'some';
        insert objAccount; 
          
        Contact objContact = new Contact();
        objContact.LastName = 'xxx';
        objContact.AccountId = objAccount.Id;
        insert objContact;

        Id objContactId = objContact.Id;

        Adjustment__c obj = new Adjustment__c();
        //insert obj;
                                 				
         ID iSave = ServiceRequestController.SaveAdjustmentCase( strSubject,strDescription,strServiceArea,strCategory,strSubCategory,strPriority,obj,objContactId);
    }
    
	@isTest static void TestSaveDefaultCase()
    {
        string strSubject = 'testsub';
        string strDescription = 'testdesc';
        string strServiceArea = 'Check Processing';
        string strCategory = 'Adjustment';
        string strSubCategory = 'Other';	
        string strPriority = 'Normal - Business as Usual';
        
        Account objAccount = new Account();
        objAccount.name = 'some';
        insert objAccount; 
          
        Contact objContact = new Contact();
        objContact.LastName = 'xxx';
        objContact.AccountId = objAccount.Id;
        insert objContact;

        Id objContactId = objContact.Id;

        
        ID iSave = ServiceRequestController.SaveDefaultCase(  strSubject,strDescription,strServiceArea,strCategory,strSubCategory,strPriority, objContactId ); 
    }
}