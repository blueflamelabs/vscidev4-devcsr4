@isTest
private class SetCaseStatusFromPhase_Test 
{
	@isTest static void Test1() 
    {    
    	// Test data setup
        // Create an object
        Case objCase = new Case();
        insert objCase;
        Change_Management_Phase__c obj = new Change_Management_Phase__c(Name='Phase',
                                       Case__c=objCase.Id);
        insert obj;
       
        obj.Status__c = 'Initial Review Approval Pending';
        obj.Timing_Required__c = 'On';
        obj.Date_Required__c = System.now();
        obj.Change_Instructions__c = 'zzz';
        obj.Change_Application__c = 'None';
        
        update obj;
        
        // Perform test
        Test.startTest();
        //
        Test.stopTest();
        System.assertEquals( '1', '1');
    }
    
	@isTest static void Test2() 
    {    
    	// Test data setup
        // Create an object
        Case objCase = new Case();
        insert objCase;
        Change_Management_Phase__c obj = new Change_Management_Phase__c(Name='Phase',
                                       Case__c=objCase.Id);
        insert obj;
       
        obj.Status__c = 'Initial Review Approval';
        
                obj.Timing_Required__c = 'On';
        obj.Date_Required__c = System.now();
        obj.Change_Instructions__c = 'zzz';
        obj.Change_Application__c = 'None';

        update obj;
        
        // Perform test
        Test.startTest();
        //
        Test.stopTest();
        System.assertEquals( '1', '1');
    }
    
	@isTest static void Test3() 
    {    
    	// Test data setup
        // Create an object
        Case objCase = new Case();
        insert objCase;
        Change_Management_Phase__c obj = new Change_Management_Phase__c(Name='Phase',
                                       Case__c=objCase.Id);
        insert obj;
       
        obj.Status__c = 'Initial Review Approval Rejected';
        
                obj.Timing_Required__c = 'On';
        obj.Date_Required__c = System.now();
        obj.Change_Instructions__c = 'zzz';
        obj.Change_Application__c = 'None';

        update obj;
        
        // Perform test
        Test.startTest();
        //
        Test.stopTest();
        System.assertEquals( '1', '1');
    }
    
	@isTest static void Test4() 
    {    
    	// Test data setup
        // Create an object
        Case objCase = new Case();
        insert objCase;
        Change_Management_Phase__c obj = new Change_Management_Phase__c(Name='Phase',
                                       Case__c=objCase.Id);
        insert obj;
       
        obj.Status__c = 'Emergency Approval Pending';
        
                obj.Timing_Required__c = 'On';
        obj.Date_Required__c = System.now();
        obj.Change_Instructions__c = 'zzz';
        obj.Change_Application__c = 'None';

        update obj;
        
        // Perform test
        Test.startTest();
        //
        Test.stopTest();
        System.assertEquals( '1', '1');
    }
    
	@isTest static void Test5() 
    {    
    	// Test data setup
        // Create an object
        Case objCase = new Case();
        insert objCase;
        Change_Management_Phase__c obj = new Change_Management_Phase__c(Name='Phase',
                                       Case__c=objCase.Id);
        insert obj;
       
        obj.Status__c = 'Emergency Approval';
        
                obj.Timing_Required__c = 'On';
        obj.Date_Required__c = System.now();
        obj.Change_Instructions__c = 'zzz';
        obj.Change_Application__c = 'None';

        update obj;
        
        // Perform test
        Test.startTest();
        //
        Test.stopTest();
        System.assertEquals( '1', '1');
    }
    
	@isTest static void Test6() 
    {    
    	// Test data setup
        // Create an object
        Case objCase = new Case();
        insert objCase;
        Change_Management_Phase__c obj = new Change_Management_Phase__c(Name='Phase',
                                       Case__c=objCase.Id);
        insert obj;
       
        obj.Status__c = 'Emergency Approval Rejected';
        
                obj.Timing_Required__c = 'On';
        obj.Date_Required__c = System.now();
        obj.Change_Instructions__c = 'zzz';
        obj.Change_Application__c = 'None';

        update obj;
        
        // Perform test
        Test.startTest();
        //
        Test.stopTest();
        System.assertEquals( '1', '1');
    }
    
	@isTest static void Test7() 
    {    
    	// Test data setup
        // Create an object
        Case objCase = new Case();
        insert objCase;
        Change_Management_Phase__c obj = new Change_Management_Phase__c(Name='Phase',
                                       Case__c=objCase.Id);
        insert obj;
       
        obj.Status__c = 'Production Implementation Approval Pending';
        
                obj.Timing_Required__c = 'On';
        obj.Date_Required__c = System.now();
        obj.Change_Instructions__c = 'zzz';
        obj.Change_Application__c = 'None';

        update obj;
        
        // Perform test
        Test.startTest();
        //
        Test.stopTest();
        System.assertEquals( '1', '1');
    }
    
	@isTest static void Test8() 
    {    
    	// Test data setup
        // Create an object
        Case objCase = new Case();
        insert objCase;
        Change_Management_Phase__c obj = new Change_Management_Phase__c(Name='Phase',
                                       Case__c=objCase.Id);
        insert obj;
       
        obj.Status__c = 'Production Implementation Approval';
        
                obj.Timing_Required__c = 'On';
        obj.Date_Required__c = System.now();
        obj.Change_Instructions__c = 'zzz';
        obj.Change_Application__c = 'None';

        update obj;
        
        // Perform test
        Test.startTest();
        //
        Test.stopTest();
        System.assertEquals( '1', '1');
    }
    
	@isTest static void Test9() 
    {    
    	// Test data setup
        // Create an object
        Case objCase = new Case();
        insert objCase;
        Change_Management_Phase__c obj = new Change_Management_Phase__c(Name='Phase',
                                       Case__c=objCase.Id);
        insert obj;
       
        obj.Status__c = 'Production Implementation Approval Rejected';
        
                obj.Timing_Required__c = 'On';
        obj.Date_Required__c = System.now();
        obj.Change_Instructions__c = 'zzz';
        obj.Change_Application__c = 'None';

        update obj;
        
        // Perform test
        Test.startTest();
        //
        Test.stopTest();
        System.assertEquals( '1', '1');
    }
    
	@isTest static void Test10() 
    {    
    	// Test data setup
        // Create an object
        Case objCase = new Case();
        insert objCase;
        Change_Management_Phase__c obj = new Change_Management_Phase__c(Name='Phase',
                                       Case__c=objCase.Id);
        insert obj;
       
        obj.Status__c = 'Prod Team Approval Pending';
        
                obj.Timing_Required__c = 'On';
        obj.Date_Required__c = System.now();
        obj.Change_Instructions__c = 'zzz';
        obj.Change_Application__c = 'None';

        update obj;
        
        // Perform test
        Test.startTest();
        //
        Test.stopTest();
        System.assertEquals( '1', '1');
    }
    
	@isTest static void Test11() 
    {    
    	// Test data setup
        // Create an object
        Case objCase = new Case();
        insert objCase;
        Change_Management_Phase__c obj = new Change_Management_Phase__c(Name='Phase',
                                       Case__c=objCase.Id);
        insert obj;
       
        obj.Status__c = 'Prod Team Approval';
        
                obj.Timing_Required__c = 'On';
        obj.Date_Required__c = System.now();
        obj.Change_Instructions__c = 'zzz';
        obj.Change_Application__c = 'None';

        update obj;
        
        // Perform test
        Test.startTest();
        //
        Test.stopTest();
        System.assertEquals( '1', '1');
    }
    
	@isTest static void Test12() 
    {    
    	// Test data setup
        // Create an object
        Case objCase = new Case();
        insert objCase;
        Change_Management_Phase__c obj = new Change_Management_Phase__c(Name='Phase',
                                       Case__c=objCase.Id);
        insert obj;
       
        obj.Status__c = 'Prod Team Approval Rejected';
        
                obj.Timing_Required__c = 'On';
        obj.Date_Required__c = System.now();
        obj.Change_Instructions__c = 'zzz';
        obj.Change_Application__c = 'None';

        update obj;
        
        // Perform test
        Test.startTest();
        //
        Test.stopTest();
        System.assertEquals( '1', '1');
    }
    
    
    
    
    
    
    
    
    
    
    
    


}