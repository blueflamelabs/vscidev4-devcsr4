/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                    Description
*       1.0         160719      VennScience_BFL_Monali       This class is built for test utility.
**********************************************************************************************************************************************************/ 

@isTest
public class TestDataFactory {
    
    public static User getUserRec() {
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        
        User user = new User(
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'testuserRecord121@test.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        
        return user;
        
    }
    
    public static void getCommunityUser() {
        
    }
    
    public static Case getCaseRec() {
       
        // Get Account data
        List<Account> listAcc = new List<Account>();
        listAcc = getAccountRecords(5);
        insert listAcc;
        Id RecordTypeIdCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Internal Service Request').getRecordTypeId();
        Case caseObj = new Case();
        caseObj.Subject = 'Test Case';
        caseObj.Description = 'Test Case Description';
        caseObj.Status = 'Final Change Review Approval';
        caseObj.Origin = 'Email';
        if(!listAcc.isEmpty()) {
            caseObj.AccountId = listAcc[0].Id;
        }
        caseObj.Date_Required__c = Date.today();
        caseObj.RecordTypeId = RecordTypeIdCase;
        
        return caseObj;
    }
    
    /**
    * Method Name : getAccountRecords
    * Parameters  : Integer
    * Description : This method is used to create test Account records
    * Created By  : VennScience_BFL_Amruta
    * Created On  : 170719
    **/
    public static List<Account> getAccountRecords(Integer recCount) {

        List<Account> listAccounts = new List<Account>();
        for(Integer i=0; i < recCount; i++) {
            Account objAcc = new Account();
            objAcc.Name = 'Test Account'+i;
            listAccounts.add(objAcc);
        }
        return listAccounts;
    }
    public static Case_Monitoring__c getCaseMonitoringRec(Case csObj) {
        
        Case_Monitoring__c objMonitoring = new Case_Monitoring__c();
        objMonitoring.Create_Date__c = System.today();
        objMonitoring.Communication_Type__c = 'Case Comment';
        objMonitoring.Name = 'Case:' + csObj.CaseNumber + ' Case Comment';
        objMonitoring.Case__c = csObj.Id; 
        objMonitoring.Review_Complete__c = true;
        
        return objMonitoring;
        
    }
    
     /**
    * Method Name : getBillingItemsRecords
    * Parameters  : Integer
    * Description : This method is used to create test Account records
    * Created By  : VennScience_BFL_Amruta
    * Created On  : 210819
    **/
    public static Billing_Item__c getBillingItemsRecords() {

        Billing_Item__c billingItemRec = new Billing_Item__c();
        billingItemRec.Cost__c = 50;
        billingItemRec.Description__c = 'Test Description';
        billingItemRec.Item_Number__c = '001';
        billingItemRec.Category__c = 'Miscellaneous';
        return billingItemRec;
    }
    
    public static User createUserRec() {
        
        Profile profile1 = [Select Id from Profile where name = 'Standard User'];
        
        User user = new User(
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'testUserRecord2@test.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        
        return user;
        
    }
    
    public static Group createGroup() {
        Group groupRec = new Group(Name='My Group ', type='Queue');
        return groupRec;
    }
    
    public static GroupMember createGroupMember(Id userId, Id groupId) {
        GroupMember member = new GroupMember();
        member.UserOrGroupId = userId;
        member.GroupId = groupId;
        
        return member;
    }
    
    public static QueuesObject createQueueRec(Id groupId) {
       
        QueuesObject queueRec = new QueueSObject(QueueID = groupId, SobjectType = 'Case');
        
        return queueRec;
    }
    
    public static Account createAccount() {
        Account acct = new Account();
        acct.Name = 'Test Account';
        acct.Total_Contract_Hours__c = 3;
        
        return acct;
    }
    
    /**
    * Method Name : createCaseRec
    * Parameters  : 
    * Description : 
    * Created By  : VennScience_BFL_Monali 
    * Created On  : 101019
    **/
    public static Case createCaseRec(Id accountId, Id userId) {
       
                
        Id RecordTypeIdCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Development Request').getRecordTypeId();
        Case caseObj = new Case();
        caseObj.AccountId = accountId;
        caseObj.Subject = 'Test Case';
        caseObj.Description = 'Test Case Description';
        caseObj.Status = 'Released';
        caseObj.Origin = 'Email';
        caseObj.OwnerId = userId;
        caseObj.Estimated_Project_Hours__c = 4;
        caseObj.Estimated_Contract_Hours__c = 2;
        caseObj.RecordTypeId = RecordTypeIdCase;
        
        return caseObj;
    }
    
    public static CaseTeamRole createCaseTeamRoleRec() {
        CaseTeamRole caseTeamRole = new CaseTeamRole();
        caseTeamRole.Name = 'BA';
        caseTeamRole.AccessLevel = 'Edit';
        return caseTeamRole;
    }
    
    public static CaseTeamRole createCaseTeamRoleRec1() {
        CaseTeamRole caseTeamRole = new CaseTeamRole();
        caseTeamRole.Name = 'Sprint Backlog';
        caseTeamRole.AccessLevel = 'Edit';
        return caseTeamRole;
    }
    /**
    * Method Name : createCaseTeamMemberRec
    * Parameters  : 
    * Description : 
    * Created By  : VennScience_BFL_Monali 
    * Created On  : 101019
    **/
    public static CaseTeamMember createCaseTeamMemberRec(Id caseId, Id userId,Id caseTeamRole) {
        CaseTeamMember caseTeamRec = new CaseTeamMember();
        caseTeamRec.MemberId = userId;
        caseTeamRec.ParentId = caseId;
        caseTeamRec.TeamRoleId = caseTeamRole;
        return caseTeamRec;
        
    }
   
    public static List<CaseTeamMember> createCaseTeamMemberRecList(Id userId, Id caseTeamRole) {
        List<CaseTeamMember> caseTeamList = new List<CaseTeamMember>();
        for(Integer i = 0; i < 5; i++) {
            CaseTeamMember caseTeamRec = new CaseTeamMember();
            caseTeamRec.MemberId = userId;
            caseTeamRec.TeamRoleId = caseTeamRole;
            caseTeamList.add(caseTeamRec);
        }
        return caseTeamList;
        
    }
    /**
    * Method Name : createCaseRecList
    * Parameters  : 
    * Description : Used to Update the case record with all matching critiera.
    * Created By  : VennScience_BFL_Monali 
    * Created On  : 101019
    **/
    public static List<Case> createCaseRecList(Id accountId, Id userId) {
       List<Case> caseList = new List<Case>();
        Id RecordTypeIdCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Development Request').getRecordTypeId();
        for(Integer i=0; i< 5; i++){
            Case caseObj = new Case();
            caseObj.Subject = 'Test Case';
            caseObj.Description = 'Test Case Description';
            caseObj.Status = 'Released';
            caseObj.Origin = 'Email';
            caseObj.OwnerId = userId;
            caseObj.Estimated_Project_Hours__c = 4;
            caseObj.Estimated_Contract_Hours__c = 2;
            caseObj.AccountId = accountId;
            caseObj.RecordTypeId = RecordTypeIdCase;
            caseList.add(caseObj);
        }
        return caseList;
    }
}