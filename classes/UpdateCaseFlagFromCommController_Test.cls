/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                    Description
*     1.0        240719         VennScience_BFL_Amruta       This is the test class for UpdateCaseFlagFromCommunityController apex class.
*                              
**********************************************************************************************************************************************************/
@isTest
public class UpdateCaseFlagFromCommController_Test {
    
    /**
    * Method Name : getCaseFlagTest1
    * Parameters  : param1: Id
    * Description : Used to test the getCaseFlag method.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 240719
    **/
    @isTest
    public static void getCaseFlagTest1() {
        
        // Insert case record
        Case objCase = TestDataFactory.getCaseRec();
        objCase.Needs_Review__c = false;
        insert objCase;
        Test.startTest();
        Boolean caseFlagVal = UpdateCaseFlagFromCommunityController.getCaseFlag(objCase.Id);
        Test.stopTest();
        // Assert : Check if Needs_Review__c is false
        System.assertEquals(false, caseFlagVal, 'Case flag value is not correct');
    }
    /**
    * Method Name : getCaseFlagTest2
    * Parameters  : param1: Id
    * Description : Used to test the getCaseFlag method.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 240719
    **/
    @isTest
    public static void getCaseFlagTest2() {
        
        // Insert case record
        Case objCase = TestDataFactory.getCaseRec();
        objCase.Needs_Review__c = true;
        insert objCase;
        Test.startTest();
        Boolean caseFlagVal = UpdateCaseFlagFromCommunityController.getCaseFlag(objCase.Id);
        Test.stopTest();
        // Assert : Check if Needs_Review__c is false
        System.assertEquals(true, caseFlagVal, 'Case flag value is not correct');
    }
    /**
    * Method Name : updateCaseFlagValueTest
    * Parameters  : param1: Id
    * Description : Used to test the updateCaseFlagValue method.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 240719
    **/
    @isTest
    public static void updateCaseFlagValueTest() {
        
        // Insert case record
        Case objCase = TestDataFactory.getCaseRec();
        objCase.Needs_Review__c = false;
        insert objCase;
        Test.startTest();
        Boolean setCaseFlag = true;
        String strMsg = UpdateCaseFlagFromCommunityController.updateCaseFlagValue(objCase.Id, setCaseFlag);
        Test.stopTest();
        // Assert : Check if Needs_Review__c is false
        System.assertEquals('Case has been updated successfully', strMsg, 'Case flag not updated');
    }
}