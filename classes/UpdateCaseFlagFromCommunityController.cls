/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                    Description
*     1.0        240719         VennScience_BFL_Amruta       This is the controller class for UpdateCaseFlagFromCommunity Lightning Component. The purpose
*                                                            of this class is to update the "Needs Review" checkbox on Case object from community.
*                              
**********************************************************************************************************************************************************/
public without sharing class UpdateCaseFlagFromCommunityController {
    
    /**
    * Method Name : getCaseFlag
    * Parameters  : param1: Id
    * Description : Used to fetch the initial value of "Needs Review" checkbox on component initial load.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 240719
    **/
    @AuraEnabled
    public static Boolean getCaseFlag(Id caseId) {
        
        Case objCase = [SELECT Id,
                               Needs_Review__c
                         FROM  Case
                        WHERE  Id = :caseId];
        System.debug('objCase======='+objCase);
        if(objCase.Needs_Review__c) {
            return true;
        }else {
            return false;
        } // End of if
    }
     /**
    * Method Name : updateCaseFlagValue
    * Parameters  : param1: Id, param1: Boolean
    * Description : Used to update the "Needs Review" checkbox on Case obejct on click of checkbox from community.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 240719
    **/
    @AuraEnabled
    public static String updateCaseFlagValue(Id caseId, Boolean caseFlagVal) {
        
        System.debug('Inside updateCaseFlagValue');
        String strMessage = '';
        Case objCase = [SELECT Id,
                               Needs_Review__c
                         FROM  Case
                        WHERE  Id = :caseId];
        objCase.Needs_Review__c = caseFlagVal;
        try {
            update objCase;
            System.debug('Case update successfully');
            strMessage = 'Case has been updated successfully';
        } catch(Exception e) {
            System.debug('Error occurred while case update:'+e.getMessage());
            strMessage = 'Case not updated. Error occurred:'+e.getMessage();
        } // End of try-catch block
        return strMessage;
    }
}