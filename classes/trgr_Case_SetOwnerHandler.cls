/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                    Description
*       1.0         160719      VennScience_BFL_Nisha        This Handler is built to update the Case owner and Their related case Monitoring
**********************************************************************************************************************************************************/
public class trgr_Case_SetOwnerHandler {
    
    // T-00547 - 140819 - VennScience_BFL_Amruta - Added variable to handler recursion
    public static Boolean executeUpdateTrigger = true;
    // T-00547 - 160819 - VennScience_BFL_Amruta - Added constant vaariable to store the Case record type developer name
    public static final String RECORDTYPENAME = 'External_Service_Request';
    // T-00627 - 100919 - VennScience_BFL_Monali - Added variable to handler recursion
    public static Boolean isExecuteUpdateTrigger = false;
    public static List<Case > csRecords = new List<Case>();
    public Map<Id, Case> caseOldMap = new Map<Id, Case>();
    Map<Id, Id> mapOfCaseIds = new Map<Id, Id>();
    public trgr_Case_SetOwnerHandler (List<sObject> caseRecords, Map<Id, case> caseRecordsOldMap) {
        csRecords = (List<Case>)caseRecords;
        caseOldMap = caseRecordsOldMap;
    }
    
    /**
    * Method Name : updateCaseOwner
    * Parameters  : 
    * Description : This method is used to update all the related Case Monitoring records when Case owner is updated
    * Created By  : VennScience_BFL_Nisha
    * Created On  : 160719trgr_Case_SetOwnerHandler
    **/
    public void updateCaseOwner() {
        System.debug('Inside updateCaseOwner =======');
        for(Case obj : csRecords) {
            
            if (caseOldMap.get(obj.id).Status != obj.Status) {
                
                if ((caseOldMap.get(obj.id).Status == 'Final Change Review Approval') && (obj.Status == 'Production Change Verification' )) {
                    obj.OwnerId = obj.CreatedById; 
                    mapOfCaseIds.put(obj.Id, obj.OwnerId);
                }
                if ((caseOldMap.get(obj.id).Status == 'Final Change Review Approval') && (obj.Status == 'Closed' )) {
                    obj.OwnerId = obj.CreatedById; 
                    mapOfCaseIds.put(obj.Id, obj.OwnerId);
                }
                
            }
            if (caseOldMap.get(obj.id).OwnerId != obj.OwnerId) {
                mapOfCaseIds.put(obj.Id, obj.OwnerId);
            }
        }
        
        if(mapOfCaseIds.size() >0) {
            
            List<Case_Monitoring__c> objLstMonitoring = [SELECT Id, 
                                                         Case__c  
                                                         FROM Case_Monitoring__c 
                                                         WHERE Case__c IN : mapOfCaseIds.keySet()];
            for(Case_Monitoring__c cm : objLstMonitoring) {
                cm.OwnerId =  mapOfCaseIds.get(cm.Case__c);
            }
            System.debug('objLstMonitoring ======='+objLstMonitoring);
            if(!objLstMonitoring.isEmpty()) {
                System.debug('Inside if for update');
                isExecuteUpdateTrigger = true; 
                update objLstMonitoring;
                
            }
        }
    }
    /**
    * Method Name : updateCaseBillingRecords
    * Parameters  : 
    * Description : This method is used to update all the related Case Billing records when Case Account is updated
    * Created By  : VennScience_BFL_Amruta
    * Created On  : 170719
    **/
    public void updateCaseBillingRecords() {

        // Variable Declarations
        List<Case> listUpdatedAccountCases = new List<Case>();
        List<Billing__c> listChildBillingRecords = new List<Billing__c>();
        List<Billing__c> listBillingRecToBeUpdated = new List<Billing__c>();
        Set<Id> setUpdatedCaseId = new Set<Id>();
        Map<Id,Id> mapCaseIdVSUpdatedAccountId = new Map<Id,Id>();
        // T-00519 - 060819 - VennScience_BFL_Amruta - Added Variables
        Map<Id,List<Id>> mapAccIdVSCaseId = new Map<Id,List<Id>>();
        Map<Id,String> mapCaseIdVSCaseNumber = new Map<Id,String>();
        Map<Id,String> mapCaseIdVSAccName = new Map<Id,String>();
        List<Account> listAccount = new List<Account>();

        // Iterate over Trigger.New list to check if Case Account has been updated
        for(Case objNewCase : csRecords) {
            if(objNewCase.AccountId != caseOldMap.get(objNewCase.Id).AccountId) {
                listUpdatedAccountCases.add(objNewCase);
                setUpdatedCaseId.add(objNewCase.Id);
                mapCaseIdVSUpdatedAccountId.put(objNewCase.Id,objNewCase.AccountId);
                // T-00519 - 060819 - VennScience_BFL_Amruta - populate AccountId vs CaseId map
                if(!mapAccIdVSCaseId.containsKey(objNewCase.AccountId)) {
                    mapAccIdVSCaseId.put(objNewCase.AccountId, new List<Id>{objNewCase.Id});
                } else {
                    List<Id> listPreviousCaseId = new List<Id>();
                    listPreviousCaseId.addAll(mapAccIdVSCaseId.get(objNewCase.AccountId));
                    listPreviousCaseId.add(objNewCase.Id);
                    mapAccIdVSCaseId.put(objNewCase.AccountId,listPreviousCaseId);
                } // End of if
                // T-00519 - 060819 - VennScience_BFL_Amruta - populate CaseId vs CaseNumber map
                mapCaseIdVSCaseNumber.put(objNewCase.Id,objNewCase.CaseNumber);
            } // End of if
        } // End of for
        // System.debug('listUpdatedAccountCases======='+listUpdatedAccountCases);
         System.debug('mapCaseIdVSUpdatedAccountId======='+mapCaseIdVSUpdatedAccountId);
         System.debug('mapAccIdVSCaseId============='+mapAccIdVSCaseId);
        // System.debug('mapCaseIdVSCaseNumber============='+mapCaseIdVSCaseNumber);
        
        // T-00519 - 060819 - VennScience_BFL_Amruta - Fetch the updated Account's Name
        listAccount = [SELECT Id,
                              Name
                         FROM Account
                        WHERE Id IN :mapAccIdVSCaseId.keySet()];
        // T-00519 - 060819 - VennScience_BFL_Amruta - Iterate over Account list
        for(Account objAcc : listAccount) {
            for(Id caseId : mapAccIdVSCaseId.get(objAcc.Id)) {
                // Populate CaseId vs AccountName map
                mapCaseIdVSAccName.put(caseId,objAcc.Name);
            } // End of inner for            
        } // End of outer for
         System.debug('mapCaseIdVSAccName=========='+mapCaseIdVSAccName);
        
        // Fetch the related billing records
        listChildBillingRecords = [SELECT Id,
                                          Case__c
                                    FROM  Billing__c
                                   WHERE  Case__c IN :setUpdatedCaseId];
        System.debug('131 listChildBillingRecords=========='+listChildBillingRecords);
        // Update the Account on related Billing records
        for(Billing__c objBilling : listChildBillingRecords) {
            // T-00519 - 060819 - VennScience_BFL_Amruta - Added conditions in if statement
            if(mapCaseIdVSUpdatedAccountId.containsKey(objBilling.Case__c)
              && mapCaseIdVSAccName.containsKey(objBilling.Case__c) 
              && mapCaseIdVSCaseNumber.containsKey(objBilling.Case__c)) {
                objBilling.Name = mapCaseIdVSAccName.get(objBilling.Case__c)+':'+mapCaseIdVSCaseNumber.get(objBilling.Case__c)+' Billing';
                objBilling.Account__c = mapCaseIdVSUpdatedAccountId.get(objBilling.Case__c);
                listBillingRecToBeUpdated.add(objBilling);
            } // End of if
        } // End of for
        // System.debug('listBillingRecToBeUpdated======='+listBillingRecToBeUpdated);
        // Update the Billing records
        if(!listBillingRecToBeUpdated.isEmpty()) {
            try {
                update listBillingRecToBeUpdated;
                 System.debug('Billing records updated');
            } catch(Exception e) {
                System.debug('Error occurred while updating the billing records:'+e.getMessage());
            } // End of try-catch block
        } // End of if
    }
    /**
    * Method Name : checkNeedsReview
    * Parameters  : 
    * Description : This method is used to set the Needs Review checkbox to true when an External Service Request 
    *               Case is created by internal user
    * Created By  : VennScience_BFL_Amruta
    * Created On  : 140819
    **/
    public void checkNeedsReview() {
        
        System.debug('csRecords========'+csRecords);
        // T-00547 - 140819 - VennScience_BFL_Amruta - Variable Declarations
        List<User> listUsers = new List<User >();
        List<Case> listCasesToBeUpdated = new List<Case>();
        List<Case> listCases = new List<Case>();
        Set<Id> setCreatedById = new Set<Id>();
        Set<Id> setInternalUserId = new Set<Id>();
        Set<Id> setCaseId = new Set<Id>();
        // String recordTypeName = 'External_Service_Request';
        Id recordTypeId;
        
        // Fetch record type Id for External Service Request record type
        recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(RECORDTYPENAME).getRecordTypeId();
        // Iterate over case list
        for(Case caseRecord : csRecords) {
            if(caseRecord.RecordTypeId == recordTypeId && caseRecord.AccountId != null) {
                setCreatedById.add(caseRecord.CreatedById);  
                setCaseId.add(caseRecord.Id);
            } // End of if
        } // End of for
        // Check if the owner is internal user or not
        listUsers = [SELECT Id,
                            Name
                       FROM User
                      WHERE ContactId = null
                        AND Id IN :setCreatedById];
        // Iterate over internal users list
        for(User userRecord : listUsers) {
            setInternalUserId.add(userRecord.Id);
        } // End of for
        System.debug('setInternalUserId========'+setInternalUserId);
        // Iterate over Case list and set the Needs review to true when Case is created by Internal User
        for(Case caseRecord : csRecords) {
            if(setCaseId.contains(caseRecord.Id) && setInternalUserId.contains(caseRecord.CreatedById)) {
                listCasesToBeUpdated.add(new Case(Id = caseRecord.Id, Needs_Review__c = true));
            } // End of if
        } // End of for
        if(!listCasesToBeUpdated.isEmpty()) {
            try {
                System.debug('listCasesToBeUpdated ===='+listCasesToBeUpdated);
                 System.debug('Inside Case update in checkNeedsReview method');
                executeUpdateTrigger = false;
                update listCasesToBeUpdated;
                //executeUpdateTrigger = false;
            } catch(Exception e) {
                System.debug('Error occurred while updating Case record:'+e.getMessage());
            } // End of try-catch block
        } // End of if
    }
    
     /**
    * Method Name : caseOwnerUpdate
    * Parameters  : 
    * Description : This method is used to update Case owner when Case Team owner Role matches the "Development Request Assigned To"
    * Created By  : VennScience_BFL_Monali
    * Created On  : 091019
    **/
    public static void caseOwnerUpdate(List<Case> caseRecords, Map<Id, case> caseRecordsOldMap) {
        //Set<Id> setCaseId = new Set<Id>();
        Map<Id , Case> mapOfCase = new Map<Id , Case>();
        System.debug('Check a list of case ' +caseRecords);
        System.debug('Check a map  of case ' +caseRecordsOldMap);
        Id csrRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CSR').getRecordTypeId();
        for(Case caseRecord : caseRecords){
            if(caseRecord.RecordTypeId == csrRecordTypeId 
               && String.isNotBlank(caseRecord.Development_Request_Assigned_To__c)
               && caseRecord.Status != caseRecordsOldMap.get(caseRecord.Id).Status
              ) {
                  mapOfCase.put(caseRecord.Id, caseRecord);
              }
            System.debug('Check a case set of Id ' +mapOfCase);
        }
        // Call method to update Case Owner
        if(mapOfCase.size() > 0 )
        	updateCaseOwnerAsPerCaseTeam(mapOfCase);
    }
    
    /**
    * Method Name : updateCaseOwnerAsPerCaseTeam
    * Parameters  : param1: Map<Id ,Case>
    * Description : This method is used to update Case owner when Case Team owner Role matches the Case.DevelopmentRequestAssignedTo value
    * Created By  : VennScience_BFL_Monali
    * Created On  : 091019
    **/
    public static void updateCaseOwnerAsPerCaseTeam(Map<Id,Case> mapOfCaseId) {
        System.debug('after filter case record===' + mapOfCaseId);
        // Variable Declarations
        Boolean isTeamMemPresent = false;
        List<CaseTeamMember> listCaseTeamMember = new List<CaseTeamMember>();
        List<Case> listCaseForQueueAssignment = new List<Case>();
        List<User> listUser = new List<User>();
        List<Group> listBAQueue = new List<Group>();
        List<Group> listProgrammerQueue = new List<Group>();
        List<Group> listQueue = new List<Group>();
        Map<String,Id> mapQueueNameVSQueueRecord = new Map<String,Id>();
        Map<Id,List<CaseTeamMember>> mapCaseIdVSListCaseTeamMember = new Map<Id,List<CaseTeamMember>>();
        Map<Id,Set<Id>> mapUserIdVSCaseIdSet = new Map<Id,Set<Id>>();
        // T-000801 - 011119 - VennScience_BFL_Monali - Added variable.
        List<Case> listCaseforSprintBacklog = new List<Case>();
    	
        System.debug('mapOfCaseId======'+mapOfCaseId);
        // Fetch Case Team Members
        listCaseTeamMember = [SELECT Id,
                                     ParentId,
                                     TeamRoleId,
                                     MemberId,
                                     TeamRole.Name
                                FROM CaseTeamMember
                               WHERE ParentId IN: mapOfCaseId.keySet()];
        System.debug('listCaseTeamMember======'+listCaseTeamMember);
        // Check if no Case has Team Member assigned
        if(listCaseTeamMember.isEmpty() || listCaseTeamMember.size() == 0 ) {
            for(Case caseRecord : mapOfCaseId.values()) {
                listCaseForQueueAssignment.add(caseRecord);
            } // End of for
        } // End of if
        for(CaseTeamMember caseTeamMemRec : listCaseTeamMember) {
            if(!mapCaseIdVSListCaseTeamMember.containsKey(caseTeamMemRec.ParentId)) {
                mapCaseIdVSListCaseTeamMember.put(caseTeamMemRec.ParentId, new List<CaseTeamMember>{caseTeamMemRec});
            } else {
                List<CaseTeamMember> listPreviousTeamMember = new List<CaseTeamMember>();
                listPreviousTeamMember = mapCaseIdVSListCaseTeamMember.get(caseTeamMemRec.ParentId);
                listPreviousTeamMember.add(caseTeamMemRec);
                mapCaseIdVSListCaseTeamMember.put(caseTeamMemRec.ParentId, listPreviousTeamMember);
            } // End of if-else block
        } // End of for
        System.debug('mapCaseIdVSListCaseTeamMember========='+mapCaseIdVSListCaseTeamMember);
        
        // Check if any case has no Case Team Member assigned and then add it to list for Queue Assignment
        if(!mapCaseIdVSListCaseTeamMember.isEmpty()) {
        	for(Id caseId : mapOfCaseId.keySet()) {
                if(!mapCaseIdVSListCaseTeamMember.containsKey(caseId)) {
                    listCaseForQueueAssignment.add(mapOfCaseId.get(caseId));
                } // End of inner if
        	} // End of for
    	} // End of outer if
        
        // Iterate over map of CaseId VS Case Team Member List
        for(Id caseId : mapCaseIdVSListCaseTeamMember.keySet()) {
            isTeamMemPresent = false;
            for(CaseTeamMember caseTeamMemRec : mapCaseIdVSListCaseTeamMember.get(caseId)) {
                if(String.valueOf(caseTeamMemRec.MemberId).startsWith('005') &&
                   mapOfCaseId.containsKey(caseId) && 
                   caseTeamMemRec.TeamRole.Name == mapOfCaseId.get(caseId).Development_Request_Assigned_To__c) 
                   {
                       isTeamMemPresent = true;
                       System.debug('Team member role========'+caseTeamMemRec.TeamRole.Name);
                       System.debug('Case.Development_Request_Assigned_To__c========'+mapOfCaseId.get(caseId).Development_Request_Assigned_To__c);
                       // Assign Team Member as Owner to Case
                       if(!mapUserIdVSCaseIdSet.containsKey(caseTeamMemRec.MemberId)) {
                           mapUserIdVSCaseIdSet.put(caseTeamMemRec.MemberId, new Set<Id>{caseId});
                       } else {
                           Set<Id> setPreviousCaseId = new Set<Id>();
                           setPreviousCaseId = mapUserIdVSCaseIdSet.get(caseTeamMemRec.MemberId);
                           setPreviousCaseId.add(caseId);
                           mapUserIdVSCaseIdSet.put(caseTeamMemRec.MemberId, setPreviousCaseId);
                       } // End of if-else block
                } // End of if
            } // End of inner for
            if(!isTeamMemPresent && mapOfCaseId.containsKey(caseId)) {
                listCaseForQueueAssignment.add(mapOfCaseId.get(caseId));
            }
        } // End of outer for
        System.debug('mapUserIdVSCaseIdSet========'+mapUserIdVSCaseIdSet);
        System.debug('listCaseForQueueAssignment========'+listCaseForQueueAssignment);
        
        // Fetch Users related to Case Team and check if the users are active
        if(!mapUserIdVSCaseIdSet.isEmpty()) {
        	listUser = [SELECT Id
                              FROM User
                        WHERE Id IN :mapUserIdVSCaseIdSet.keySet()
                          AND IsActive = true];
    	} // End of if
        System.debug('listUser======='+listUser);
        // Iterate over users list
        for(User userRecord : listUser) {
            if(mapUserIdVSCaseIdSet.containsKey(userRecord.Id)) {
                for(Id caseId : mapUserIdVSCaseIdSet.get(userRecord.Id)) {
                    // Assign Case Owner
                    mapOfCaseId.get(caseId).OwnerId = userRecord.Id;
                    /*
                    Case caseToUpdate = new Case();
                    caseToUpdate.Id = caseId;
                    caseToUpdate.OwnerId = userRecord.Id;
                    System.debug('Case Id==='+caseId);
                    System.debug('Member Id==='+userRecord.Id);
					*/
                    System.debug('Case Owner has been assigned');
                } // End of inner if
            } // End of if
        } // End of for
    
        // Fetch Queue records
        // @Reminder: Move Queue names to custom label
        if(!listCaseForQueueAssignment.isEmpty()) {
            listQueue = [SELECT Id,
                                Name
                           FROM Group 
                          WHERE Type = 'Queue' 
                            /*AND (NAME = :Label.Sprint_Backlog
                             OR Name = 'Programmer Queue')];*/
                         AND NAME = :Label.Sprint_Backlog];
        } // End of if
        System.debug('listQueue========'+listQueue);
        
        //Iterate over Queue list
        for(Group grpRecord : listQueue) {
            /*if(grpRecord.Name == Label.Sprint_Backlog) {
                mapQueueNameVSQueueRecord.put('BA', grpRecord.Id);
            }
            if(grpRecord.Name == 'Programmer Queue') {
                mapQueueNameVSQueueRecord.put('Programmer', grpRecord.Id);
                mapQueueNameVSQueueRecord.put('Reviewing Programmer', grpRecord.Id);
            }*/
            mapQueueNameVSQueueRecord.put('BA', grpRecord.Id);
            mapQueueNameVSQueueRecord.put('Programmer', grpRecord.Id);
            mapQueueNameVSQueueRecord.put('Reviewing Programmer', grpRecord.Id);
        } // End of for
        
        System.debug('mapQueueNameVSQueueRecord========'+mapQueueNameVSQueueRecord);
        
        // Check if BA or Programmer Queue can be assigned to filtered Cases
        for(Case caseRec : listCaseForQueueAssignment) {
            System.debug('Inside for======='+caseRec);
            if(caseRec.Development_Request_Assigned_To__c.contains('BA') &&
               mapQueueNameVSQueueRecord.containsKey(caseRec.Development_Request_Assigned_To__c)) {
                   System.debug('inside BA Sprint===');
                caseRec.OwnerId = mapQueueNameVSQueueRecord.get(caseRec.Development_Request_Assigned_To__c);
                System.debug('Assigned to BA Queue');
               } 
            if(caseRec.Development_Request_Assigned_To__c.contains('Programmer') && 
               mapQueueNameVSQueueRecord.containsKey(caseRec.Development_Request_Assigned_To__c)) {
                caseRec.OwnerId = mapQueueNameVSQueueRecord.get(caseRec.Development_Request_Assigned_To__c);
                System.debug('Assigned to Programmer Queue');
            } // End of if
        } // End of for
        System.debug('-----listCaseforSprintBacklog------'+listCaseforSprintBacklog);
        
        System.debug('-----End------');
	}
}