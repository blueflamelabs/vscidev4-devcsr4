/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                    Description
*       1.0         160719      VennScience_BFL_Nisha        This Test class is built to testing positive and negative scenarios for 
the update cases owner and their related Case monitoring ownerId.
**********************************************************************************************************************************************************/
@isTest
public class trgr_Case_SetOwnerHandler_Test {

    @isTest
    public static void positiveTestForCaseOwnerUpdate() {  
        
        
        User usr = TestDataFactory.getUserRec();
        insert usr;
        
        Case csObj = TestDataFactory.getCaseRec();
        
        insert csObj;
        
        System.runAs(usr) {
            
            csObj.OwnerId = usr.Id;
            test.startTest();
            update csObj;
            
            
            Case_Monitoring__c cmObj = TestDataFactory.getCaseMonitoringRec(csObj);
            
            insert cmObj;
            
            
            Case cs = [SELECT Id,
                       OwnerId
                       FROM Case];
            Case_Monitoring__c cm = [SELECT Id,
                                     OwnerId
                                     FROM Case_Monitoring__c];
            
            
            System.assert(cm.OwnerID == cs.OwnerID);
            
            test.stopTest();
        }
        
    }
    
    
    @isTest
    public static void positiveTestForCaseOwnerUpdate1() {  
        
        
        User usr = TestDataFactory.getUserRec();
        insert usr;
        
        Case csObj = TestDataFactory.getCaseRec();
        
        insert csObj;
        csObj.status = 'Production Change Verification';
        test.startTest();
        update csObj;
        
        
        Case_Monitoring__c cmObj = TestDataFactory.getCaseMonitoringRec(csObj);
        
        insert cmObj;
        
        
        Case cs = [SELECT Id,
                   OwnerId
                   FROM Case];
        Case_Monitoring__c cm = [SELECT Id,
                                 OwnerId
                                 FROM Case_Monitoring__c];
        
        
        System.assert(cm.OwnerID == cs.OwnerID);
        
        test.stopTest();
        
    }
    /**
    * Method Name : updateCaseBillingRecordsPositiveTest
    * Parameters  : 
    * Description : This method is used to test the positive scenarios for updateCaseBillingRecords method
    * Created By  : VennScience_BFL_Amruta
    * Created On  : 170719
    **/
    @isTest
    public static void updateCaseBillingRecordsPositiveTest() {

        // Call Test Data Factory to fetch Account data
        List<Account> listAcount = TestDataFactory.getAccountRecords(5);
        // Call Test Data Factory 
        Case objCase = TestDataFactory.getCaseRec();
        insert objCase;
        
        // // 210819 - T-00564 - Call Test Data Factory to fetch Billing Item record
        Billing_Item__c billingItemRecord = TestDataFactory.getBillingItemsRecords();
        insert billingItemRecord;
        // System.debug('objCase.AccountId====='+objCase);
        // Create Billing record for the Case
        Billing__c objBilling = new Billing__c();
        objBilling.Name = 'Test Billing';
        objBilling.Account__c = objCase.AccountId;
        objBilling.Case__c = objCase.Id;
        objBilling.Quantity__c = 1;
        objBilling.Billing_Item__c = billingItemRecord.Id;
        objBilling.Period_End_Date__c = System.today();
        insert objBilling;
        // Create a new Account in order to update the Case Account
        Account objNewAcc = new Account();
        objNewAcc.Name = 'Test Account 001';
        insert objNewAcc;
        Test.startTest();
        // Update Case Account with this new Account
        Case objNewCase = new Case(Id=objCase.Id);
        objNewCase.AccountId = objNewAcc.Id;
        update objNewCase;
        
        Billing__c objBillingUpdated = new Billing__c();
        objBillingUpdated = [SELECT Id,
                                    Account__c
                               FROM Billing__c
                              WHERE Id = :objBilling.Id];
        Test.stopTest();
        // Assert: Check if Account on related Billing record is updated
        System.assertEquals(objBillingUpdated.Account__c,objNewCase.AccountId,'Billing Account is not updated');
    }
    /**
    * Method Name : updateCaseBillingRecordsNegativeTest
    * Parameters  : 
    * Description : This method is used to test the negative scenarios for updateCaseBillingRecords method
    * Created By  : VennScience_BFL_Amruta
    * Created On  : 170719
    **/
    @isTest
    public static void updateCaseBillingRecordsNegativeTest() {

        // Call Test Data Factory to fetch Account data
        List<Account> listAcount = TestDataFactory.getAccountRecords(5);
        // Call Test Data Factory 
        Case objCase = TestDataFactory.getCaseRec();
        insert objCase;
        // 210819 - T-00564 - Call Test Data Factory to fetch Billing Item record
        Billing_Item__c billingItemRecord = TestDataFactory.getBillingItemsRecords();
        insert billingItemRecord;
        // System.debug('objCase.AccountId====='+objCase);
        // Create Billing record for the Case
        Billing__c objBilling = new Billing__c();
        objBilling.Name = 'Test Billing';
        objBilling.Account__c = objCase.AccountId;
        objBilling.Case__c = objCase.Id;
        objBilling.Quantity__c = 1;
        objBilling.Billing_Item__c = billingItemRecord.Id;
        objBilling.Period_End_Date__c = System.today();
        insert objBilling;
        // Create a new Account in order to update the Case Account
        Account objNewAcc = new Account();
        objNewAcc.Name = 'Test Account 001';
        insert objNewAcc;
        Test.startTest();
        // Update Case Account with this new Account
        Case objNewCase = new Case(Id=objCase.Id);
        objNewCase.AccountId = objNewAcc.Id;
        
        Billing__c objBillingUpdated = new Billing__c();
        objBillingUpdated = [SELECT Id,
                                    Account__c
                               FROM Billing__c
                              WHERE Id = :objBilling.Id];
        Test.stopTest();
        // Assert: Check if Account on related Billing record is not updated
        System.assertNotEquals(objBillingUpdated.Account__c,objNewCase.AccountId,'Billing Account is updated');
    }
    
    /**
    * Method Name : updateCaseOwnerPositiveTestForTestingStatus
    * Parameters  : 
    * Description : This method is used to update the single case record with single case team member
    * Created By  : VennScience_BFL_Monali
    * Created On  : 111019
    **/
    
    /*@isTest
    public static void updateCaseOwnerForTestingStatusTest() {
       
        User user = TestDataFactory.getUserRec();
        
        insert user;
        system.runAs(user) {
            
            User u = TestDataFactory.createUserRec();
            insert u;
            // Insert BA Queue
            Group groupRec = TestDataFactory.createGroup();
            groupRec.Name = 'BA Queue';
            insert groupRec;
            
            Group groupRecBacklog = TestDataFactory.createGroup();
            groupRecBacklog.Name = 'Sprint Backlog';
            insert groupRecBacklog;
            System.debug('groupRecBacklog ->'+groupRecBacklog);
            // Insert Programmer Queue
            Group groupRec1 = TestDataFactory.createGroup();
            groupRec1.Name = 'Programmer Queue';
            insert groupRec1;
            
            GroupMember grupMember = TestDataFactory.createGroupMember(u.Id, groupRec.Id);
            insert grupMember;
            
            QueuesObject queueRec = TestDataFactory.createQueueRec(groupRec.Id);
            insert queueRec;
            QueuesObject queueRec1 = TestDataFactory.createQueueRec(groupRec1.Id);
            insert queueRec1;
            
            Account act = TestDataFactory.createAccount();
            
            insert act;
            
            Case cs = TestDataFactory.createCaseRec(act.Id, user.Id);
            cs.Has_CM__c = true;
            insert cs;
            
            //CaseTeamRole  caseTeamRole = TestDataFactory.createCaseTeamRoleRec();
            //insert caseTeamRole;
            List<CaseTeamRole> caseTeamRole = [SELECT Id,
                                                Name
                                              FROM CaseTeamRole
                                              WHERE Name like '%BA%'];
            System.debug('caseTeamRole ->'+caseTeamRole);
            
            CaseTeamMember caseTeam = TestDataFactory.createCaseTeamMemberRec(cs.Id, user.Id, caseTeamRole[0].Id);
            
            insert caseTeam;
            
            Test.startTest();
            Case caseRec = [Select Id, OwnerId, Status 
                                FROM Case
                         WHERE Id = :cs.Id];
            caseRec.Status = 'Coding';                  
            update caseRec;
            
            Case caseRec1 = [Select Id, OwnerId, Status 
                               FROM Case
                              WHERE Id = :cs.Id];
                              
            Test.stopTest();
            //System.assertEquals(caseTeam.MemberId, caseRec1.OwnerId);
        }
    } */
    
    /**
    * Method Name : updateCaseOwnerPositiveTestForTestingStatus
    * Parameters  : 
    * Description : This method is used to test the 
    * Created By  : VennScience_BFL_Amruta
    * Created On  : 101019
    **/
    
    @isTest
    public static void updateCaseOwnerForCodingStatusTest() {
       
        User user = TestDataFactory.getUserRec();
        
        insert user;
        system.runAs(user) {
            // Insert BA Queue
            Group groupRec = TestDataFactory.createGroup();
            
            groupRec.Name = 'BA Queue';
            insert groupRec;
            
            // Insert Programmer Queue
            Group groupRec1 = TestDataFactory.createGroup();
            
            groupRec1.Name = 'Programmer Queue';
            insert groupRec1;
            
            User u = TestDataFactory.createUserRec();
            insert u;
            GroupMember grupMember = TestDataFactory.createGroupMember(u.Id, groupRec.Id);
            insert grupMember;
            
            QueuesObject queueRec = TestDataFactory.createQueueRec(groupRec.Id);
            insert queueRec;
            
            QueuesObject queueRec1 = TestDataFactory.createQueueRec(groupRec1.Id);
            insert queueRec1;
            
            System.debug('queueRec======='+queueRec);
            System.debug('queue result========='+[Select Name From Group Where Id=:groupRec.Id]);
            
            Account act = TestDataFactory.createAccount();
            
            insert act;
            
            Case cs = TestDataFactory.createCaseRec(act.Id, user.Id);
            cs.Has_CM__c = true;
            insert cs;
            
            //CaseTeamRole  caseTeamRole = TestDataFactory.createCaseTeamRoleRec();
            //insert caseTeamRole;
            List<CaseTeamRole> caseTeamRole = [SELECT Id,
                                                Name
                                              FROM CaseTeamRole
                                              WHERE Name like '%Programmer%'];
            System.debug('caseTeamRole ->'+caseTeamRole);
            
            CaseTeamMember caseTeam = TestDataFactory.createCaseTeamMemberRec(cs.Id, user.Id, caseTeamRole[0].Id);
            
            insert caseTeam;
            
            Test.startTest();
            Case caseRec = [Select Id, OwnerId, Status 
                                FROM Case
                         WHERE Id = :cs.Id];
            caseRec.Status = 'Coding';                  
            update caseRec;
            
            Case caseRec1 = [Select Id, OwnerId, Status 
                               FROM Case
                              WHERE Id = :cs.Id];
                              
            Test.stopTest();
            //System.assertEquals(caseRec1.OwnerId, caseTeam.MemberId);
        }
    }
    
    /**
    * Method Name : caseOwnerForCodingStatusNegativeTest
    * Parameters  : 
    * Description : 
    * Created By  : VennScience_BFL_Amruta
    * Created On  : 101019
    **/
    
    @isTest
    public static void caseOwnerForCodingStatusNegativeTest() {
       
        User user = TestDataFactory.getUserRec();
        
        insert user;
        system.runAs(user) {
            // Insert BA Queue
            Group groupRec = TestDataFactory.createGroup();
            
            groupRec.Name = 'BA Queue';
            insert groupRec;
            
            // Insert Programmer Queue
            Group groupRec1 = TestDataFactory.createGroup();
            groupRec1.Name = 'Programmer Queue';
            insert groupRec1;
            
            User u = TestDataFactory.createUserRec();
            insert u;
            GroupMember grupMember = TestDataFactory.createGroupMember(u.Id, groupRec.Id);
            insert grupMember;
            
            QueuesObject queueRec = TestDataFactory.createQueueRec(groupRec.Id);
            insert queueRec;
            QueuesObject queueRec1 = TestDataFactory.createQueueRec(groupRec1.Id);
            insert queueRec1;
            
            Account act = TestDataFactory.createAccount();
            
            insert act;
            
            Case cs = TestDataFactory.createCaseRec(act.Id, user.Id);
            cs.Has_CM__c = true;
            insert cs;
            
            
            List<CaseTeamRole> caseTeamRole = [SELECT Id,
                                                Name
                                              FROM CaseTeamRole
                                              WHERE Name like '%Programmer%'];
            System.debug('caseTeamRole ->'+caseTeamRole);
            
            CaseTeamMember caseTeam = TestDataFactory.createCaseTeamMemberRec(cs.Id, user.Id, caseTeamRole[0].Id);
            
            insert caseTeam;
            
            Test.startTest();
            Case caseRec = [Select Id, OwnerId, Status 
                                FROM Case
                         WHERE Id = :cs.Id];
            caseRec.Status = 'Coding';                  
            update caseRec;
            
            Case caseRec1 = [Select Id, OwnerId, Status 
                                FROM Case
                         WHERE Id = :cs.Id];
            
            Test.stopTest();
            //System.assertEquals(caseRec1.OwnerId, caseTeam.MemberId);
        }
    }
    
    /**
    * Method Name : CaseOwnerForTestingStatusNegativeTest
    * Parameters  : 
    * Description : 
    * Created By  : VennScience_BFL_Amruta
    * Created On  : 111019
    **/
    
    @isTest
    public static void caseOwnerForTestingStatusNegativeTest() {
       
        User user = TestDataFactory.getUserRec();
        
        insert user;
        system.runAs(user) {
           /* Group groupRec = TestDataFactory.createGroup();
            insert groupRec;
            
            User u = TestDataFactory.createUserRec();
            insert u;
            GroupMember grupMember = TestDataFactory.createGroupMember(u.Id, groupRec.Id);
            insert grupMember;
            
            QueuesObject queueRec = TestDataFactory.createQueueRec(groupRec.Id);
            insert queueRec;*/
            
            Account act = TestDataFactory.createAccount();
            
            insert act;
            
            Case cs = TestDataFactory.createCaseRec(act.Id, user.Id);
            cs.Has_CM__c = true;
            insert cs;
            
            
            List<CaseTeamRole> caseTeamRole = [SELECT Id,
                                                Name
                                              FROM CaseTeamRole
                                              WHERE Name like '%Programmer%'];
            System.debug('caseTeamRole ->'+caseTeamRole);
            
            CaseTeamMember caseTeam = TestDataFactory.createCaseTeamMemberRec(cs.Id, user.Id, caseTeamRole[0].Id);
            
            insert caseTeam;
            
            Test.startTest();
            Case caseRec = [Select Id, OwnerId, Status 
                                FROM Case
                         WHERE Id = :cs.Id];
            caseRec.Status = 'Coding';                  
            update caseRec;
            
            Case caseRec1 = [Select Id, OwnerId, Status 
                               FROM Case
                              WHERE Id = :caseRec.Id];
            
            Test.stopTest();
            //System.assertNotEquals(cs.OwnerId, caseRec1.OwnerId);
            //System.assertEquals(caseRec1.OwnerId, caseTeam.MemberId);
        }
    }
    
    /**
    * Method Name : bulkCaseOwnerTestingStatusWithOutTeamMember
    * Parameters  : 
    * Description : 
    * Created By  : VennScience_BFL_Monali
    * Created On  : 111019
    **/
    
    @isTest
    public static void bulkCaseOwnerTestingStatusWithOutTeamMember() {
       
        User user = TestDataFactory.getUserRec();
        
        insert user;
        system.runAs(user) {
            
            Account act = TestDataFactory.createAccount();
            
            insert act;
            
            List<Case> caseList = TestDataFactory.createCaseRecList(act.Id, user.Id);
            for(Case caseRecord : caseList){
                caseRecord.Has_CM__c = true;
            }
            Test.startTest();
            insert caseList;
            
            List<Case> updateCaseList = [SELECT Id,
                                                 OwnerId,
                                                 Status
                                         FROM Case
                                         WHERE Id IN : caseList];
            System.debug('updateCaseList ->'+updateCaseList);
            for(Integer i=0;i<updateCaseList.size();i++) {
                if(i < updateCaseList.size()/2) {
                    updateCaseList[i].status = 'Coding';
                }else {
                    updateCaseList[i].status = 'Testing';
                }
            }
            update updateCaseList;
            System.debug('updateCaseList 11->'+updateCaseList);
            Test.stopTest();
            
            List<Case> updateCaseList1 = [SELECT Id,
                                                 OwnerId,
                                                 Status
                                         FROM Case
                                         WHERE Id IN : updateCaseList];
            
            System.assertEquals(caseList[0].OwnerId, caseList[0].OwnerId);
        }
    }
    
    /**
    * Method Name : bulkCaseOwnerStatuTeamMember
    * Parameters  : 
    * Description : 
    * Created By  : VennScience_BFL_Monali
    * Created On  : 111019
    **/
    
    /*@isTest
    public static void bulkCaseOwnerStatusWithTeamMember() {
       
        User user = TestDataFactory.getUserRec();
        
        insert user;
        system.runAs(user) {
            
            Group groupRec = TestDataFactory.createGroup();
            insert groupRec;
            
            User u = TestDataFactory.createUserRec();
            insert u;
            
            GroupMember grupMember = TestDataFactory.createGroupMember(u.Id, groupRec.Id);
            insert grupMember;
            
            QueuesObject queueRec = TestDataFactory.createQueueRec(groupRec.Id);
            insert queueRec;
           
           
            Account act = TestDataFactory.createAccount();
            insert act;
            
            List<Case> caseList = TestDataFactory.createCaseRecList(act.Id, user.Id);
            
            Test.startTest();
            insert caseList;
            
            List<CaseTeamRole> caseTeamRole = [SELECT Id,
                                                Name
                                              FROM CaseTeamRole
                                              WHERE Name like '%Programmer%'];
            System.debug('caseTeamRole ->'+caseTeamRole);
            
            List<CaseTeamMember> caseTeamList = TestDataFactory.createCaseTeamMemberRecList(user.Id, caseTeamRole[0].Id);
            for(Integer i = 0; i < caseTeamList.size(); i++) {
                if(i < 2) {
                    caseTeamList[i].ParentId = caseList[0].Id;
                } else {
                    caseTeamList[i].ParentId = caseList[1].Id;
                }
            }
             System.debug('caseTeamList ->'+caseTeamList);
            insert caseTeamList;
            
           
            
            List<Case> updateCaseList = [SELECT Id,
                                                 OwnerId,
                                                 Status
                                         FROM Case
                                         WHERE Id IN : caseList];
            System.debug('updateCaseList ->'+updateCaseList);
            for(Integer i=0;i<updateCaseList.size();i++) {
                if(i < updateCaseList.size()/2) {
                    updateCaseList[i].status = 'Code Review';
                }else {
                    updateCaseList[i].status = 'Coding';
                }
            }
            update updateCaseList;
            System.debug('updateCaseList 11->'+updateCaseList);
            Test.stopTest();
            
            List<Case> updateCaseList1 = [SELECT Id,
                                                 OwnerId,
                                                 Status
                                         FROM Case
                                         WHERE Id IN : updateCaseList];
            
            System.assertNotEquals(caseList[0].OwnerId, updateCaseList1[0].OwnerId);
        }
    }*/
    /**
    * Method Name : checkNeedsReviewTest
    * Parameters  : 
    * Description : This method is used to test the checkNeedsReview method
    * Created By  : VennScience_BFL_Amruta
    * Created On  : 210819
    **/
    /*
    @isTest
    public static void checkNeedsReviewTest() {

        // Call Test Data Factory to fetch Account data
        List<Account> listAcount = TestDataFactory.getAccountRecords(5);
        Account getAccRecord = [Select Id From Account LIMIT 1];
        Contact getConRecord = [Select Id From Contact LIMIT 1];
        // Call Test Data Factory 
        Case objCase = TestDataFactory.getExternalServiceCaseRec2(getAccRecord,getConRecord );
        Test.startTest();
        insert objCase;
        Test.stopTest();
        
        // Fetch Case record 
        Case caseUpdatedRecord = new Case();
        caseUpdatedRecord = [SELECT Needs_Review__c 
                               FROM Case
                              WHERE Id = :objCase.Id];
        // Assert: Check if Needs Review checkbox is updated
        //System.assertEquals(caseUpdatedRecord.Needs_Review__c,true,'Needs Review checkbox is not updated');
    }
    */
}