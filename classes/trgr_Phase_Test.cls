@isTest
private class trgr_Phase_Test 
{
	@isTest static void TestPhaseName() 
    {
        // Test data setup
        // Create an object
        Case objCase = new Case();
        insert objCase;
        Change_Management_Phase__c objPhase = new Change_Management_Phase__c(Name='Phase',
                                       Case__c=objCase.Id);
        insert objPhase;
        
        // Perform test
        Test.startTest();
        //Database.DeleteResult result = Database.delete(acct, false);
        Test.stopTest();
        // Verify 
        // In this case the deletion should have been stopped by the trigger,
        // so verify that we got back an error.
        System.assertEquals( 'Phase 1', 'Phase 1');
	}
}