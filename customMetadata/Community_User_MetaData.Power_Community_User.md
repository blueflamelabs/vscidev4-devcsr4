<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>+Power Community User</label>
    <protected>false</protected>
    <values>
        <field>CU_Admin__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Default__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Profile_ID__c</field>
        <value xsi:type="xsd:string">+Power Community User</value>
    </values>
</CustomMetadata>
