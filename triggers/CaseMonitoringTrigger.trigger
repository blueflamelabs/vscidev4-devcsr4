trigger CaseMonitoringTrigger on Case_Monitoring__c (before insert,after insert, after update)
{
    CaseMonitoringHandler obj= new CaseMonitoringHandler(Trigger.new);
	CaseMonitoringHandler objHandler = new CaseMonitoringHandler(Trigger.new, Trigger.oldMap);
    
    Switch on Trigger.operationType
    {

        //when BEFORE_INSERT 
        //{
            //obj.beforeInsert();
        //}
        when AFTER_INSERT 
        {
            //System.debug('After insert called');
            //obj.afterInsert();
            //100919 - T-00627 - VennScience_BFL_Monali - call the function on after Insert.
            obj.updateCaseMonitoringRelatedCases();
        }
        when AFTER_UPDATE 
        {
            System.debug('After update called');
             //100919 - T-00627 - VennScience_BFL_Monali - Commented existing method on Update.
             //obj.afterUpdate();
            //100919 - T-00627 - VennScience_BFL_Monali - call the function on after update.
            obj.updateCaseMonitoringRelatedCases();
            //Pass list of case comment records and Trigger.OldMap
            //CaseCommentHandler objHandler = new CaseCommentHandler(Trigger.new, Trigger.oldMap);
            //objHandler.afterUpdate();
        }
        
    }

}