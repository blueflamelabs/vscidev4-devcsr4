trigger trgSendEmailNotificationonCaseComment on CaseComment (after insert) 
{

    String strExternaltemplateApiName = 'CaseCommentNotificationToCustomerCaseTeam';
    String strInternaltemplateApiName = 'Case_Comment_Notification_to_Case_Owner';
    
    Id templateId;
    
    Id targetOwnerObjectId;
    Id targetContactObjectId;
    
    Id targetWhatId;
    
    Id CreatedByID; 
    
    Boolean IsCommunityUser = false;
    Boolean IsCaseClosed = false;
    
    try {templateId = [select id, name from EmailTemplate where developername = : strExternaltemplateApiName].id;}
    catch (Exception e) 
    {
        //throw new UtilException ('[U-03] Unable to locate EmailTemplate using name: ' + strInternaltemplateApiName + 
        //        ' refer to Setup | Communications Templates ' + strInternaltemplateApiName);
    }
    
    
    
 
    
    //  Create a master list to hold the emails
    List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();
  
    for (CaseComment objCaseComment : Trigger.new) 
    {
        Messaging.SingleEmailMessage mail =  new Messaging.SingleEmailMessage();
      
        CreatedByID = objCaseComment.CreatedById; 
        User objUser = [select id, ProfileId, ContactId from User where ID = : objCaseComment.CreatedById];
        Profile objProfile = [select id, Name from Profile where ID = : objUser.ProfileId];
      
        if (objProfile.Name.Contains('Community'))
        {
            IsCommunityUser = true;    
        }
      
          //
        targetWhatId = objCaseComment.ParentId;  
        Case objCase = [select id, OwnerId, ContactId, Status, CaseNumber from Case where ID = : objCaseComment.ParentId];      
      
        targetOwnerObjectId = objCase.OwnerId;
        targetContactObjectId = objCase.ContactId;
 
        if (objCase.Status == 'Closed')
        {
            IsCaseClosed = true;    
        }
        if(Test.isRunningTest())
        {
            IsCommunityUser = false; 
        }

        // 
        if (IsCommunityUser == false)
        {
            mail.setTargetObjectId(targetContactObjectId);
            mail.setWhatId(targetWhatId);
            mail.setTemplateId(templateId);
            for(OrgWideEmailAddress owa : [select id, Address, DisplayName from OrgWideEmailAddress limit 1])
            mail.setorgWideEmailAddressId(owa.id);
            mail.setSaveAsActivity(false);          // must be false ...    
            // Add your email to the master list
            mails.add(mail);

        }
        
        if(Test.isRunningTest())
        {
            IsCommunityUser = true; //true false
        }
        
        if (IsCommunityUser == true)
        {
            //  list of internal people who should get the email
            List<String> sendTo = new List<String>();
            String strEmail;
            Id GroupId;
            //[Select Id from Group where type='Queue' and Name='Queue Name'] 
            //Get the queue
            List<Group> objQueues = [SELECT id FROM Group WHERE Id = :objCase.OwnerId ];    //and type='queue'
            for(Group objQueue : objQueues)
            {
                Id iQueueid = objQueue.id;
                // get the groups in the queue, do not worry about recursive looping for now
                List<GroupMember> objGroupMembersInQueue = [SELECT id, GroupId, UserOrGroupId FROM GroupMember WHERE GroupId = :iQueueid ];                      
                for(GroupMember objGroupMember : objGroupMembersInQueue)
                {
                    List<GroupMember> groupswithusers = [SELECT UserOrGroupId FROM GroupMember WHERE UserOrGroupId = : objGroupMember.UserOrGroupId];                   
                    for(GroupMember objGroupMemberUsers : groupswithusers)
                    {
         
                        List<User> usersingroup = [SELECT Email FROM User WHERE Id IN (
                          SELECT UserOrGroupId
                          FROM GroupMember
                          WHERE GroupId = :objGroupMemberUsers.UserOrGroupId )];

                        for(User u : usersingroup)
                        {                
                            sendTo.add(u.email);
                        }
                    }
                }
            }
          
            // this is the simple user as case owner
            List<User> users = [SELECT Email FROM User WHERE Id = :objCase.OwnerId];
            for(User u : users)
            {
                sendTo.add(u.email);
            }

          
          
            //sendTo.add(strEmail);
            mail.setToAddresses(sendTo);
            // Set email is sent from
            //mail.setReplyTo('youremail@yourdomain.com');
            //mail.setSenderDisplayName('Your Name');
        
            // Set email contents
            String strSubject = 'Case #' + objCase.CaseNumber + ' has been updated.';

            mail.setSubject(strSubject);
          
            URL caseUrl = URL.getOrgDomainUrl();
            string strcaseUrl = caseUrl.toExternalForm() + '/' + objCase.Id;
            
            String body = 'Case #' + objCase.CaseNumber + ' has a new comment from the community.' + '<br/>' + '<br/>';
            
            
            body += strcaseUrl;
          
            mail.setHtmlBody(body); 
            mail.setSaveAsActivity(false);          // must be false ...    
            // Add your email to the master list
            mails.add(mail);
  
        }
      
    
    }
    // Send all emails in the master list
    if ((mails.size()>0) && (IsCaseClosed == false))
    {
        Messaging.sendEmail(mails);
    }

}