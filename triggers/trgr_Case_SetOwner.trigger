trigger trgr_Case_SetOwner on Case ( after insert, before update, after update) 
{
    // 160719 - T-00445 -  VennScience_BFL_Nisha - Pass list of case  records and Trigger.OldMap
    if(trigger.isBefore && trigger.isUpdate){
        System.debug('Inside before update');
        System.debug('trgr_Case_SetOwnerHandler.executeUpdateTrigger======'+trgr_Case_SetOwnerHandler.executeUpdateTrigger);
       if(trgr_Case_SetOwnerHandler.executeUpdateTrigger) {
             System.debug('Inside before update');
            trgr_Case_SetOwnerHandler objHandler = new  trgr_Case_SetOwnerHandler(Trigger.new, Trigger.oldMap);
           // Below method is changes the case monitoring Owner 
            objHandler.updateCaseOwner();
           // Below method is changes the case Owner 
            trgr_Case_SetOwnerHandler.caseOwnerUpdate(Trigger.new, Trigger.oldMap);
        } // End of inner if
    }
    
    // 170719 - T-00443 - VennScience_BFL_Amruta - Call handler in order to update the related Billing records when Account is updated
    if(trigger.isAfter && trigger.isUpdate) {
        //System.debug('trgr_Case_SetOwnerHandler.executeUpdateTrigger======'+trgr_Case_SetOwnerHandler.executeUpdateTrigger);
        if(trgr_Case_SetOwnerHandler.executeUpdateTrigger) {
            System.debug('Inside after update ==== ');
            trgr_Case_SetOwnerHandler objHandler = new  trgr_Case_SetOwnerHandler(Trigger.new, Trigger.oldMap);
            objHandler.updateCaseBillingRecords();
        } // End of inner if       
    }
    // 140819 - T-00547 - VennScience_BFL_Amruta - Call handler in order to update the Needs review checkbox when Case is created by internal user
    if(trigger.isAfter && Trigger.isInsert) {
        System.debug('Inside after insert');
        trgr_Case_SetOwnerHandler objHandler = new  trgr_Case_SetOwnerHandler(Trigger.new, Trigger.oldMap);
        objHandler.checkNeedsReview();
    }
    /*for(Case obj : trigger.new)
    {
        if (Trigger.oldmap.get(obj.id).Status != Trigger.newmap.get(obj.id).Status)
        {
          if ((Trigger.oldmap.get(obj.id).Status == 'Final Change Review Approval') &&  (Trigger.newmap.get(obj.id).Status == 'Production Change Verification' ))
          {
                obj.OwnerId = obj.CreatedById; 
          }
          if ((Trigger.oldmap.get(obj.id).Status == 'Final Change Review Approval') &&  (Trigger.newmap.get(obj.id).Status == 'Closed' ))
          {
                obj.OwnerId = obj.CreatedById; 
          }

        }
    }*/

}