<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Account_non_activity</fullName>
        <description>Account non activity</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Last_activity_is_over_60_days</template>
    </alerts>
    <alerts>
        <fullName>Cards_Expiration</fullName>
        <description>Cards Expiration</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderAddress>customerservice@synergentcorp.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Cards_Contract_End_Date</template>
    </alerts>
    <alerts>
        <fullName>Checks_Expiration</fullName>
        <description>Checks Expiration</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderAddress>customerservice@synergentcorp.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Checks_Contract_End_Date</template>
    </alerts>
    <alerts>
        <fullName>Core_Expiration</fullName>
        <description>Core Expiration</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderAddress>customerservice@synergentcorp.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Core_Contract_End_Date</template>
    </alerts>
    <rules>
        <fullName>Notification of non-activity</fullName>
        <actions>
            <name>Account_non_activity</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>LastActivityDate  &gt; TODAY()-60</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify of Cards Expiration</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.Cards_Contract_End_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Cards_Expiration</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Account.Cards_Contract_End_Date__c</offsetFromField>
            <timeLength>-540</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Notify of Checks Expiration</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.Checks_Contract_End_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Checks_Expiration</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Account.Checks_Contract_End_Date__c</offsetFromField>
            <timeLength>-540</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Notify of Core Expiration</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.Core_Contract_End_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Core_Expiration</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Account.Core_Contract_End_Date__c</offsetFromField>
            <timeLength>-540</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
