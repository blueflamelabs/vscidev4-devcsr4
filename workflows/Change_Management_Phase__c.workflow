<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ECM_Approve</fullName>
        <description>ECM Approve</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ECM_Phase_Approval_Notification</template>
    </alerts>
    <alerts>
        <fullName>ECM_Recall</fullName>
        <description>ECM Recall</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ECM_Phase_Approval_Recall</template>
    </alerts>
    <alerts>
        <fullName>ECM_Reject</fullName>
        <description>ECM Reject</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ECM_Phase_Rejection_Notification</template>
    </alerts>
    <alerts>
        <fullName>Initial_Approval_Granted</fullName>
        <description>Initial Approval Granted</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CM_Phase_Approval_Notification</template>
    </alerts>
    <alerts>
        <fullName>Initial_Approval_rejected</fullName>
        <description>Initial Approval rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CM_Phase_Rejection_Notification</template>
    </alerts>
    <alerts>
        <fullName>phase_recall</fullName>
        <description>phase recall</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CM_Phase_Approval_Recall</template>
    </alerts>
    <fieldUpdates>
        <fullName>SetPhaseComplete</fullName>
        <field>Date_Completed__c</field>
        <formula>Now()</formula>
        <name>SetPhaseComplete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetPhaseStatusComplete</fullName>
        <field>Status__c</field>
        <literalValue>Complete</literalValue>
        <name>SetPhaseStatusComplete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetStatusEmergApproval</fullName>
        <field>Status__c</field>
        <literalValue>Emergency Approval</literalValue>
        <name>SetStatusEmergApproval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetStatusEmergRejected</fullName>
        <field>Status__c</field>
        <literalValue>Emergency Approval Rejected</literalValue>
        <name>SetStatusEmergRejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetStatusInitialApproval</fullName>
        <field>Status__c</field>
        <literalValue>Initial Review Approval</literalValue>
        <name>SetStatusInitialApproval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetStatusInitialApprovalPending</fullName>
        <field>Status__c</field>
        <literalValue>Initial Review Approval Pending</literalValue>
        <name>SetStatusInitialApprovalPending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetStatusInitialRejected</fullName>
        <field>Status__c</field>
        <literalValue>Initial Review Approval Rejected</literalValue>
        <name>SetStatusInitialRejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetStatusProdTeamApproval</fullName>
        <field>Status__c</field>
        <literalValue>Ops General Approval</literalValue>
        <name>SetStatusProdTeamApproval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetStatusProdTeamRejected</fullName>
        <field>Status__c</field>
        <literalValue>Ops General Approval Rejected</literalValue>
        <name>SetStatusProdTeamRejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetStatusVerifyApproval</fullName>
        <field>Status__c</field>
        <literalValue>Production Implementation Approval</literalValue>
        <name>SetStatusVerifyApproval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetStatusVerifyRejected</fullName>
        <field>Status__c</field>
        <literalValue>Production Implementation Approval Rejected</literalValue>
        <name>SetStatusVerifyRejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StatusEmerg</fullName>
        <field>Status__c</field>
        <literalValue>Emergency Approval Pending</literalValue>
        <name>StatusEmerg</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StatusProd</fullName>
        <field>Status__c</field>
        <literalValue>Ops General Approval Pending</literalValue>
        <name>StatusProd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StatusVerify</fullName>
        <field>Status__c</field>
        <literalValue>Production Implementation Approval Pending</literalValue>
        <name>StatusVerify</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
